<?php
/**
 * Created by PhpStorm.
 * User: oren
 * Date: 08-Nov-17
 * Time: 8:19 AM
 */

class PLSMHeaderFooterTerms {


	function __construct() {


		$taxonomies = get_taxonomies();

		if ( $taxonomies ) {
			foreach ( $taxonomies  as $key=>$taxonomy ) {
				add_action( $key.'_edit_form_fields', array( $this, 'edit_category_fields' ) );
				add_action( 'edited_'.$key, array( $this, 'update_category_meta' ) );
			}
		}

	}


	function update_category_meta( $term_id ) {
		// Secondly we need to check if the user intended to change this value.
		if ( ! isset( $_POST['plsm_add_script_noncename'] ) || ! wp_verify_nonce( $_POST['plsm_add_script_noncename'], plugin_basename( __FILE__ ) ) )
			return;

		if ( isset( $_POST['plsm-hide-header'] ) && '' !== $_POST['plsm-hide-header'] ) {
			update_term_meta( $term_id, 'plsm-hide-header', $_POST['plsm-hide-header'] );
		} else {
			delete_term_meta( $term_id, 'plsm-hide-header' );
		}
		if ( isset( $_POST['plsm-hide-footer'] ) && '' !== $_POST['plsm-hide-footer'] ) {
			update_term_meta( $term_id, 'plsm-hide-footer', $_POST['plsm-hide-footer'] );
		} else {
			delete_term_meta( $term_id, 'plsm-hide-footer' );
		}
		if ( isset( $_POST['plsm-header-script'] )   ) {
			update_term_meta( $term_id, 'plsm-header-script', $_POST['plsm-header-script'] );
		}

		if ( isset( $_POST['plsm-footer-script'] )   ) {
			update_term_meta( $term_id, 'plsm-footer-script', $_POST['plsm-footer-script'] );
		}



	}




	function edit_category_fields( $term ) {


		// get current group
		$hide_header = get_term_meta( $term->term_id, 'plsm-hide-header', true );
		$hide_footer = get_term_meta( $term->term_id, 'plsm-hide-footer', true );
		$header      = get_term_meta( $term->term_id, 'plsm-header-script', true );
		$footer      = get_term_meta( $term->term_id, 'plsm-footer-script', true );
		wp_nonce_field( plugin_basename( __FILE__ ), 'plsm_add_script_noncename' );
		?>


        <tr class="form-field term-group-wrap">
					<th scope="row">
						<label for="plsm_add_script_header_hide"><?php _e( "Hide global header script", 'plsm_add_script' ); ?></label>
					</th>
					<td>
						<input type="checkbox" <?php echo checked( $hide_header, 'on', false ); ?> name="plsm-hide-header" id="plsm_add_script_header_hide"/>
						<p class="description"></p>
					</td>
        </tr>
        <tr class="form-field term-group-wrap">
					<th scope="row">
						<label for="plsm_add_script_footer_hide"><?php _e("Hide global footer script", 'plsm_add_script' ); ?></label>
					</th>
					<td>
						<input type="checkbox" <?php echo checked( $hide_footer, 'on', false ); ?> name="plsm-hide-footer" id="plsm_add_script_footer_hide"/>
						<p class="description"></p>
					</td>
        </tr>
        <tr class="form-field term-group-wrap">
					<th scope="row">
						<label for="plsm_add_script_header"><?php _e( 'Header Scripts', 'plsm_add_script' ); ?></label>
					</th>
					<td>
						<textarea class="large-text" cols="50" rows="5" id="plsm_add_script_header" name="plsm-header-script"><?php echo $header ?></textarea>
						<p class="description"></p>
					</td>
        </tr>
        <tr class="form-field term-group-wrap">
        <th scope="row">
					<label for="plsm_add_script_footer"><?php _e( 'Footer Scripts', 'plsm_add_script' ); ?></label>
				</th>
        <td>
					<textarea class="large-text" cols="50" rows="5" id="plsm_add_script_footer" name="plsm-footer-script"><?php echo $footer ?></textarea>
					<p class="description"></p>
				</td>
			</tr>
	<?php
	}
}

if ( is_admin() ) {
	$my_terms_settings_page = new PLSMHeaderFooterTerms();
}