<?php

class PLSMHeaderFooterSetting
{
    /**
     * Holds the values to be used in the fields callbacks
     */

    /**
     * Start up
     */
    public function __construct()
    {
        add_action('admin_menu', array($this, 'plsm_add_plugin_page'));
        add_action('admin_init', array($this, 'plsm_page_init'));
    }


    /**
     * Add options page
     */
    public function plsm_add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Page Level Scripts Manager',
            'Page Level Scripts Manager',
            'manage_options',
            'plsm-header-footer-settings',
            array($this, 'plsm_create_admin_page')
        );
    }

    /**
     * Options page callback
     */
    public function plsm_create_admin_page()
    {
        // Set class property
        $this->options = get_option('plsm_header_footer');
?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>Page Level Script Manager Settings</h2>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('global_plsm_options');
                do_settings_sections('plsm-setting-admin');
                submit_button();
                ?>
            </form>
        </div>
<?php
    }

    /**
     * Register and add settings
     */
    public function plsm_page_init()
    {
        register_setting(
            'global_plsm_options', // Option group
            'plsm_header_footer', // Option name
            array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            '', // Title
            array($this, 'print_section_info'), // Callback
            'plsm-setting-admin' // Page
        );

        add_settings_field(
            'plsm_header_script', // ID
            'Global Header Scripts', // Title
            array($this, 'plsm_header_callback'), // Callback
            'plsm-setting-admin', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'plsm_posttype', // ID
            'Only show global header scripts on these post types:', // Title
            array($this, 'plsm_posttype_callback'), // Callback
            'plsm-setting-admin', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'plsm_footer_script', // ID
            'Global Footer Scripts', // Title
            array($this, 'plsm_footer_callback'), // Callback
            'plsm-setting-admin', // Page
            'setting_section_id' // Section
        );

        add_settings_field(
            'plsm_posttype_footer', // ID
            'Only show global footer scripts on these post types:', // Title
            array($this, 'plsm_posttype_footer_callback'), // Callback
            'plsm-setting-admin', // Page
            'setting_section_id' // Section
        );

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input)
    {
        $new_input = array();
        if (isset($input['plsm_header']))
            $new_input['plsm_header'] = $input['plsm_header'];
        if (isset($input['plsm_footer']))
            $new_input['plsm_footer'] = $input['plsm_footer'];
        if (isset($input['plsm_posttype']))
            $new_input['plsm_posttype'] = $input['plsm_posttype'];
        if (isset($input['plsm_posttype_footer']))
            $new_input['plsm_posttype_footer'] = $input['plsm_posttype_footer'];

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'For individual pages please go to the page itself and use the header and footer scripts section there.  You can also hide the global header and footer scripts on individual pages. ';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function plsm_header_callback()
    {
        printf(
            '<textarea style="margin: 0px; width: 730px; height: 211px;" id="plsm_header" name="plsm_header_footer[plsm_header]"   >%s</textarea>',
            isset($this->options['plsm_header']) ? esc_attr($this->options['plsm_header']) : ''
        );
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function plsm_posttype_callback()
    {
        $post_types = get_post_types();
        $selected = isset($this->options['plsm_posttype']) ? $this->options['plsm_posttype'] : array();
        echo '<div style="display: flex; flex-wrap: wrap; justify-content: start;">';
        echo '<p style="width: 100%;margin-bottom:1em;">If all boxes are unchecked, the global header scripts will display on all post types.</p>';
        foreach ($post_types as $key => $post_type) {
            $checked = (in_array($post_type, $selected)) ? 'checked="checked"' : '';
            echo '<label for="' . $key . '" style="width: 20%; margin-bottom: 1em;"><input value="' . $post_type . '" type="checkbox" name="plsm_header_footer[plsm_posttype][]" ' . $checked . ' id="' . $key . '"/>' . $post_type . '</label>';
        }
        echo '</div>';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function plsm_posttype_footer_callback()
    {
        $post_types = get_post_types();
        $selected = isset($this->options['plsm_posttype_footer']) ? $this->options['plsm_posttype_footer'] : array();
        echo '<div style="display: flex; flex-wrap: wrap; justify-content: start;">';
        echo '<p style="width: 100%;margin-bottom:1em;">If all boxes are unchecked, the global footer scripts will display on all post types.</p>';
        foreach ($post_types as $key => $post_type) {
            $checked = (in_array($post_type, $selected)) ? 'checked="checked"' : '';
            echo '<label for="footer_' . $key . '" style="width: 20%; margin-bottom: 1em;"><input value="' . $post_type . '" type="checkbox" name="plsm_header_footer[plsm_posttype_footer][]" ' . $checked . ' id="footer_' . $key . '"/>' . $post_type . '</label>';
        }
        echo '</div>';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function plsm_footer_callback()
    {
        printf(
            '<textarea style="margin: 0px; width: 730px; height: 211px;" id="plsm_footer" name="plsm_header_footer[plsm_footer]"   >%s</textarea>',
            isset($this->options['plsm_footer']) ? esc_attr($this->options['plsm_footer']) : ''
        );
    }

    private function get_post_types()
    {
        $args = array(
            'public' => true,
            '_builtin' => false
        );

        $output = 'names'; // names or objects, note names is the default
        $operator = 'and'; // 'and' or 'or'
        $post_types = get_post_types($args, $output, $operator);
        $post_types[] = 'post';
        $post_types[] = 'page';
        return $post_types;
    }
}

if (is_admin())
    $my_settings_page = new PLSMHeaderFooterSetting();
