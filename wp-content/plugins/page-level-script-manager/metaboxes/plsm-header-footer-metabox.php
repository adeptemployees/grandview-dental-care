<div class="postbox">
    <div class="inside">
        <table width="100%">
            <tr>
                <td valign="top">
                    <p>
                        <label for="plsm_add_script_header_hide">
                            <input type="checkbox" <?php echo checked($hide_header, 'on', false) ?> name="plsm_add_script_header_hide" id="plsm_add_script_header_hide" />
                            <?php _e("Hide global header scripts on this page", 'plsm_add_script'); ?>
                        </label>
                    </p>
                    <p>
                        <label for="plsm_add_script_footer_hide">
                            <input type="checkbox" <?php echo checked($hide_footer, 'on', false) ?> name="plsm_add_script_footer_hide" id="plsm_add_script_footer_hide" />
                            <?php _e("Hide global footer scripts on this page", 'plsm_add_script'); ?>
                        </label>
                    </p>
                </td>
            </tr>
        </table>
    </div>
</div>

<div class="postbox">
    <div class="inside">
        <p><?php _e('Include script & style tags<br><code> &lt;script type="text/javascript"&gt; the code &lt;/script&gt;</code><br><code> &lt;style&gt; .element {} &lt;/style&gt;</code>', 'plsm_add_script_footer'); ?></p>
        <table width="100%">
            <tr>
                <td valign="top">
                    <p>
                        <label for="plsm_add_script_header">
                            <?php _e("Page Level Header Styles / Scripts", 'plsm_add_script'); ?>
                        </label>
                        <textarea style="display: block;width: 100%;min-height: 150px;" id="plsm_add_script_header" name="plsm_add_script_header" size="25"><?php echo $value ?> </textarea>
                    </p>
                </td>
            </tr>

            <tr>
                <td valign="top">
                    <p>
                        <label for="plsm_add_script_header">
                            <?php _e("Page Level Footer Scripts", 'plsm_add_script_footer'); ?>
                        </label>
                        <textarea style="display: block;width: 100%;min-height: 150px;" id="plsm_add_script_footer" name="plsm_add_script_footer" size="25"><?php echo $value_footer ?></textarea>
                    </p>
                </td>
            </tr>

        </table>

    </div>
    <!-- .inside -->

</div>
<!-- .postbox -->