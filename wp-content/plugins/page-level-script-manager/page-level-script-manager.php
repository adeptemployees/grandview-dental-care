<?php
/**
 * Plugin Name: Page Level Script Manager
 * Plugin URI: https://www.adeptmarketing.com
 * Description:  Provides a simple way to add javscript code to individual pages, posts, or custom post types header and footer.
 * Version: 1.0
 * Author: Adept
 * Author URI: https://www.adeptmarketing.com
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

require_once plugin_dir_path( __FILE__ ) . "plsm-settings-page.php";
add_action('init',function(){
	require_once plugin_dir_path( __FILE__ ) . "plsm-terms-settings.php";
}, 99);



add_filter('plugin_action_links_page-level-script-manager/page-level-script-manager.php', 'plsm_settings_link');
function plsm_settings_link($links)
{
	// Build and escape the URL.
	$url = esc_url(add_query_arg(
		'page',
		'plsm-header-footer-settings',
		get_admin_url() . 'options-general.php'
	));
	// Create the link.
	$settings_link = "<a href='$url'>" . __('Settings') . '</a>';
	// Adds the link to the end of the array.
	array_push(
		$links,
		$settings_link
	);
	return $links;
}


function plsm_get_current_page_id() {
	global $post;

	$id = false;
	if ( ! isset( $post ) ) {
		return false;
	}
	if ( class_exists( 'WooCommerce' ) && is_shop() ) {
		$id = wc_get_page_id( 'shop' );
	} else {
		// skip archive pages
		if ( is_singular() ) {
			$id = $post->ID;
		}
	}

	return $id;

}

// add scripts to header
function plsm_add_script() {
	$output = '';
	if ( is_tax() || is_category() || is_tag() ) {

		$output = get_term_meta( get_queried_object_id(), 'plsm-header-script', true );
		$show_global =  get_term_meta( get_queried_object_id(), 'plsm-hide-header', true ) != 'on'   ;
	} else {
		$id = plsm_get_current_page_id();
		if ( $id ) {
			$output = get_post_meta( $id, '_plsm_add_script_header', true );
		}
		$show_global = ( plsm_show_me_on( 'plsm_posttype' ) && get_post_meta( $id, '_plsm_add_script_header_hide', true ) != 'on' ) ;

	}

	$plsm_header_footer = get_option( 'plsm_header_footer' );

	if ($show_global && isset( $plsm_header_footer['plsm_header'] ) ) {
		echo stripslashes(  $plsm_header_footer['plsm_header'] );
	}

	echo stripslashes( $output );

}

// add scripts to footer
function plsm_add_script_footer() {

	$output = '';
	if ( is_tax() || is_category() || is_tag() ) {

		$output = get_term_meta( get_queried_object_id(), 'plsm-footer-script', true );
		$show_global =  get_term_meta( get_queried_object_id(), 'plsm-hide-footer', true ) != 'on'   ;
	} else {
		$id = plsm_get_current_page_id();
		if ( $id ) {
			$output = get_post_meta( $id, '_plsm_add_script_footer', true );
		}
		$show_global = ( plsm_show_me_on( 'plsm_posttype_footer' ) && get_post_meta( $id, '_plsm_add_script_footer_hide', true ) != 'on' ) ;

	}

	$plsm_header_footer = get_option( 'plsm_header_footer' );

	if ($show_global && isset( $plsm_header_footer['plsm_footer'] ) ) {
		echo stripslashes(  $plsm_header_footer['plsm_footer'] );
	}

	echo stripslashes( $output );


}

add_action( 'wp_head', 'plsm_add_script' );
add_action( 'wp_footer', 'plsm_add_script_footer' );

function plsm_show_me_on( $param ) {
	$plsm_header_footer = get_option( 'plsm_header_footer' );
	if ( isset( $plsm_header_footer[ $param ] ) ) {
		return in_array( get_post_type(), $plsm_header_footer[ $param ] );
	}

	return true; // if not set - show on all
}


/* Define the custom box */

add_action( 'add_meta_boxes', 'plsm_script_add_custom_box' );


/* Do something with the data entered */
add_action( 'save_post', 'plsm_script_save_custom_box' );

/* Adds a box to the main column on the Post and Page edit screens */
function plsm_script_add_custom_box() {

	$screens = get_post_types( '', 'names' );
	$screens = array_merge( $screens, array( 'post', 'page' ) );

	foreach ( $screens as $screen ) {
		add_meta_box(
			'myplugin_sectionid',
			__( 'Page Level Script Manager', 'plsm_add_script' ),
			'plsm_script_inner_custom_box',
			$screen
		);
	}


}

/* Prints the box content */
function plsm_script_inner_custom_box( $post ) {

	// Use nonce for verification
	wp_nonce_field( plugin_basename( __FILE__ ), 'plsm_add_script_noncename' );

	// The actual fields for data entry
	// Use get_post_meta to retrieve an existing value from the database and use the value for the form
	$value        = get_post_meta( $post->ID, '_plsm_add_script_header', true );
	$value_footer = get_post_meta( $post->ID, '_plsm_add_script_footer', true );
	$hide_header  = get_post_meta( $post->ID, '_plsm_add_script_header_hide', true );
	$hide_footer  = get_post_meta( $post->ID, '_plsm_add_script_footer_hide', true );
	include( 'metaboxes/plsm-header-footer-metabox.php' );

}

/* When the post is saved, saves our custom data */
function plsm_script_save_custom_box( $post_id ) {

	// First we need to check if the current user is authorised to do this action.
	if ( isset( $_POST['post_type'] ) && 'page' == $_POST['post_type'] ) {
		if ( ! current_user_can( 'edit_page', $post_id ) ) {
			return;
		}
	} else {
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
	}

	// Secondly we need to check if the user intended to change this value.
	if ( ! isset( $_POST['plsm_add_script_noncename'] ) || ! wp_verify_nonce( $_POST['plsm_add_script_noncename'], plugin_basename( __FILE__ ) ) ) {
		return;
	}

	// Thirdly we can save the value to the database

	//if saving in a custom table, get post_ID
	$post_ID = isset( $_POST['post_ID'] ) ? $_POST['post_ID'] : - 1;
	//sanitize user input
	$header_script = isset( $_POST['plsm_add_script_header'] ) ? $_POST['plsm_add_script_header'] : '';

	$footer_script = isset( $_POST['plsm_add_script_footer'] ) ? $_POST['plsm_add_script_footer'] : '';
	$hide_header   = isset( $_POST['plsm_add_script_header_hide'] ) ? $_POST['plsm_add_script_header_hide'] : '';
	$hide_footer   = isset( $_POST['plsm_add_script_footer_hide'] ) ? $_POST['plsm_add_script_footer_hide'] : '';

	update_post_meta( $post_ID, '_plsm_add_script_header', $header_script );
	update_post_meta( $post_ID, '_plsm_add_script_footer', $footer_script );
	update_post_meta( $post_ID, '_plsm_add_script_header_hide', $hide_header );
	update_post_meta( $post_ID, '_plsm_add_script_footer_hide', $hide_footer );

}


