=== Page Level Script Manager  ===
Contributors: adept
Tags: javascript, js, header, footer
Requires at least: 3.5
Tested up to: 5.7.2
Stable tag: 3.9
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


Provides a simple way to add javscript code to individual pages, posts, or custom post types header and footer.


== Description ==

Create a simple way to add script or style tags to individual pages, posts, or custom post types in the header and footer.  
