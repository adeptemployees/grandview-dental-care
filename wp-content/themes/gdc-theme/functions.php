<?php
/**
 * The functions
 */

 //show_admin_bar(false);

 // load jquery
 if (!is_admin()) {
    wp_deregister_script('jquery');
    wp_register_script('jquery', ("https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"), false, '1.10.2',true);
    wp_enqueue_script('jquery');
 }

 if( function_exists('acf_add_options_page') ) {
    acf_add_options_page('Grandview Dental Info');
 }


if(!function_exists('get_post_top_ancestor_id')){
/**
 * Gets the id of the topmost ancestor of the current page. Returns the current
 * page's id if there is no parent.
 *
 * @uses object $post
 * @return int
 */
function get_post_top_ancestor_id(){
    global $post;

    if($post->post_parent){
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }

    return $post->ID;
}}

 //log in logo
 function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png);
			background-size:100%;
            padding-bottom: 10px;
			width:201px;
	        }

    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );


//get first image from blog post
function blog_first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches[1][0];

  if(empty($first_img)) {
    return false;
  }
  return $first_img;
}

// Add support for featured images.
require_once('inc/aq_resizer.php');

add_theme_support( 'post-thumbnails');
add_theme_support( 'menus');

	// Register nav
if(function_exists('register_nav_menu')):
 register_nav_menu( 'primary_nav', 'Primary Navigation');
endif;

if(function_exists('register_nav_menu')):
 register_nav_menu( 'secondary_nav', 'Secondary Navigation');
endif;

if(function_exists('register_nav_menu')):
 register_nav_menu( 'footer_nav', 'Footer Navigation');
endif;

//post thumbnail sizes


if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'entry-featured', 300, 000, true); //300 pixels wide (and unlimited height)
}

if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'entry', 200, 200, true ); //300 pixels wide (and unlimited height)
}

if ( function_exists( 'add_image_size' ) ) {
  add_image_size( 'product', 175, 115, false ); //300 pixels wide (and unlimited height)

}

function get_the_slug() {
    $slug = basename(get_permalink());
    return $slug;
}

// Allow SVGS

function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');



// Callback function to insert 'styleselect' into the $buttons array
function my_mce_buttons_2( $buttons ) {
    array_unshift( $buttons, 'styleselect' );
    return $buttons;
}
// Register our callback to the appropriate filter
add_filter('mce_buttons_2', 'my_mce_buttons_2');

// Callback function to filter the MCE settings
function my_mce_before_init_insert_formats( $init_array ) {
    // Define the style_formats array
    $style_formats = array(
        // Each array child is a format with it's own settings
        array(
            'title' => 'Convert link to a Button',
            'selector' => 'a',
            'classes' => 'btn green-btn m-btn'
        )
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $init_array['style_formats'] = json_encode( $style_formats );

    return $init_array;

}
// Attach callback to 'tiny_mce_before_init'
add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );

	// Shortcodes for content sections


function wrapper($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	return '<div class="wrap">'.do_shortcode($content).'</div>';
}
add_shortcode("wrapper", "wrapper");

function content($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	return '<div class="content">'.do_shortcode($content).'</div>';
}
add_shortcode("content", "content");

function quote($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	return '<span class="quote">'.do_shortcode($content).'</span>';
}
add_shortcode("quote", "quote");

function officials($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	return '<div class="officials wrap">'.do_shortcode($content).'</div>';
}
add_shortcode("officials", "officials");

function half_col($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	return '<div class="col-1-2">'.do_shortcode($content).'</div>';
}
add_shortcode("half_col", "half_col");

function third_col($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	return '<div class="col-1-3">'.do_shortcode($content).'</div>';
}
add_shortcode("third_col", "third_col");

function fourth_col($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	return '<div class="col-1-4">'.do_shortcode($content).'</div>';
}
add_shortcode("fourth_col", "fourth_col");

function scholarship($atts, $content = null) {
		$content = preg_replace('#^<\/p>|<p>$#', '', $content);
	return '<div class="content scholarship">'.do_shortcode($content).'</div>';
}
add_shortcode("scholarship", "scholarship");


function btn( $atts, $content = null ) {
	$btn = preg_replace('#^<\/p>|<p>$#', '', $btn);
	extract( shortcode_atts( array(
		'class' => 'skin-1',
		'class' => 'skin-2',
		'class' => 'skin-3',
		'url' => 'url'
	), $atts ) );

	return '<span class="btn ' . esc_attr($class) . '">' . $content . '</span>';
}
add_shortcode("btn", "btn");

function include_social( $atts ) {
    extract( shortcode_atts( array(
        'id' => 1,
        'email_body' => '',
        'title' => '',
        'twitter' => ''
    ), $atts ) );
    $page_id = $id;
    $page_data = get_post( $page_id );
    $title = $title;
    $title = ($title !== "") ? $title : get_the_title($page_data);
    $twitter = $twitter;
    $twitter = ($twitter !== "") ? $twitter : $title;
    $email_body = $email_body;
    return "

    <span st_via='grandviewdental' st_username='grandviewdental' class='st_twitter_large' st_title='".$twitter."' st_url='".get_permalink( $page_data )."'></span>

    <span class='st_facebook_large' st_title='".$title."' st_url='".get_permalink( $page_data )."'></span>

    <span st_title='".$title."' st_url='".get_permalink( $page_data )."' st_summary='".$email_body."' class='st_email_large'></span>

    ";
}
add_shortcode("social", "include_social");

//LIST PAGES

if(!function_exists('get_post_top_ancestor_id')){
/**
 * Gets the id of the topmost ancestor of the current page. Returns the current
 * page's id if there is no parent.
 *
 * @uses object $post
 * @return int
 */
function get_post_top_ancestor_id(){
    global $post;

    if($post->post_parent){
        $ancestors = array_reverse(get_post_ancestors($post->ID));
        return $ancestors[0];
    }

    return $post->ID;
}}


//paginate

function kriesi_pagination($pages = '', $range = 2)
{
     $showitems = ($range * 2)+1;

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }

     if(1 != $pages)
     {
         echo "<div class='pagination clear'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class='current'>".$i."</span>":"<a href='".get_pagenum_link($i)."' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."'>&rsaquo;</a>";
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>&raquo;</a>";
         echo "</div>\n";
     }
}



//force gravity forms script to load in footer
add_filter('gform_init_scripts_footer', '__return_true');


require_once('inc/gravity_forms_merge_tags.php');


//Read More for Excerpt


function get_excerpt($count){
  $permalink = get_permalink($post->ID);
  $excerpt = get_the_content();
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = $excerpt.'';
  return $excerpt;
}


// Replaces the excerpt "more" text by a link
function new_excerpt_more($more) {
       global $post;
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');


//The Excerpt Length

function custom_excerpt_length( $length ) {
	return 50;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );


 // Exclude Category

     function the_category_filter($thelist,$separator=' ') {
        if(!defined('WP_ADMIN')) {
            //Category Names to exclude
            $exclude = array('Uncategorized', 'Featured');

            $cats = explode($separator,$thelist);
            $newlist = array();
            foreach($cats as $cat) {
                $catname = trim(strip_tags($cat));
                if(!in_array($catname,$exclude))
                    $newlist[] = $cat;
            }
            return implode($separator,$newlist);
        } else {
            return $thelist;
        }
    }
    add_filter('the_category','the_category_filter', 2);



//Register Sidebar


function sidebar_widget() {

	register_sidebar( array(
		'name' => 'Sidebar Widget',
		'id' => 'sidebar_widget',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
		'before_title' => '<h4>',
		'after_title' => '</h4>',
	) );
}
add_action( 'widgets_init', 'sidebar_widget' );




//Gravatars

add_filter( 'avatar_defaults', 'new_default_avatar' );

function new_default_avatar ( $avatar_defaults ) {
		//Set the URL where the image file for your avatar is located
		$new_avatar_url = get_bloginfo( 'template_directory' ) . '/images/new_default_avatar.png';
		//Set the text that will appear to the right of your avatar in Settings>>Discussion
		$avatar_defaults[$new_avatar_url] = 'Your New Default Avatar';
		return $avatar_defaults;
}


//Nav Last

function wpb_first_and_last_menu_class($items) {
	$items[1]->classes[] = 'first';
	$items[count($items)]->classes[] = 'last';
	return $items;
}
add_filter('wp_nav_menu_objects', 'wpb_first_and_last_menu_class');

//wp caption remove width

add_shortcode('wp_caption', 'fixed_img_caption_shortcode');
add_shortcode('caption', 'fixed_img_caption_shortcode');
function fixed_img_caption_shortcode($attr, $content = null) {
    if ( ! isset( $attr['caption'] ) ) {
        if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
            $content = $matches[1];
            $attr['caption'] = trim( $matches[2] );
        }
    }
    $output = apply_filters('img_caption_shortcode', '', $attr, $content);
    if ( $output != '' )
        return $output;
    extract(shortcode_atts(array(
        'id'    => '',
        'align' => 'alignnone',
        'width' => '',
        'caption' => ''
    ), $attr));
    if ( 1 > (int) $width || empty($caption) )
        return $content;
    if ( $id ) $id = 'id="' . esc_attr($id) . '" ';
    return '<div ' . $id . 'class="wp-caption ' . esc_attr($align) . '">'
    . do_shortcode( $content ) . '<p class="wp-caption-text">' . $caption . '</p></div>';
}



//

// Add this to your bootstrap, which should be hooked to `after_setup_theme`
# Add Scripts
add_action( 'wp_enqueue_scripts', 'wpse71451_comment_scripts', 0 );
/**
 * Scripts
 * + Comment reply
 *
 * @return void
 */
function wpse71451_comment_scripts()
{
    # Comment reply script
    if (
        is_singular()
        AND get_option( 'thread_comments' )
    )
        wp_enqueue_script( 'comment-reply' );
}


function my_comments_callback( $comment, $args, $depth ) {
    $GLOBALS['comment'] = $comment;

    ?>

    <?php $args = array(
	'walker'            => null,
	'max_depth'         => '100',
	'style'             => 'ul',
	'callback'          => null,
	'end-callback'      => null,
	'type'              => 'all',
	'reply_text'        => 'Reply',
	'page'              => '',
	'per_page'          => '',
	'avatar_size'       => 100,
	'reverse_top_level' => null,
	'reverse_children'  => ''
); ?>

    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
        <div id="comment-<?php comment_ID(); ?>" class="comment comment_single">

        <div class="comment-author vcard">
		<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['avatar_size'] ); ?>
		</div>



            <div class="comment-content">

            <p class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
			<?php
				/* translators: 1: date, 2: time */
				printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
			?>
            </p>
            <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
			<?php comment_text(); ?>
            </div>

            <div class="reply">
			<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </div>

        </div>
    </li>
    <?php
}


//Posts Name

function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Blog';
	$submenu['edit.php'][5][0] = 'Blog';
	$submenu['edit.php'][10][0] = 'Add Blog Post';
	$submenu['edit.php'][16][0] = 'Blog Tags';
	echo '';
}
function change_post_object_label() {
	global $wp_post_types;
	$labels = &$wp_post_types['post']->labels;
	$labels->name = 'Blog';
	$labels->singular_name = 'Blog';
	$labels->add_new = 'Add Blog';
	$labels->add_new_item = 'Add Blog';
	$labels->edit_item = 'Edit Blog';
	$labels->new_item = 'Blog';
	$labels->view_item = 'View Blog';
	$labels->search_items = 'Search Blog';
	$labels->not_found = 'No News found';
	$labels->not_found_in_trash = 'No News found in Trash';
}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );




/*
 * when oembed is youtube or vimeo,
 * wrap code in a class to make it responsive.
 * otherwise just return the typical embed code
 */
function custom_oembed_filter($html, $url, $attr, $post_ID) {
    if(strpos($url,'youtube') !== false || strpos($url,'youtu.be') !== false || strpos($url,'vimeo') !== false) {
        return '<div class="flexible-video">'.$html.'</div>';
    }
    else {
        return $html;
    }
}
add_filter( 'embed_oembed_html', 'custom_oembed_filter', 10, 4 ) ;

//remove emoji scripts from loading
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );


?>
