<div class="sidebar">
  <!--start sidebar-->

  <?php
  $parent = array_reverse(get_post_ancestors($post->ID));
  $first_parent = get_post($parent[0]);
  $top_parent_page_id = $first_parent->ID;
  $top_parent_page_slug = $first_parent->post_name;
  $top_parent_page_title = $first_parent->post_title;

  $args = array(
    'child_of'    => $top_parent_page_id,
    'echo'      => 1,
    'post_type'   => 'page',
    'post_status' => 'publish',
    'show_date'   => '',
    'depth'       => 2,
    'title_li'    => __(''),
    'sort_column' => 'menu_order, post_title'
  );
  $children = get_pages($args);
  if(count($children) > 0 && !get_field('do_not_display_list_of_child_pages')) : ?>

  <div class="widget list-pages">
    <!--start sidebar nav-->
    <nav>
      <ul>
        <li class="parent"><a
            href="<?php echo get_bloginfo('url').'/'.$top_parent_page_slug; ?>"><?php echo strtoupper(str_replace("-", " ", $top_parent_page_title)); ?></a>
        </li>
        <?php wp_list_pages($args); ?>
      </ul>
    </nav>
  </div>
  <!--end widget-->
  <?php endif; ?>

  <?php if( have_rows('widget') ): ?>

  <!--loop through the rows of data-->
  <?php while ( have_rows('widget') ) : the_row(); ?>

  <div
    class="widget <?php echo get_the_slug(); ?> <?php if(get_sub_field('group_this_content_in_a_box')) {echo 'box';} ?>">
    <!--start widget-->

    <?php the_sub_field('content'); ?>

    <?php if( get_sub_field('button_text') ): ?>
    <a href="<?php the_sub_field('button_link');?>" <?php if(get_sub_field('new_window')) {echo 'target="_blank"';} ?>
      class="btn green-btn xl-btn"><?php the_sub_field('button_text');?></a>
    <?php endif; ?>

  </div>
  <!--end widget-->

  <?php endwhile; ?>

  <?php else : ?>
  <!--no content found-->

  <?php endif; ?>

  <div class="widget sidebar-contact-details" itemscope itemtype="http://schema.org/Dentist">
    <!--Schema.org Company Data-->
    <meta itemprop="name" content="Grandview Dental Care">
    <meta itemprop="logo" content="https://www.grandviewdentalcare.com/wp-content/themes/gdc-theme/images/logo.png">
    <meta itemprop="url" content="https://www.grandviewdentalcare.com/">
    <meta itemprop="sameAs" content="https://plus.google.com/+GrandviewDentalCareColumbus/videos">
    <meta itemprop="sameAs" content="https://twitter.com/grandviewdental">
    <meta itemprop="sameAs" content="https://www.facebook.com/grandviewdentalcare">
    <meta itemprop="sameAs" content="https://www.pinterest.com/grandviewdental/">
    <meta itemprop="sameAs" content="https://www.youtube.com/user/GrandviewDental">
    <meta itemprop="sameAs" content="https://instagram.com/GrandviewDental/">
    <meta itemprop="hasMap" content="https://goo.gl/maps/rcw8I">
    <meta itemprop="image" content="https://www.grandviewdentalcare.com/wp-content/themes/gdc-theme/images/logo.png">
    <!--End Schema.org Company Data-->
    <?php if(get_field('display_address_in_sidebar')) : ?>
    <h4>Address</h4>
    <p itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
      <span itemprop="streetAddress"><?php echo get_field('street_address', 'options'); ?></span><br>
      <span itemprop="addressLocality"><?php echo get_field('city', 'options'); ?></span>, <span
        itemprop="addressRegion"><?php echo get_field('state', 'options'); ?></span> <span
        itemprop="postalCode"><?php echo get_field('zip_code', 'options'); ?></span><br>
      <?php if(!is_page(25)) : ?>
      <a href="<?php echo get_page_link(25); ?>">Get Directions &raquo;</a>
      <?php endif; ?>
    </p>
    <?php endif; ?>
    <?php if(get_field('display_phone_number_in_sidebar')) : ?>
    <h4>Phone</h4>
    <p>
      <span itemprop="telephone"><?php echo get_field('phone_number', 'options'); ?></span>
    </p>
    <?php else : ?>
    <span itemprop="telephone"><?php echo get_field('phone_number', 'options'); ?></span>
    <?php endif; ?>
    <?php if(get_field('display_email_address_in_sidebar')) : ?>
    <h4>Email</h4>
    <p>
      <span itemprop="email"><a
          href="mailto:<?php echo get_field('email_address', 'options'); ?>"><?php echo get_field('email_address', 'options'); ?></a></span>
    </p>
    <?php endif; ?>
    <?php if(get_field('display_hours_in_sidebar')) : ?>
    <h4>Office Hours</h4>
    <?php if(get_field('hours', 'options')) : while(has_sub_field('hours', 'options')) : ?>
    <dl class="hours">
      <dt>Monday</dt>
      <dd>9:00 AM - 6:00 PM</dd>
    </dl>
    <dl class="hours">
      <dt>Tuesday</dt>
      <dd>8:00 AM - 6:00 PM</dd>
    </dl>
    <dl class="hours">
      <dt>Wednesday</dt>
      <dd>8:00 AM - 6:00 PM</dd>
    </dl>
    <dl class="hours">
      <dt>Thursday</dt>
      <dd>8:30 AM - 4:30 PM</dd>
    </dl>
    <dl class="hours">
      <dt>Friday</dt>
      <dd>8:00 AM - 2:00 PM</dd>
    </dl>
    <dl class="hours">
      <dt>Saturday</dt>
      <dd>Closed</dd>
    </dl>
    <dl class="hours">
      <dt>Sunday</dt>
      <dd>Closed</dd>
    </dl>
    <time itemprop="openingHours" datetime="Mo 09:00-18:00"></time>
    <time itemprop="openingHours" datetime="Tu 08:00-18:00"></time>
    <time itemprop="openingHours" datetime="We 08:00-18:00"></time>
    <time itemprop="openingHours" datetime="Th 08:30-16:30"></time>
    <time itemprop="openingHours" datetime="Fr 08:00-14:00"></time>

    <?php endwhile; endif; ?>
    <?php endif; ?>
  </div>
</div>