	<?php get_header();?>

	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	 <?php $intro = get_field('page_intro')?>

	<?php
		if(get_the_title() === 'Offers / Promotions') {
			echo '<img style="margin:0 auto 1em; display: block;" src="'.get_bloginfo('template_url').'/images/promo-banner.jpg">';
		}
		else {
			$positionTitle = get_field('position_title');
			$positionTitle !== '' ? '<h2 class="position-title">'.$positionTitle.'</h2>' : '';
			echo '
				<div class="page-title"><!--start page title-->
					<div class="container"><!--start container-->
						<h1>'.get_the_title().'</h1>
						'.$postitionTitle.'
						<div class="has-intro">'.$intro.'</div>
					</div><!--end container-->
				</div><!--end page title-->
			';
		}
	?>

	<div class="page"><!--start page-->

		<div class="main-content-wrap"><!--start main content wrap-->

			<div class="container"><!--start container-->

				<div class="wrap"><!--start wrap-->

					<div class="main-content"><!--start main content-->
						<?php if(has_post_thumbnail()) {
							$img = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium_large' );

							if ($post->post_parent == 17 || $post->post_parent == 6241) {
								$img_med = aq_resize($img[0], 380, 475, true, true, true);
							}
							else{
								$img_med = aq_resize($img[0], 400, 209, true, true, true);
							}

							if($img_med) {
								echo '<div class="feature-image"><img src="'.$img_med.'"></div>';
							} 
							else {
								echo '<div class="feature-image">'.get_the_post_thumbnail($page->ID, 'img_med').'</div>';
							}
						} ?>
						<?php if( have_rows('layout') ):?>

						<!--loop through the rows of data-->
						<?php while ( have_rows('layout') ) : the_row();?>

							<?php if( get_row_layout() == 'general_content' ):?>

								<?php if( get_sub_field('content') ): ?>

									<div class="content row"><!--start content-->

										<?php the_sub_field('content');?>

									</div><!--end content-->

									<?php if(is_page('6241')) : ?>
									<!-- meet the team members -->
									<?php

										$args = array(
											'child_of' => '6241',
											'sort_order' => 'ASC',
											'sort_column' => 'menu_order',
											'post_status' => 'publish'
										);

										$team_members = get_pages($args);
										//var_dump($team_members);

										echo '<div class="team-members">';
										foreach($team_members as $team_member) {
											$img = wp_get_attachment_url( get_post_thumbnail_id($team_member->ID) );
											$img_resize = aq_resize($img, 250, 350, true);
											echo '<div class="team-member"><a href="'.get_page_link($team_member->ID).'">';
											if($img) { echo '<img src="'.$img_resize.'">'; } else { echo '<img src="'.get_bloginfo('template_url').'/images/bio-placeholder.gif">';}
											echo '<h4>'.$team_member->post_title.'<br><small>'.get_field('position_title', $team_member->ID).'</small></h4></a></div>';
										}
										echo '</div>';
									?>

									<?php endif;  //is_page(6241) ?>

								<?php endif; ?>

							<?php elseif( get_row_layout() == 'call_to_action' ):?>

								<div class="content row"><!--start row-->

									<div class="cta"><!--start cta-->
										<div class="bg-image" style="background-image:url('<?php echo the_sub_field('background_image');?>');"></div>
										<?php if(get_sub_field('background_image')) { echo '<div class="overlay"></div>'; } ?>
										<div class="content">
											<?php the_sub_field('content');?>

											<?php if( get_sub_field('button_text') ): ?>
												<a href="<?php the_sub_field('button_link');?>" class="btn green-btn xl-btn"><?php the_sub_field('button_text');?></a>
											<?php endif; ?>
										</div>

									</div><!--end cta-->

								</div><!--end row-->

							<?php elseif( get_row_layout() == 'page_button' ):?>

								<div class="content row"><!--start row-->
									<div class="button-row">

										<?php if( have_rows('button') ):?>

											<!--loop through the rows of data-->
											<?php while ( have_rows('button') ) : the_row();?>

												<?php if( get_sub_field('button_text') ): ?>
													<a href=" <?php if( get_sub_field('button_link') ): ?><?php the_sub_field('button_link');?><?php endif; ?> <?php if( get_sub_field('button_file') ): ?><?php the_sub_field('button_file');?><?php endif; ?>" class="btn <?php the_sub_field('button_color');?> <?php the_sub_field('button_size');?>"><?php the_sub_field('button_text');?></a>

												<?php endif; ?>


											<?php endwhile;?>

										<?php endif; ?>
									</div>
								</div><!--end row-->

							<?php elseif( get_row_layout() == 'video' ):?>

								<div class="content row"><!--start content-->

									<?php if( get_sub_field('video_embed') ): ?>

										<div class="video-wrapper"><!--start video wrapper-->

											<?php the_sub_field('video_embed');?>

										</div><!--end video wrapper-->

									<?php endif; ?>

								</div><!--end content-->

							<?php elseif( get_row_layout() == 'testimonials' ):?>

								<div class="row testimonials"><!--start content-->

									<?php if( have_rows('testimonial') ):?>

										<!--loop through the rows of data-->
										<?php while ( have_rows('testimonial') ) : the_row();?>

											<div class="testimonial content row"><!--start single call out-->

												<?php if( get_sub_field('content') ): ?>

													<?php the_sub_field('content');?>

												<?php endif; ?>

												<?php if( get_sub_field('cite') ): ?>

													<cite>- <?php the_sub_field('cite');?></cite>

												<?php endif; ?>

											</div><!--end testimonial-->

										<?php endwhile;?>

									<?php endif;?>

								</div><!--end content-->

							<?php elseif( get_row_layout() == 'faqs' ):?>

								<div class="faqs content row"><!--start content-->

									<?php if( have_rows('faq_group') ):?>

										<?php while ( have_rows('faq_group') ) : the_row();?>

											<h2 class="faq-title"><?php the_sub_field('faq_group_title');?></h2>

											<div class="single-faq"><!--single faq-->

												<?php if( have_rows('single_faq') ):?>

													<?php while ( have_rows('single_faq') ) : the_row();?>

														<span class="m-btn acc-btn"><?php the_sub_field('faq_title');?> <i class="fa fa-caret-down"></i></span>

														<div class="acc-content">

															<?php the_sub_field('faq_content');?>

														</div><!--end acc content-->

													<?php endwhile;?>

												<?php endif; ?>

											</div><!--end single faq-->

										<?php endwhile;?>

									<?php endif; ?>

								</div><!--end faqs-->


							<?php elseif( get_row_layout() == 'page_slider' ):?>

								<div class="content row page-slider"><!--start content-->

									<div class="slide-wrap"><!--start slide wrap-->

										<ul class="bxslider secondary-slider">

											<?php if( have_rows('slide') ):?>

												<!--loop through the rows of data-->
												<?php while ( have_rows('slide') ) : the_row();?>

													<li>

													<?php if( get_sub_field('slide_image') ): ?>

														<img src="<?php the_sub_field('slide_image');?>">

													<?php endif; ?>

													</li>

												<?php endwhile;?>

											<?php endif;?>

										</ul>

									</div><!--end slide wrap-->

							</div><!--end content-->

							<?php elseif( get_row_layout() == 'half_columns' ):?>

								<div class="content row"><!--start content-->

									<?php if( have_rows('column') ):?>

										<!--check if the repeater field has rows of data-->
										<?php while ( have_rows('column') ) : the_row();?>

											<div class="col-6"><!--start col 6-->

												<!--display a sub field value-->
												<?php if( get_sub_field('content') ): ?>
													<?php the_sub_field('content');?>

												<?php endif; ?>

											</div><!--end col 6-->

										<?php endwhile;?>

									<?php endif;?>

								</div><!--end content-->

								<?php elseif( get_row_layout() == 'third_columns' ):?>

								<div class="content row"><!--start content-->

									<?php if( have_rows('column') ):?>

										<!--check if the repeater field has rows of data-->
										<?php while ( have_rows('column') ) : the_row();?>

											<div class="col-4"><!--start col 6-->

												<!--display a sub field value-->
												<?php if( get_sub_field('content') ): ?>
													<?php the_sub_field('content');?>

												<?php endif; ?>

											</div><!--end col 6-->

										<?php endwhile;?>

									<?php endif;?>

								</div><!--end content-->


							<?php elseif( get_row_layout() == 'service_cards' ):?>

								<div class="content row service-cards">
									<!--start content-->

									<?php if( have_rows('service_cards') ):?>

										<ul class="service-cards__list">

										<!--check if the repeater field has rows of data-->
										<?php while ( have_rows('service_cards') ) : the_row();?>

											<li class="service-cards__card">

												<a href="<?php the_sub_field('service_link');?>"
													class="service-cards__link">

													<?php if (get_sub_field('service_icon')) : ?>
													<div class="service-cards__image-wrapper" style="background-image: url('<?php echo get_sub_field('service_icon')['url']?>')">
														<img src="<?php echo get_sub_field('service_icon')['url']?>" alt="<?php echo get_sub_field('service_icon')['alt']?>"  class="service-cards__img" >
													</div>
													<?php endif; ?>

													<h4 class="service-cards__text">
														<span class="service-cards__text--title"><?php
														if(get_sub_field('service_title')):
															the_sub_field('service_title');
													 	endif;
														?>
														</span>
														<span class="service-cards__text--description"><?php
														if(get_sub_field('service_description')):
															the_sub_field('service_description');
													 	endif;
														?>
														</span>
													</h4>
													<p>
														<span class="btn green-btn xl-btn service-cards__btn">
															Read More
														</span>
													</p>

												</a>
											</li>

										<?php endwhile;?>

										</ul>

									<?php endif;?>

								</div><!--end content-->


							<?php else :?>

								<!--no rows found-->

							<?php endif;?>

						<?php endwhile;?>


						<?php else :?>

							<!--other content goes here-->

						<?php endif;?><!--end flexible content-->

					</div><!--end main content-->

					<?php get_sidebar();?>

				</div><!--end wrap-->

			</div><!--end container-->

		</div><!--end main content wrap-->

	<?php endwhile; endif; ?><!--end loop-->

	<?php get_footer();?>
