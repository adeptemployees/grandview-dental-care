<?php get_header();?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="page-title"><!--start page title-->

		<div class="container"><!--start container-->

		</div><!--end container-->

	</div><!--end page title-->
	
	<div class="page"><!--start page-->

		<div class="main-content-wrap"><!--start main content wrap-->

			<div class="container"><!--start container-->

				<div class="wrap"><!--start wrap-->

					<div class="main-content"><!--start main content-->
						
						<h1><?php the_title(); ?></h1>

						<em><?php the_author_posts_link(); ?>  | <?php echo get_the_date(); ?></em>

						<?php the_content(); ?>

						<?php 
							$orig_post = $post;
							global $post;
							$categories = get_the_category($post->ID);
							if ($categories) {
								$category_ids = array();
								foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
								$args=array(
									'category__in' => $category_ids,
									'post__not_in' => array($post->ID),
									'posts_per_page'=> 4, // Number of related posts that will be shown.
									'caller_get_posts'=>1
								);
								$my_query = new wp_query( $args );
								if( $my_query->have_posts() ) {
									echo '<div class="related-posts"><h4>Related Posts:</h4><ul>';
									while( $my_query->have_posts() ) {
										$my_query->the_post(); ?>
									<li>
										<a href="<?php the_permalink()?>" rel="bookmark" title="<?php the_title(); ?>">
											<?php if ( has_post_thumbnail($post->ID) ) { the_post_thumbnail('thumbnail'); } elseif(blog_first_image()) { echo '<img src="'.aq_resize(blog_first_image(), 150).'">';} else { echo '<span class="no-image"></span>';} ?>
											<span class="title"><?php the_title(); ?></span>
										</a>
									</li>
									<?php
									}
									echo '</ul></div>';
								}
							}
							$post = $orig_post;
							wp_reset_query(); 
						?>
						
						<div class="post-navigation">
							<div class="alignleft">
								<?php previous_post_link('%link', '&laquo; Previous Post', FALSE); ?>
							</div>
							<div class="alignright">
								<?php next_post_link('%link', 'Next Post &raquo;', FALSE); ?>
							</div>
						</div> <!-- navigation -->

					</div><!--end main content-->

					<div class="sidebar"><!--start sidebar-->
						<div class="widget">
							<?php dynamic_sidebar('sidebar_widget'); ?>
						</div> <!-- widget -->
					</div> <!-- sidebar -->

				</div><!--end wrap-->

			</div><!--end container-->

		</div><!--end main content wrap-->

	<?php endwhile; ?> 
<?php endif; ?><!--end loop-->

<?php get_footer();?>