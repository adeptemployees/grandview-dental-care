
<?php
if ('comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) die ('Please do not load this page directly. Thanks!');
if (!empty($post->post_password)) { 
    if ($_COOKIE['wp-postpass_' . COOKIEHASH] != $post->post_password) {
?>

<h2>This Post is Password Protected!</h2>
<p>Please enter the password to view Comments.</p>

<?php return;
    }
}
?>

<?php $altcomment = 'alt'; ?>

<?php  wp_list_comments('type=comment&callback=my_comments_callback&max_depth=X');?>

    <?php if ($post->comment_status == 'open') : ?>
        <!--<p>Add a comment below!</p>-->
    <? else : ?>
        <p>Comments are closed.</p>
    <?php endif; ?>
    





<?php if ('open' == $post->comment_status) : ?>



    <h3 class="comment-title">Leave a Comment</h3>
    <br />
    
    <?php if ( get_option('comment_registration') && !$user_ID ) : ?>
    <p>You must be <a href="<?php bloginfo('url'); ?>/wp-login.php?redirect_to=<?php the_permalink(); ?>">logged in</a> to post a comment.</p>

    <?php else : ?>
    
    <div id="respond">
    <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
    

    
        <?php if ( $user_ID ) : ?>

            <p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>.
            <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Logout</a></p>
        <?php else : ?>

            <p><label for="author"><small>Name <?php if ($req) echo "(required)"; ?></small></label></p>
            <p><input type="text" name="author" id="author" value="<?php echo $comment_author; ?>" size="50" class="input field" /></p>

            <p><label for="email"><small>Mail (will not be published) <?php if ($req) echo "(required)"; ?></small></label></p>
            <p><input type="text" name="email" id="email" value="<?php echo $comment_author_email; ?>" size="50" class="input field" /></p>
            
            <p><label for="url"><small>Website</small></label></p>
            <p><input type="text" name="url" id="url" value="<?php echo $comment_author_url; ?>" size="50"  class="input field"/></p>
            
        <?php endif; ?>
        
        <p><textarea name="comment" id="data" class="field" cols="43" rows="7" tabindex="4"></textarea></p>

        <input class="entry-btn" name="submit" class="send" type="submit" id="submit" tabindex="5" value="Submit Comment" />
        <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
        

        <?php comment_id_fields();?>
        
        <div id="cancel-comment-reply"><small><?php cancel_comment_reply_link() ?></small></div>

    </form>
    </div>
<?php endif; ?>
<?php endif; ?>