
		<?php get_header();?>
			<style>
				.slider.loaded {
					max-height: none !important;
					overflow: visible !important;
				}
			</style>
			<div class="slider" style="max-height:400px;overflow:hidden;"><!--start slider-->

				<ul class="bxslider home-slider <?php if( count(get_field('home_page_slide')) > 1 ) { echo 'activate-slider'; } ?>">

					<!-- check if the repeater field has rows of data-->
					<?php if( have_rows('home_page_slide') ): ?>

					<!--loop through the rows of data-->
					<?php while ( have_rows('home_page_slide') ) : the_row(); ?>
					<?php $img = aq_resize(get_sub_field('slide_background'), 1500, null, true, true, true); ?>
					<?php $hide_content = get_sub_field('hide_slide_content'); ?>
					<?php $height = get_sub_field('slider_height') ? get_sub_field('slider_height').'%' : '50%'; ?>
					<li class="slide <?php echo $hide_content ? 'slide--no-content' : ''; ?>">

					<div class="slide__bg"  style="background-image: url('<?php echo $img; ?>'); <?php echo $hide_content ? 'padding-bottom: '.$height : ''; ?>"></div>
						<?php 
							if($hide_content && get_sub_field('button_link')) {
								echo '<a href="'.get_sub_field('button_link').'" class="slide__cover-link"></a>';
							}
						?>

						<div class="slide-content <?php echo $hide_content ? 'hidden' : ''; ?>"><!--start slide content-->

							<?php if(get_sub_field('slide_title')) : ?><h1><?php the_sub_field('slide_title');?></h1><?php endif; ?>
							
							<?php the_sub_field('slide_content');?>

							<?php if( get_sub_field('button_text') ): ?>
							<a href="<?php the_sub_field('button_link');?>" class="btn ghost-btn sm-btn"><?php the_sub_field('button_text');?></a>
							<?php endif; ?>
							
						</div><!--end slide content-->

					</li>

					<?php endwhile; ?>

					<?php else : ?>

				    <!--no slides found-->

					<?php endif; ?>
					
				</ul>

				<div class="slider-cta"><!--start cta-->


					<div class="cta-icon"><!--start cta icon-->

						<?php $image = get_field('cta_icon');

						if( !empty($image) ): ?>

							<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

						<?php endif; ?>

					</div><!--end cta icon-->

					<div class="cta-info"><!--start cta info-->

						<?php if( get_field('button_text') ): ?>
						<a href="<?php the_field('button_link');?>" class="btn gray-btn xl-btn cta-btn"><?php the_field('button_text');?></a>
						<?php endif; ?>

						<h4><?php the_field('cta_heading');?></h4>

						<h5><?php the_field('cta_sub_heading');?></h5>

					</div><!--end cta info-->
					
				</div><!--end cta-->
				
			</div><!--end slider-->

			<section class="dental-tech container"><!--start dental tech-->

				<h2><?php the_field('dental_intro_title');?></h2>

				<span class="intro"><?php the_field('dental_intro_content');?></span>

				<!-- check if the repeater field has rows of data-->
				<?php if( have_rows('dental_technology_call_out') ): ?>

				<div class="wrap"><!--start tech wrap-->

					<!--loop through the rows of data-->
					<?php while ( have_rows('dental_technology_call_out') ) : the_row(); ?>

					<div class="single-tech"><!--start single tech-->

						<div class="single-tech-content"><!--start single tech content-->

							<?php $image = get_sub_field('image');

							if( !empty($image) ): ?>

								<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

							<?php endif; ?>

							<h3><?php the_sub_field('title');?></h3>

							<span class="intro"><?php the_sub_field('content');?></span>

						</div><!--end single tech content-->

						<?php if( get_sub_field('button_text') ): ?>
						<a href="<?php the_sub_field('button_link');?>" class="btn green-btn xl-btn"><?php the_sub_field('button_text');?></a>
						<?php endif; ?>

					</div><!--end single tech-->

					<?php endwhile; ?>

				</div><!--end tech wrap-->

				<?php else : ?>

				    <!--no call outs found-->

				<?php endif; ?>

			</section><!--end dental tech-->

			<div class="exp-wrap wrap"><!--start wrap-->

				<section class="exp container"><!--start experience-->

					<h2><?php the_field('exp_intro_title');?></h2>

					<span class="intro"><?php the_field('exp_intro_content');?></span>

					<div class="wrap"><!--start wrap-->

						<div class="featured"><!--start featured-->
							<?php
							$media_type = get_field('exp_media_type') ? get_field('exp_media_type') : 'image';

							if($media_type === 'image') {
								$image = get_field('exp_image');
								if(!empty($image) ) {
									echo '<img src="'.$image['url'].'" alt="'.$image['alt'].'" />';
								}
							
							} else if($media_type === 'video') {
								$video = get_field('exp_video');
								if(!empty($video)) {
									echo '<div class="flexible-video">'.$video.'</div>';
								}
							}
							?>

						</div><!--end featured-->

						<div class="desc"><!--start desc-->

							<!-- check if the repeater field has rows of data-->
								<?php if( have_rows('exp_call_outs') ): ?>

								<!--loop through the rows of data-->
								<?php while ( have_rows('exp_call_outs') ) : the_row(); ?>

								<h3><?php the_sub_field('title');?></h3>

								<?php the_sub_field('content');?>

							<?php endwhile; ?>

							<?php else : ?>

						    <!--no slides found-->

							<?php endif; ?>
									
							<a href="<?php the_field('exp_button_link'); ?>" class="btn green-btn l-btn"><?php the_field('exp_button_text'); ?></a>

						</div><!--end desc-->

					</div><!--end wrap-->
					
				</section><!--end experience-->

			</div><!--end wrap-->

			<section class="dental-services container"><!--start dental services-->

				<h2><?php the_field('services_intro_title');?></h2>

				<span class="intro"><?php the_field('services_intro_content');?></span>

				<!-- check if the repeater field has rows of data-->
				<?php if( have_rows('service_call_out') ): ?>

				<div class="wrap service-wrap"><!--start wrap-->

				<!--loop through the rows of data-->
				<?php while ( have_rows('service_call_out') ) : the_row(); ?>


					<div class="service"><!--start service-->

						<?php $image = get_sub_field('icon');?>

						<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" <?php if( get_sub_field('fallback') ): ?>data-fallback="<?php the_sub_field('fallback');?>"<?php endif; ?>>

						<h3><?php the_sub_field('title');?></h3>

						<?php the_sub_field('content');?>

					</div><!--end service-->

					<?php endwhile; ?>

					<?php else : ?>

			    	<!--no services found-->
					
				</div><!--end wrap-->

				<?php endif; ?>

			</section><!--end dental services-->

<?php get_footer();?>