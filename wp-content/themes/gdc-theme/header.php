<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie8"> <![endif]-->
<!--[if gt IE 8]> <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 9]> <!--> <html class="no-js"> <!--><![endif]-->
<head>
	<title><?php wp_title(); ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="author" content="Adept Marketing">
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="google-site-verification" content="IXQM1EqH-81gg_T4UvEiq4zLHLCMDC70bsjfQ-flkN4" />

	<!--RSS and Pingbacks-->
	<link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>">
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" /> 

	<!--style sheets-->
	<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/css/main.css">
	
	<link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@400;700&family=Open+Sans:wght@400;700&display=swap" rel="stylesheet">

	<?php wp_head();?>
	
	<!-- GA -->
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-4008027-1', 'auto');
	  ga('require', 'displayfeatures');
	  ga('send', 'pageview');

	</script>

</head>
<body <?php body_class($class); ?>>

	<!-- Google Tag Manager -->
	<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-MD2ZQM"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-MD2ZQM');</script>
	<!-- End Google Tag Manager -->
	
	<!--[if lt IE 9]>
		<div class="browser-notice">
			<p>You are using an outdated browser. Please <a href="http://browsehappy.com/" target="_blank">update your browser</a> to improve your experience.  </p>
		</div>
		<![endif]-->

		<div class="page-wrap <?php echo get_the_slug(); ?>">

			<header class="main-header"><!--start main header-->
				<div class="covid-banner">
					<p>We are open! <a href="https://www.grandviewdentalcare.com/covid-19-policies/">View our COVID-19 Policies here</a></p>
				</div>
				<div class="secondary-nav-container container"><!--start secondary container-->

					<div class="social"><!--start social-->

						<a target="_blank" title="Twitter" class="fa fa-twitter" href="http://twitter.com/grandviewdental"><span class="hidden">Twitter</span></a>
						<a target="_blank" title="Facebook" class="fa fa-facebook" href="http://facebook.com/grandviewdentalcare"><span class="hidden">Facebook</span></a>
						<a target="_blank" title="Pinterest" class="fa fa-pinterest" href="http://pinterest.com/grandviewdental/"><span class="hidden">Pinterest</span></a>
						<a target="_blank" title="YouTube" class="fa fa-youtube-play" href="http://www.youtube.com/user/GrandviewDental"><span class="hidden">YouTube</span></a>
						<a target="_blank" title="Instagram" class="fa fa-instagram" href="https://instagram.com/GrandviewDental/"><span class="hidden">Instagram</span></a>

					</div><!--end social-->

					<nav class="secondary-nav"><!--start secondary header-->

						<?php wp_nav_menu( array( 'theme_location' => 'secondary_nav','container' => 'false','menu_id' => 'secondary_nav' ) ); ?>

					</nav><!--end secondary header-->

				</div><!--end secondary container-->

				<div class="main-nav-wrap"><!--start main-nav wrap-->

					<div class="main-nav-container container"><!--start main nav container-->

						<div class="logo"><!--start logo-->
							<a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img class="main-logo" src="<?php bloginfo('template_url'); ?>/images/logo.png" alt="Grandview Dental Care" width="633" height="148"></a>
						</div><!--end logo-->

						<div class="content-info-wrap">

							<div class="contact-info"><!--start contact info-->

								<a href="tel:<?php echo the_field('phone_number', 'options'); ?>" class="number"><?php echo the_field('phone_number', 'options'); ?></a> <a href="/get-scheduled/" class="btn green-btn m-btn">Schedule an Appointment</a>

							</div><!--end contact info-->

							<nav class="main-nav"><!--start main nav-->

								<?php wp_nav_menu( array( 'theme_location' => 'primary_nav','container' => 'false','menu_id' => 'primary_nav','after' => '<span class="sub-btn"><i class="fa fa-caret-right"></i></span>') ); ?>

							</nav><!--end main nav-->
							
						</div>

						<span class="menu-btn">
							<span></span>
							<!-- <img src="<?php bloginfo('template_url'); ?>/images/menu-btn.svg" alt="menu" data-fallback = "<?php bloginfo('template_url'); ?>/images/menu-btn.png"> -->
						</span>
						
					</div><!--end main nav container-->

				</div><!--end main nav wrap-->

			</header><!--end main header-->

			<div class="content-wrapper">
