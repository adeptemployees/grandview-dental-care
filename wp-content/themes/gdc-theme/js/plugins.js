/* Modernizr 2.8.3 (Custom Build) | MIT & BSD
 * Build: http://modernizr.com/download/#-inlinesvg-svg-svgclippaths-printshiv-cssclasses-load
 */
(window.Modernizr = (function(a, b, c) {
  function v(a) {
    j.cssText = a;
  }
  function w(a, b) {
    return v(prefixes.join(a + ";") + (b || ""));
  }
  function x(a, b) {
    return typeof a === b;
  }
  function y(a, b) {
    return !!~("" + a).indexOf(b);
  }
  function z(a, b, d) {
    for (var e in a) {
      var f = b[a[e]];
      if (f !== c)
        return d === !1 ? a[e] : x(f, "function") ? f.bind(d || b) : f;
    }
    return !1;
  }
  var d = "2.8.3",
    e = {},
    f = !0,
    g = b.documentElement,
    h = "modernizr",
    i = b.createElement(h),
    j = i.style,
    k,
    l = {}.toString,
    m = { svg: "http://www.w3.org/2000/svg" },
    n = {},
    o = {},
    p = {},
    q = [],
    r = q.slice,
    s,
    t = {}.hasOwnProperty,
    u;
  !x(t, "undefined") && !x(t.call, "undefined")
    ? (u = function(a, b) {
        return t.call(a, b);
      })
    : (u = function(a, b) {
        return b in a && x(a.constructor.prototype[b], "undefined");
      }),
    Function.prototype.bind ||
      (Function.prototype.bind = function(b) {
        var c = this;
        if (typeof c != "function") throw new TypeError();
        var d = r.call(arguments, 1),
          e = function() {
            if (this instanceof e) {
              var a = function() {};
              a.prototype = c.prototype;
              var f = new a(),
                g = c.apply(f, d.concat(r.call(arguments)));
              return Object(g) === g ? g : f;
            }
            return c.apply(b, d.concat(r.call(arguments)));
          };
        return e;
      }),
    (n.svg = function() {
      return (
        !!b.createElementNS && !!b.createElementNS(m.svg, "svg").createSVGRect
      );
    }),
    (n.inlinesvg = function() {
      var a = b.createElement("div");
      return (
        (a.innerHTML = "<svg/>"),
        (a.firstChild && a.firstChild.namespaceURI) == m.svg
      );
    }),
    (n.svgclippaths = function() {
      return (
        !!b.createElementNS &&
        /SVGClipPath/.test(l.call(b.createElementNS(m.svg, "clipPath")))
      );
    });
  for (var A in n)
    u(n, A) &&
      ((s = A.toLowerCase()), (e[s] = n[A]()), q.push((e[s] ? "" : "no-") + s));
  return (
    (e.addTest = function(a, b) {
      if (typeof a == "object") for (var d in a) u(a, d) && e.addTest(d, a[d]);
      else {
        a = a.toLowerCase();
        if (e[a] !== c) return e;
        (b = typeof b == "function" ? b() : b),
          typeof f != "undefined" &&
            f &&
            (g.className += " " + (b ? "" : "no-") + a),
          (e[a] = b);
      }
      return e;
    }),
    v(""),
    (i = k = null),
    (e._version = d),
    (g.className =
      g.className.replace(/(^|\s)no-js(\s|$)/, "$1$2") +
      (f ? " js " + q.join(" ") : "")),
    e
  );
})(this, this.document)),
  (function(a, b) {
    function l(a, b) {
      var c = a.createElement("p"),
        d = a.getElementsByTagName("head")[0] || a.documentElement;
      return (
        (c.innerHTML = "x<style>" + b + "</style>"),
        d.insertBefore(c.lastChild, d.firstChild)
      );
    }
    function m() {
      var a = s.elements;
      return typeof a == "string" ? a.split(" ") : a;
    }
    function n(a) {
      var b = j[a[h]];
      return b || ((b = {}), i++, (a[h] = i), (j[i] = b)), b;
    }
    function o(a, c, d) {
      c || (c = b);
      if (k) return c.createElement(a);
      d || (d = n(c));
      var g;
      return (
        d.cache[a]
          ? (g = d.cache[a].cloneNode())
          : f.test(a)
          ? (g = (d.cache[a] = d.createElem(a)).cloneNode())
          : (g = d.createElem(a)),
        g.canHaveChildren && !e.test(a) && !g.tagUrn ? d.frag.appendChild(g) : g
      );
    }
    function p(a, c) {
      a || (a = b);
      if (k) return a.createDocumentFragment();
      c = c || n(a);
      var d = c.frag.cloneNode(),
        e = 0,
        f = m(),
        g = f.length;
      for (; e < g; e++) d.createElement(f[e]);
      return d;
    }
    function q(a, b) {
      b.cache ||
        ((b.cache = {}),
        (b.createElem = a.createElement),
        (b.createFrag = a.createDocumentFragment),
        (b.frag = b.createFrag())),
        (a.createElement = function(c) {
          return s.shivMethods ? o(c, a, b) : b.createElem(c);
        }),
        (a.createDocumentFragment = Function(
          "h,f",
          "return function(){var n=f.cloneNode(),c=n.createElement;h.shivMethods&&(" +
            m()
              .join()
              .replace(/\w+/g, function(a) {
                return (
                  b.createElem(a), b.frag.createElement(a), 'c("' + a + '")'
                );
              }) +
            ");return n}"
        )(s, b.frag));
    }
    function r(a) {
      a || (a = b);
      var c = n(a);
      return (
        s.shivCSS &&
          !g &&
          !c.hasCSS &&
          (c.hasCSS = !!l(
            a,
            "article,aside,dialog,figcaption,figure,footer,header,hgroup,main,nav,section{display:block}mark{background:#FF0;color:#000}template{display:none}"
          )),
        k || q(a, c),
        a
      );
    }
    function w(a) {
      var b,
        c = a.getElementsByTagName("*"),
        d = c.length,
        e = RegExp("^(?:" + m().join("|") + ")$", "i"),
        f = [];
      while (d--)
        (b = c[d]), e.test(b.nodeName) && f.push(b.applyElement(x(b)));
      return f;
    }
    function x(a) {
      var b,
        c = a.attributes,
        d = c.length,
        e = a.ownerDocument.createElement(u + ":" + a.nodeName);
      while (d--)
        (b = c[d]), b.specified && e.setAttribute(b.nodeName, b.nodeValue);
      return (e.style.cssText = a.style.cssText), e;
    }
    function y(a) {
      var b,
        c = a.split("{"),
        d = c.length,
        e = RegExp(
          "(^|[\\s,>+~])(" + m().join("|") + ")(?=[[\\s,>+~#.:]|$)",
          "gi"
        ),
        f = "$1" + u + "\\:$2";
      while (d--)
        (b = c[d] = c[d].split("}")),
          (b[b.length - 1] = b[b.length - 1].replace(e, f)),
          (c[d] = b.join("}"));
      return c.join("{");
    }
    function z(a) {
      var b = a.length;
      while (b--) a[b].removeNode();
    }
    function A(a) {
      function g() {
        clearTimeout(d._removeSheetTimer), b && b.removeNode(!0), (b = null);
      }
      var b,
        c,
        d = n(a),
        e = a.namespaces,
        f = a.parentWindow;
      return !v || a.printShived
        ? a
        : (typeof e[u] == "undefined" && e.add(u),
          f.attachEvent("onbeforeprint", function() {
            g();
            var d,
              e,
              f,
              h = a.styleSheets,
              i = [],
              j = h.length,
              k = Array(j);
            while (j--) k[j] = h[j];
            while ((f = k.pop()))
              if (!f.disabled && t.test(f.media)) {
                try {
                  (d = f.imports), (e = d.length);
                } catch (m) {
                  e = 0;
                }
                for (j = 0; j < e; j++) k.push(d[j]);
                try {
                  i.push(f.cssText);
                } catch (m) {}
              }
            (i = y(i.reverse().join(""))), (c = w(a)), (b = l(a, i));
          }),
          f.attachEvent("onafterprint", function() {
            z(c),
              clearTimeout(d._removeSheetTimer),
              (d._removeSheetTimer = setTimeout(g, 500));
          }),
          (a.printShived = !0),
          a);
    }
    var c = "3.7.0",
      d = a.html5 || {},
      e = /^<|^(?:button|map|select|textarea|object|iframe|option|optgroup)$/i,
      f = /^(?:a|b|code|div|fieldset|h1|h2|h3|h4|h5|h6|i|label|li|ol|p|q|span|strong|style|table|tbody|td|th|tr|ul)$/i,
      g,
      h = "_html5shiv",
      i = 0,
      j = {},
      k;
    (function() {
      try {
        var a = b.createElement("a");
        (a.innerHTML = "<xyz></xyz>"),
          (g = "hidden" in a),
          (k =
            a.childNodes.length == 1 ||
            (function() {
              b.createElement("a");
              var a = b.createDocumentFragment();
              return (
                typeof a.cloneNode == "undefined" ||
                typeof a.createDocumentFragment == "undefined" ||
                typeof a.createElement == "undefined"
              );
            })());
      } catch (c) {
        (g = !0), (k = !0);
      }
    })();
    var s = {
      elements:
        d.elements ||
        "abbr article aside audio bdi canvas data datalist details dialog figcaption figure footer header hgroup main mark meter nav output progress section summary template time video",
      version: c,
      shivCSS: d.shivCSS !== !1,
      supportsUnknownElements: k,
      shivMethods: d.shivMethods !== !1,
      type: "default",
      shivDocument: r,
      createElement: o,
      createDocumentFragment: p
    };
    (a.html5 = s), r(b);
    var t = /^$|\b(?:all|print)\b/,
      u = "html5shiv",
      v =
        !k &&
        (function() {
          var c = b.documentElement;
          return (
            typeof b.namespaces != "undefined" &&
            typeof b.parentWindow != "undefined" &&
            typeof c.applyElement != "undefined" &&
            typeof c.removeNode != "undefined" &&
            typeof a.attachEvent != "undefined"
          );
        })();
    (s.type += " print"), (s.shivPrint = A), A(b);
  })(this, document),
  (function(a, b, c) {
    function d(a) {
      return "[object Function]" == o.call(a);
    }
    function e(a) {
      return "string" == typeof a;
    }
    function f() {}
    function g(a) {
      return !a || "loaded" == a || "complete" == a || "uninitialized" == a;
    }
    function h() {
      var a = p.shift();
      (q = 1),
        a
          ? a.t
            ? m(function() {
                ("c" == a.t
                  ? B.injectCss
                  : B.injectJs)(a.s, 0, a.a, a.x, a.e, 1);
              }, 0)
            : (a(), h())
          : (q = 0);
    }
    function i(a, c, d, e, f, i, j) {
      function k(b) {
        if (
          !o &&
          g(l.readyState) &&
          ((u.r = o = 1),
          !q && h(),
          (l.onload = l.onreadystatechange = null),
          b)
        ) {
          "img" != a &&
            m(function() {
              t.removeChild(l);
            }, 50);
          for (var d in y[c]) y[c].hasOwnProperty(d) && y[c][d].onload();
        }
      }
      var j = j || B.errorTimeout,
        l = b.createElement(a),
        o = 0,
        r = 0,
        u = { t: d, s: c, e: f, a: i, x: j };
      1 === y[c] && ((r = 1), (y[c] = [])),
        "object" == a ? (l.data = c) : ((l.src = c), (l.type = a)),
        (l.width = l.height = "0"),
        (l.onerror = l.onload = l.onreadystatechange = function() {
          k.call(this, r);
        }),
        p.splice(e, 0, u),
        "img" != a &&
          (r || 2 === y[c]
            ? (t.insertBefore(l, s ? null : n), m(k, j))
            : y[c].push(l));
    }
    function j(a, b, c, d, f) {
      return (
        (q = 0),
        (b = b || "j"),
        e(a)
          ? i("c" == b ? v : u, a, b, this.i++, c, d, f)
          : (p.splice(this.i++, 0, a), 1 == p.length && h()),
        this
      );
    }
    function k() {
      var a = B;
      return (a.loader = { load: j, i: 0 }), a;
    }
    var l = b.documentElement,
      m = a.setTimeout,
      n = b.getElementsByTagName("script")[0],
      o = {}.toString,
      p = [],
      q = 0,
      r = "MozAppearance" in l.style,
      s = r && !!b.createRange().compareNode,
      t = s ? l : n.parentNode,
      l = a.opera && "[object Opera]" == o.call(a.opera),
      l = !!b.attachEvent && !l,
      u = r ? "object" : l ? "script" : "img",
      v = l ? "script" : u,
      w =
        Array.isArray ||
        function(a) {
          return "[object Array]" == o.call(a);
        },
      x = [],
      y = {},
      z = {
        timeout: function(a, b) {
          return b.length && (a.timeout = b[0]), a;
        }
      },
      A,
      B;
    (B = function(a) {
      function b(a) {
        var a = a.split("!"),
          b = x.length,
          c = a.pop(),
          d = a.length,
          c = { url: c, origUrl: c, prefixes: a },
          e,
          f,
          g;
        for (f = 0; f < d; f++)
          (g = a[f].split("=")), (e = z[g.shift()]) && (c = e(c, g));
        for (f = 0; f < b; f++) c = x[f](c);
        return c;
      }
      function g(a, e, f, g, h) {
        var i = b(a),
          j = i.autoCallback;
        i.url
          .split(".")
          .pop()
          .split("?")
          .shift(),
          i.bypass ||
            (e &&
              (e = d(e)
                ? e
                : e[a] ||
                  e[g] ||
                  e[
                    a
                      .split("/")
                      .pop()
                      .split("?")[0]
                  ]),
            i.instead
              ? i.instead(a, e, f, g, h)
              : (y[i.url] ? (i.noexec = !0) : (y[i.url] = 1),
                f.load(
                  i.url,
                  i.forceCSS ||
                    (!i.forceJS &&
                      "css" ==
                        i.url
                          .split(".")
                          .pop()
                          .split("?")
                          .shift())
                    ? "c"
                    : c,
                  i.noexec,
                  i.attrs,
                  i.timeout
                ),
                (d(e) || d(j)) &&
                  f.load(function() {
                    k(),
                      e && e(i.origUrl, h, g),
                      j && j(i.origUrl, h, g),
                      (y[i.url] = 2);
                  })));
      }
      function h(a, b) {
        function c(a, c) {
          if (a) {
            if (e(a))
              c ||
                (j = function() {
                  var a = [].slice.call(arguments);
                  k.apply(this, a), l();
                }),
                g(a, j, b, 0, h);
            else if (Object(a) === a)
              for (n in ((m = (function() {
                var b = 0,
                  c;
                for (c in a) a.hasOwnProperty(c) && b++;
                return b;
              })()),
              a))
                a.hasOwnProperty(n) &&
                  (!c &&
                    !--m &&
                    (d(j)
                      ? (j = function() {
                          var a = [].slice.call(arguments);
                          k.apply(this, a), l();
                        })
                      : (j[n] = (function(a) {
                          return function() {
                            var b = [].slice.call(arguments);
                            a && a.apply(this, b), l();
                          };
                        })(k[n]))),
                  g(a[n], j, b, n, h));
          } else !c && l();
        }
        var h = !!a.test,
          i = a.load || a.both,
          j = a.callback || f,
          k = j,
          l = a.complete || f,
          m,
          n;
        c(h ? a.yep : a.nope, !!i), i && c(i);
      }
      var i,
        j,
        l = this.yepnope.loader;
      if (e(a)) g(a, 0, l, 0);
      else if (w(a))
        for (i = 0; i < a.length; i++)
          (j = a[i]),
            e(j) ? g(j, 0, l, 0) : w(j) ? B(j) : Object(j) === j && h(j, l);
      else Object(a) === a && h(a, l);
    }),
      (B.addPrefix = function(a, b) {
        z[a] = b;
      }),
      (B.addFilter = function(a) {
        x.push(a);
      }),
      (B.errorTimeout = 1e4),
      null == b.readyState &&
        b.addEventListener &&
        ((b.readyState = "loading"),
        b.addEventListener(
          "DOMContentLoaded",
          (A = function() {
            b.removeEventListener("DOMContentLoaded", A, 0),
              (b.readyState = "complete");
          }),
          0
        )),
      (a.yepnope = k()),
      (a.yepnope.executeStack = h),
      (a.yepnope.injectJs = function(a, c, d, e, i, j) {
        var k = b.createElement("script"),
          l,
          o,
          e = e || B.errorTimeout;
        k.src = a;
        for (o in d) k.setAttribute(o, d[o]);
        (c = j ? h : c || f),
          (k.onreadystatechange = k.onload = function() {
            !l &&
              g(k.readyState) &&
              ((l = 1), c(), (k.onload = k.onreadystatechange = null));
          }),
          m(function() {
            l || ((l = 1), c(1));
          }, e),
          i ? k.onload() : n.parentNode.insertBefore(k, n);
      }),
      (a.yepnope.injectCss = function(a, c, d, e, g, i) {
        var e = b.createElement("link"),
          j,
          c = i ? h : c || f;
        (e.href = a), (e.rel = "stylesheet"), (e.type = "text/css");
        for (j in d) e.setAttribute(j, d[j]);
        g || (n.parentNode.insertBefore(e, n), m(c, 0));
      });
  })(this, document),
  (Modernizr.load = function() {
    yepnope.apply(window, [].slice.call(arguments, 0));
  });

/* Respond.js: min/max-width media query polyfill. (c) Scott Jehl. MIT Lic. j.mp/respondjs  */
(function(w) {
  "use strict";

  //exposed namespace
  var respond = {};
  w.respond = respond;

  //define update even in native-mq-supporting browsers, to avoid errors
  respond.update = function() {};

  //define ajax obj
  var requestQueue = [],
    xmlHttp = (function() {
      var xmlhttpmethod = false;
      try {
        xmlhttpmethod = new w.XMLHttpRequest();
      } catch (e) {
        xmlhttpmethod = new w.ActiveXObject("Microsoft.XMLHTTP");
      }
      return function() {
        return xmlhttpmethod;
      };
    })(),
    //tweaked Ajax functions from Quirksmode
    ajax = function(url, callback) {
      var req = xmlHttp();
      if (!req) {
        return;
      }
      req.open("GET", url, true);
      req.onreadystatechange = function() {
        if (
          req.readyState !== 4 ||
          (req.status !== 200 && req.status !== 304)
        ) {
          return;
        }
        callback(req.responseText);
      };
      if (req.readyState === 4) {
        return;
      }
      req.send(null);
    },
    isUnsupportedMediaQuery = function(query) {
      return query
        .replace(respond.regex.minmaxwh, "")
        .match(respond.regex.other);
    };

  //expose for testing
  respond.ajax = ajax;
  respond.queue = requestQueue;
  respond.unsupportedmq = isUnsupportedMediaQuery;
  respond.regex = {
    media: /@media[^\{]+\{([^\{\}]*\{[^\}\{]*\})+/gi,
    keyframes: /@(?:\-(?:o|moz|webkit)\-)?keyframes[^\{]+\{(?:[^\{\}]*\{[^\}\{]*\})+[^\}]*\}/gi,
    comments: /\/\*[^*]*\*+([^/][^*]*\*+)*\//gi,
    urls: /(url\()['"]?([^\/\)'"][^:\)'"]+)['"]?(\))/g,
    findStyles: /@media *([^\{]+)\{([\S\s]+?)$/,
    only: /(only\s+)?([a-zA-Z]+)\s?/,
    minw: /\(\s*min\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/,
    maxw: /\(\s*max\-width\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/,
    minmaxwh: /\(\s*m(in|ax)\-(height|width)\s*:\s*(\s*[0-9\.]+)(px|em)\s*\)/gi,
    other: /\([^\)]*\)/g
  };

  //expose media query support flag for external use
  respond.mediaQueriesSupported =
    w.matchMedia &&
    w.matchMedia("only all") !== null &&
    w.matchMedia("only all").matches;

  //if media queries are supported, exit here
  if (respond.mediaQueriesSupported) {
    return;
  }

  //define vars
  var doc = w.document,
    docElem = doc.documentElement,
    mediastyles = [],
    rules = [],
    appendedEls = [],
    parsedSheets = {},
    resizeThrottle = 30,
    head = doc.getElementsByTagName("head")[0] || docElem,
    base = doc.getElementsByTagName("base")[0],
    links = head.getElementsByTagName("link"),
    lastCall,
    resizeDefer,
    //cached container for 1em value, populated the first time it's needed
    eminpx,
    // returns the value of 1em in pixels
    getEmValue = function() {
      var ret,
        div = doc.createElement("div"),
        body = doc.body,
        originalHTMLFontSize = docElem.style.fontSize,
        originalBodyFontSize = body && body.style.fontSize,
        fakeUsed = false;

      div.style.cssText = "position:absolute;font-size:1em;width:1em";

      if (!body) {
        body = fakeUsed = doc.createElement("body");
        body.style.background = "none";
      }

      // 1em in a media query is the value of the default font size of the browser
      // reset docElem and body to ensure the correct value is returned
      docElem.style.fontSize = "100%";
      body.style.fontSize = "100%";

      body.appendChild(div);

      if (fakeUsed) {
        docElem.insertBefore(body, docElem.firstChild);
      }

      ret = div.offsetWidth;

      if (fakeUsed) {
        docElem.removeChild(body);
      } else {
        body.removeChild(div);
      }

      // restore the original values
      docElem.style.fontSize = originalHTMLFontSize;
      if (originalBodyFontSize) {
        body.style.fontSize = originalBodyFontSize;
      }

      //also update eminpx before returning
      ret = eminpx = parseFloat(ret);

      return ret;
    },
    //enable/disable styles
    applyMedia = function(fromResize) {
      var name = "clientWidth",
        docElemProp = docElem[name],
        currWidth =
          (doc.compatMode === "CSS1Compat" && docElemProp) ||
          doc.body[name] ||
          docElemProp,
        styleBlocks = {},
        lastLink = links[links.length - 1],
        now = new Date().getTime();

      //throttle resize calls
      if (fromResize && lastCall && now - lastCall < resizeThrottle) {
        w.clearTimeout(resizeDefer);
        resizeDefer = w.setTimeout(applyMedia, resizeThrottle);
        return;
      } else {
        lastCall = now;
      }

      for (var i in mediastyles) {
        if (mediastyles.hasOwnProperty(i)) {
          var thisstyle = mediastyles[i],
            min = thisstyle.minw,
            max = thisstyle.maxw,
            minnull = min === null,
            maxnull = max === null,
            em = "em";

          if (!!min) {
            min =
              parseFloat(min) *
              (min.indexOf(em) > -1 ? eminpx || getEmValue() : 1);
          }
          if (!!max) {
            max =
              parseFloat(max) *
              (max.indexOf(em) > -1 ? eminpx || getEmValue() : 1);
          }

          // if there's no media query at all (the () part), or min or max is not null, and if either is present, they're true
          if (
            !thisstyle.hasquery ||
            ((!minnull || !maxnull) &&
              (minnull || currWidth >= min) &&
              (maxnull || currWidth <= max))
          ) {
            if (!styleBlocks[thisstyle.media]) {
              styleBlocks[thisstyle.media] = [];
            }
            styleBlocks[thisstyle.media].push(rules[thisstyle.rules]);
          }
        }
      }

      //remove any existing respond style element(s)
      for (var j in appendedEls) {
        if (appendedEls.hasOwnProperty(j)) {
          if (appendedEls[j] && appendedEls[j].parentNode === head) {
            head.removeChild(appendedEls[j]);
          }
        }
      }
      appendedEls.length = 0;

      //inject active styles, grouped by media type
      for (var k in styleBlocks) {
        if (styleBlocks.hasOwnProperty(k)) {
          var ss = doc.createElement("style"),
            css = styleBlocks[k].join("\n");

          ss.type = "text/css";
          ss.media = k;

          //originally, ss was appended to a documentFragment and sheets were appended in bulk.
          //this caused crashes in IE in a number of circumstances, such as when the HTML element had a bg image set, so appending beforehand seems best. Thanks to @dvelyk for the initial research on this one!
          head.insertBefore(ss, lastLink.nextSibling);

          if (ss.styleSheet) {
            ss.styleSheet.cssText = css;
          } else {
            ss.appendChild(doc.createTextNode(css));
          }

          //push to appendedEls to track for later removal
          appendedEls.push(ss);
        }
      }
    },
    //find media blocks in css text, convert to style blocks
    translate = function(styles, href, media) {
      var qs = styles
          .replace(respond.regex.comments, "")
          .replace(respond.regex.keyframes, "")
          .match(respond.regex.media),
        ql = (qs && qs.length) || 0;

      //try to get CSS path
      href = href.substring(0, href.lastIndexOf("/"));

      var repUrls = function(css) {
          return css.replace(respond.regex.urls, "$1" + href + "$2$3");
        },
        useMedia = !ql && media;

      //if path exists, tack on trailing slash
      if (href.length) {
        href += "/";
      }

      //if no internal queries exist, but media attr does, use that
      //note: this currently lacks support for situations where a media attr is specified on a link AND
      //its associated stylesheet has internal CSS media queries.
      //In those cases, the media attribute will currently be ignored.
      if (useMedia) {
        ql = 1;
      }

      for (var i = 0; i < ql; i++) {
        var fullq, thisq, eachq, eql;

        //media attr
        if (useMedia) {
          fullq = media;
          rules.push(repUrls(styles));
        }
        //parse for styles
        else {
          fullq = qs[i].match(respond.regex.findStyles) && RegExp.$1;
          rules.push(RegExp.$2 && repUrls(RegExp.$2));
        }

        eachq = fullq.split(",");
        eql = eachq.length;

        for (var j = 0; j < eql; j++) {
          thisq = eachq[j];

          if (isUnsupportedMediaQuery(thisq)) {
            continue;
          }

          mediastyles.push({
            media:
              (thisq.split("(")[0].match(respond.regex.only) && RegExp.$2) ||
              "all",
            rules: rules.length - 1,
            hasquery: thisq.indexOf("(") > -1,
            minw:
              thisq.match(respond.regex.minw) &&
              parseFloat(RegExp.$1) + (RegExp.$2 || ""),
            maxw:
              thisq.match(respond.regex.maxw) &&
              parseFloat(RegExp.$1) + (RegExp.$2 || "")
          });
        }
      }

      applyMedia();
    },
    //recurse through request queue, get css text
    makeRequests = function() {
      if (requestQueue.length) {
        var thisRequest = requestQueue.shift();

        ajax(thisRequest.href, function(styles) {
          translate(styles, thisRequest.href, thisRequest.media);
          parsedSheets[thisRequest.href] = true;

          // by wrapping recursive function call in setTimeout
          // we prevent "Stack overflow" error in IE7
          w.setTimeout(function() {
            makeRequests();
          }, 0);
        });
      }
    },
    //loop stylesheets, send text content to translate
    ripCSS = function() {
      for (var i = 0; i < links.length; i++) {
        var sheet = links[i],
          href = sheet.href,
          media = sheet.media,
          isCSS = sheet.rel && sheet.rel.toLowerCase() === "stylesheet";

        //only links plz and prevent re-parsing
        if (!!href && isCSS && !parsedSheets[href]) {
          // selectivizr exposes css through the rawCssText expando
          if (sheet.styleSheet && sheet.styleSheet.rawCssText) {
            translate(sheet.styleSheet.rawCssText, href, media);
            parsedSheets[href] = true;
          } else {
            if (
              (!/^([a-zA-Z:]*\/\/)/.test(href) && !base) ||
              href.replace(RegExp.$1, "").split("/")[0] === w.location.host
            ) {
              // IE7 doesn't handle urls that start with '//' for ajax request
              // manually add in the protocol
              if (href.substring(0, 2) === "//") {
                href = w.location.protocol + href;
              }
              requestQueue.push({
                href: href,
                media: media
              });
            }
          }
        }
      }
      makeRequests();
    };

  //translate CSS
  ripCSS();

  //expose update for re-running respond later on
  respond.update = ripCSS;

  //expose getEmValue
  respond.getEmValue = getEmValue;

  //adjust on resize
  function callMedia() {
    applyMedia(true);
  }

  if (w.addEventListener) {
    w.addEventListener("resize", callMedia, false);
  } else if (w.attachEvent) {
    w.attachEvent("onresize", callMedia);
  }
})(this);

/*
selectivizr v1.0.2 - (c) Keith Clark, freely distributable under the terms 
of the MIT license.

selectivizr.com
*/
/* 
  
Notes about this source
-----------------------

 * The #DEBUG_START and #DEBUG_END comments are used to mark blocks of code
   that will be removed prior to building a final release version (using a
   pre-compression script)
  
  
References:
-----------
 
 * CSS Syntax          : http://www.w3.org/TR/2003/WD-css3-syntax-20030813/#style
 * Selectors           : http://www.w3.org/TR/css3-selectors/#selectors
 * IE Compatability    : http://msdn.microsoft.com/en-us/library/cc351024(VS.85).aspx
 * W3C Selector Tests  : http://www.w3.org/Style/CSS/Test/CSS3/Selectors/current/html/tests/
 
*/

(function(win) {
  // If browser isn't IE, then stop execution! This handles the script
  // being loaded by non IE browsers because the developer didn't use
  // conditional comments.
  if (/*@cc_on!@*/ true) return;

  // =========================== Init Objects ============================

  var doc = document;
  var root = doc.documentElement;
  var xhr = getXHRObject();
  var ieVersion = /MSIE (\d+)/.exec(navigator.userAgent)[1];

  // If were not in standards mode, IE is too old / new or we can't create
  // an XMLHttpRequest object then we should get out now.
  if (
    doc.compatMode != "CSS1Compat" ||
    ieVersion < 6 ||
    ieVersion > 8 ||
    !xhr
  ) {
    return;
  }

  // ========================= Common Objects ============================

  // Compatiable selector engines in order of CSS3 support. Note: '*' is
  // a placholder for the object key name. (basically, crude compression)
  var selectorEngines = {
    NW: "*.Dom.select",
    MooTools: "$$",
    DOMAssistant: "*.$",
    Prototype: "$$",
    YAHOO: "*.util.Selector.query",
    Sizzle: "*",
    jQuery: "*",
    dojo: "*.query"
  };

  var selectorMethod;
  var enabledWatchers = []; // array of :enabled/:disabled elements to poll
  var ie6PatchID = 0; // used to solve ie6's multiple class bug
  var patchIE6MultipleClasses = true; // if true adds class bloat to ie6
  var namespace = "slvzr";

  // Stylesheet parsing regexp's
  var RE_COMMENT = /(\/\*[^*]*\*+([^\/][^*]*\*+)*\/)\s*/g;
  var RE_IMPORT = /@import\s*(?:(?:(?:url\(\s*(['"]?)(.*)\1)\s*\))|(?:(['"])(.*)\3))[^;]*;/g;
  var RE_ASSET_URL = /\burl\(\s*(["']?)(?!data:)([^"')]+)\1\s*\)/g;
  var RE_PSEUDO_STRUCTURAL = /^:(empty|(first|last|only|nth(-last)?)-(child|of-type))$/;
  var RE_PSEUDO_ELEMENTS = /:(:first-(?:line|letter))/g;
  var RE_SELECTOR_GROUP = /(^|})\s*([^\{]*?[\[:][^{]+)/g;
  var RE_SELECTOR_PARSE = /([ +~>])|(:[a-z-]+(?:\(.*?\)+)?)|(\[.*?\])/g;
  var RE_LIBRARY_INCOMPATIBLE_PSEUDOS = /(:not\()?:(hover|enabled|disabled|focus|checked|target|active|visited|first-line|first-letter)\)?/g;
  var RE_PATCH_CLASS_NAME_REPLACE = /[^\w-]/g;

  // HTML UI element regexp's
  var RE_INPUT_ELEMENTS = /^(INPUT|SELECT|TEXTAREA|BUTTON)$/;
  var RE_INPUT_CHECKABLE_TYPES = /^(checkbox|radio)$/;

  // Broken attribute selector implementations (IE7/8 native [^=""], [$=""] and [*=""])
  var BROKEN_ATTR_IMPLEMENTATIONS = ieVersion > 6 ? /[\$\^*]=(['"])\1/ : null;

  // Whitespace normalization regexp's
  var RE_TIDY_TRAILING_WHITESPACE = /([(\[+~])\s+/g;
  var RE_TIDY_LEADING_WHITESPACE = /\s+([)\]+~])/g;
  var RE_TIDY_CONSECUTIVE_WHITESPACE = /\s+/g;
  var RE_TIDY_TRIM_WHITESPACE = /^\s*((?:[\S\s]*\S)?)\s*$/;

  // String constants
  var EMPTY_STRING = "";
  var SPACE_STRING = " ";
  var PLACEHOLDER_STRING = "$1";

  // =========================== Patching ================================

  // --[ patchStyleSheet() ]----------------------------------------------
  // Scans the passed cssText for selectors that require emulation and
  // creates one or more patches for each matched selector.
  function patchStyleSheet(cssText) {
    return cssText
      .replace(RE_PSEUDO_ELEMENTS, PLACEHOLDER_STRING)
      .replace(RE_SELECTOR_GROUP, function(m, prefix, selectorText) {
        var selectorGroups = selectorText.split(",");
        for (var c = 0, cs = selectorGroups.length; c < cs; c++) {
          var selector =
            normalizeSelectorWhitespace(selectorGroups[c]) + SPACE_STRING;
          var patches = [];
          selectorGroups[c] = selector.replace(RE_SELECTOR_PARSE, function(
            match,
            combinator,
            pseudo,
            attribute,
            index
          ) {
            if (combinator) {
              if (patches.length > 0) {
                applyPatches(selector.substring(0, index), patches);
                patches = [];
              }
              return combinator;
            } else {
              var patch = pseudo
                ? patchPseudoClass(pseudo)
                : patchAttribute(attribute);
              if (patch) {
                patches.push(patch);
                return "." + patch.className;
              }
              return match;
            }
          });
        }
        return prefix + selectorGroups.join(",");
      });
  }

  // --[ patchAttribute() ]-----------------------------------------------
  // returns a patch for an attribute selector.
  function patchAttribute(attr) {
    return !BROKEN_ATTR_IMPLEMENTATIONS ||
      BROKEN_ATTR_IMPLEMENTATIONS.test(attr)
      ? { className: createClassName(attr), applyClass: true }
      : null;
  }

  // --[ patchPseudoClass() ]---------------------------------------------
  // returns a patch for a pseudo-class
  function patchPseudoClass(pseudo) {
    var applyClass = true;
    var className = createClassName(pseudo.slice(1));
    var isNegated = pseudo.substring(0, 5) == ":not(";
    var activateEventName;
    var deactivateEventName;

    // if negated, remove :not()
    if (isNegated) {
      pseudo = pseudo.slice(5, -1);
    }

    // bracket contents are irrelevant - remove them
    var bracketIndex = pseudo.indexOf("(");
    if (bracketIndex > -1) {
      pseudo = pseudo.substring(0, bracketIndex);
    }

    // check we're still dealing with a pseudo-class
    if (pseudo.charAt(0) == ":") {
      switch (pseudo.slice(1)) {
        case "root":
          applyClass = function(e) {
            return isNegated ? e != root : e == root;
          };
          break;

        case "target":
          // :target is only supported in IE8
          if (ieVersion == 8) {
            applyClass = function(e) {
              var handler = function() {
                var hash = location.hash;
                var hashID = hash.slice(1);
                return isNegated
                  ? hash == EMPTY_STRING || e.id != hashID
                  : hash != EMPTY_STRING && e.id == hashID;
              };
              addEvent(win, "hashchange", function() {
                toggleElementClass(e, className, handler());
              });
              return handler();
            };
            break;
          }
          return false;

        case "checked":
          applyClass = function(e) {
            if (RE_INPUT_CHECKABLE_TYPES.test(e.type)) {
              addEvent(e, "propertychange", function() {
                if (event.propertyName == "checked") {
                  toggleElementClass(e, className, e.checked !== isNegated);
                }
              });
            }
            return e.checked !== isNegated;
          };
          break;

        case "disabled":
          isNegated = !isNegated;

        case "enabled":
          applyClass = function(e) {
            if (RE_INPUT_ELEMENTS.test(e.tagName)) {
              addEvent(e, "propertychange", function() {
                if (event.propertyName == "$disabled") {
                  toggleElementClass(e, className, e.$disabled === isNegated);
                }
              });
              enabledWatchers.push(e);
              e.$disabled = e.disabled;
              return e.disabled === isNegated;
            }
            return pseudo == ":enabled" ? isNegated : !isNegated;
          };
          break;

        case "focus":
          activateEventName = "focus";
          deactivateEventName = "blur";

        case "hover":
          if (!activateEventName) {
            activateEventName = "mouseenter";
            deactivateEventName = "mouseleave";
          }
          applyClass = function(e) {
            addEvent(
              e,
              isNegated ? deactivateEventName : activateEventName,
              function() {
                toggleElementClass(e, className, true);
              }
            );
            addEvent(
              e,
              isNegated ? activateEventName : deactivateEventName,
              function() {
                toggleElementClass(e, className, false);
              }
            );
            return isNegated;
          };
          break;

        // everything else
        default:
          // If we don't support this pseudo-class don't create
          // a patch for it
          if (!RE_PSEUDO_STRUCTURAL.test(pseudo)) {
            return false;
          }
          break;
      }
    }
    return { className: className, applyClass: applyClass };
  }

  // --[ applyPatches() ]-------------------------------------------------
  // uses the passed selector text to find DOM nodes and patch them
  function applyPatches(selectorText, patches) {
    var elms;

    // Although some selector libraries can find :checked :enabled etc.
    // we need to find all elements that could have that state because
    // it can be changed by the user.
    var domSelectorText = selectorText.replace(
      RE_LIBRARY_INCOMPATIBLE_PSEUDOS,
      EMPTY_STRING
    );

    // If the dom selector equates to an empty string or ends with
    // whitespace then we need to append a universal selector (*) to it.
    if (
      domSelectorText == EMPTY_STRING ||
      domSelectorText.charAt(domSelectorText.length - 1) == SPACE_STRING
    ) {
      domSelectorText += "*";
    }

    // Ensure we catch errors from the selector library
    try {
      elms = selectorMethod(domSelectorText);
    } catch (ex) {
      // #DEBUG_START
      log("Selector '" + selectorText + "' threw exception '" + ex + "'");
      // #DEBUG_END
    }

    if (elms) {
      for (var d = 0, dl = elms.length; d < dl; d++) {
        var elm = elms[d];
        var cssClasses = elm.className;
        for (var f = 0, fl = patches.length; f < fl; f++) {
          var patch = patches[f];

          if (!hasPatch(elm, patch)) {
            if (
              patch.applyClass &&
              (patch.applyClass === true || patch.applyClass(elm) === true)
            ) {
              cssClasses = toggleClass(cssClasses, patch.className, true);
            }
          }
        }
        elm.className = cssClasses;
      }
    }
  }

  // --[ hasPatch() ]-----------------------------------------------------
  // checks for the exsistence of a patch on an element
  function hasPatch(elm, patch) {
    return new RegExp("(^|\\s)" + patch.className + "(\\s|$)").test(
      elm.className
    );
  }

  // =========================== Utility =================================

  function createClassName(className) {
    return (
      namespace +
      "-" +
      (ieVersion == 6 && patchIE6MultipleClasses
        ? ie6PatchID++
        : className.replace(RE_PATCH_CLASS_NAME_REPLACE, function(a) {
            return a.charCodeAt(0);
          }))
    );
  }

  // --[ log() ]----------------------------------------------------------
  // #DEBUG_START
  function log(message) {
    if (win.console) {
      win.console.log(message);
    }
  }
  // #DEBUG_END

  // --[ trim() ]---------------------------------------------------------
  // removes leading, trailing whitespace from a string
  function trim(text) {
    return text.replace(RE_TIDY_TRIM_WHITESPACE, PLACEHOLDER_STRING);
  }

  // --[ normalizeWhitespace() ]------------------------------------------
  // removes leading, trailing and consecutive whitespace from a string
  function normalizeWhitespace(text) {
    return trim(text).replace(RE_TIDY_CONSECUTIVE_WHITESPACE, SPACE_STRING);
  }

  // --[ normalizeSelectorWhitespace() ]----------------------------------
  // tidies whitespace around selector brackets and combinators
  function normalizeSelectorWhitespace(selectorText) {
    return normalizeWhitespace(
      selectorText
        .replace(RE_TIDY_TRAILING_WHITESPACE, PLACEHOLDER_STRING)
        .replace(RE_TIDY_LEADING_WHITESPACE, PLACEHOLDER_STRING)
    );
  }

  // --[ toggleElementClass() ]-------------------------------------------
  // toggles a single className on an element
  function toggleElementClass(elm, className, on) {
    var oldClassName = elm.className;
    var newClassName = toggleClass(oldClassName, className, on);
    if (newClassName != oldClassName) {
      elm.className = newClassName;
      elm.parentNode.className += EMPTY_STRING;
    }
  }

  // --[ toggleClass() ]--------------------------------------------------
  // adds / removes a className from a string of classNames. Used to
  // manage multiple class changes without forcing a DOM redraw
  function toggleClass(classList, className, on) {
    var re = RegExp("(^|\\s)" + className + "(\\s|$)");
    var classExists = re.test(classList);
    if (on) {
      return classExists ? classList : classList + SPACE_STRING + className;
    } else {
      return classExists
        ? trim(classList.replace(re, PLACEHOLDER_STRING))
        : classList;
    }
  }

  // --[ addEvent() ]-----------------------------------------------------
  function addEvent(elm, eventName, eventHandler) {
    elm.attachEvent("on" + eventName, eventHandler);
  }

  // --[ getXHRObject() ]-------------------------------------------------
  function getXHRObject() {
    if (win.XMLHttpRequest) {
      return new XMLHttpRequest();
    }
    try {
      return new ActiveXObject("Microsoft.XMLHTTP");
    } catch (e) {
      return null;
    }
  }

  // --[ loadStyleSheet() ]-----------------------------------------------
  function loadStyleSheet(url) {
    xhr.open("GET", url, false);
    xhr.send();
    return xhr.status == 200 ? xhr.responseText : EMPTY_STRING;
  }

  // --[ resolveUrl() ]---------------------------------------------------
  // Converts a URL fragment to a fully qualified URL using the specified
  // context URL. Returns null if same-origin policy is broken
  function resolveUrl(url, contextUrl) {
    function getProtocolAndHost(url) {
      return url.substring(0, url.indexOf("/", 8));
    }

    // absolute path
    if (/^https?:\/\//i.test(url)) {
      return getProtocolAndHost(contextUrl) == getProtocolAndHost(url)
        ? url
        : null;
    }

    // root-relative path
    if (url.charAt(0) == "/") {
      return getProtocolAndHost(contextUrl) + url;
    }

    // relative path
    var contextUrlPath = contextUrl.split(/[?#]/)[0]; // ignore query string in the contextUrl
    if (
      url.charAt(0) != "?" &&
      contextUrlPath.charAt(contextUrlPath.length - 1) != "/"
    ) {
      contextUrlPath = contextUrlPath.substring(
        0,
        contextUrlPath.lastIndexOf("/") + 1
      );
    }

    return contextUrlPath + url;
  }

  // --[ parseStyleSheet() ]----------------------------------------------
  // Downloads the stylesheet specified by the URL, removes it's comments
  // and recursivly replaces @import rules with their contents, ultimately
  // returning the full cssText.
  function parseStyleSheet(url) {
    if (url) {
      return loadStyleSheet(url)
        .replace(RE_COMMENT, EMPTY_STRING)
        .replace(RE_IMPORT, function(
          match,
          quoteChar,
          importUrl,
          quoteChar2,
          importUrl2
        ) {
          return parseStyleSheet(resolveUrl(importUrl || importUrl2, url));
        })
        .replace(RE_ASSET_URL, function(match, quoteChar, assetUrl) {
          quoteChar = quoteChar || EMPTY_STRING;
          return (
            " url(" + quoteChar + resolveUrl(assetUrl, url) + quoteChar + ") "
          );
        });
    }
    return EMPTY_STRING;
  }

  // --[ init() ]---------------------------------------------------------
  function init() {
    // honour the <base> tag
    var url, stylesheet;
    var baseTags = doc.getElementsByTagName("BASE");
    var baseUrl = baseTags.length > 0 ? baseTags[0].href : doc.location.href;

    /* Note: This code prevents IE from freezing / crashing when using 
		@font-face .eot files but it modifies the <head> tag and could
		trigger the IE stylesheet limit. It will also cause FOUC issues.
		If you choose to use it, make sure you comment out the for loop 
		directly below this comment.

		var head = doc.getElementsByTagName("head")[0];
		for (var c=doc.styleSheets.length-1; c>=0; c--) {
			stylesheet = doc.styleSheets[c]
			head.appendChild(doc.createElement("style"))
			var patchedStylesheet = doc.styleSheets[doc.styleSheets.length-1];
			
			if (stylesheet.href != EMPTY_STRING) {
				url = resolveUrl(stylesheet.href, baseUrl)
				if (url) {
					patchedStylesheet.cssText = patchStyleSheet( parseStyleSheet( url ) )
					stylesheet.disabled = true
					setTimeout( function () {
						stylesheet.owningElement.parentNode.removeChild(stylesheet.owningElement)
					})
				}
			}
		}
		*/

    for (var c = 0; c < doc.styleSheets.length; c++) {
      stylesheet = doc.styleSheets[c];
      if (stylesheet.href != EMPTY_STRING) {
        url = resolveUrl(stylesheet.href, baseUrl);
        if (url) {
          stylesheet.cssText = patchStyleSheet(parseStyleSheet(url));
        }
      }
    }

    // :enabled & :disabled polling script (since we can't hook
    // onpropertychange event when an element is disabled)
    if (enabledWatchers.length > 0) {
      setInterval(function() {
        for (var c = 0, cl = enabledWatchers.length; c < cl; c++) {
          var e = enabledWatchers[c];
          if (e.disabled !== e.$disabled) {
            if (e.disabled) {
              e.disabled = false;
              e.$disabled = true;
              e.disabled = true;
            } else {
              e.$disabled = e.disabled;
            }
          }
        }
      }, 250);
    }
  }

  // Bind selectivizr to the ContentLoaded event.
  ContentLoaded(win, function() {
    // Determine the "best fit" selector engine
    for (var engine in selectorEngines) {
      var members,
        member,
        context = win;
      if (win[engine]) {
        members = selectorEngines[engine].replace("*", engine).split(".");
        while ((member = members.shift()) && (context = context[member])) {}
        if (typeof context == "function") {
          selectorMethod = context;
          init();
          return;
        }
      }
    }
  });

  /*!
   * ContentLoaded.js by Diego Perini, modified for IE<9 only (to save space)
   *
   * Author: Diego Perini (diego.perini at gmail.com)
   * Summary: cross-browser wrapper for DOMContentLoaded
   * Updated: 20101020
   * License: MIT
   * Version: 1.2
   *
   * URL:
   * http://javascript.nwbox.com/ContentLoaded/
   * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
   *
   */

  // @w window reference
  // @f function reference
  function ContentLoaded(win, fn) {
    var done = false,
      top = true,
      init = function(e) {
        if (e.type == "readystatechange" && doc.readyState != "complete")
          return;
        (e.type == "load" ? win : doc).detachEvent("on" + e.type, init, false);
        if (!done && (done = true)) fn.call(win, e.type || e);
      },
      poll = function() {
        try {
          root.doScroll("left");
        } catch (e) {
          setTimeout(poll, 50);
          return;
        }
        init("poll");
      };

    if (doc.readyState == "complete") fn.call(win, EMPTY_STRING);
    else {
      if (doc.createEventObject && root.doScroll) {
        try {
          top = !win.frameElement;
        } catch (e) {}
        if (top) poll();
      }
      addEvent(doc, "readystatechange", init);
      addEvent(win, "load", init);
    }
  }
})(this);

/*
 * jQuery Easing v1.3 - http://gsgd.co.uk/sandbox/jquery/easing/
 *
 * Uses the built in easing capabilities added In jQuery 1.1
 * to offer multiple easing options
 *
 * TERMS OF USE - jQuery Easing
 *
 * Open source under the BSD License.
 *
 * Copyright © 2008 George McGinley Smith
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 *
 * Neither the name of the author nor the names of contributors may be used to endorse
 * or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

// t: current time, b: begInnIng value, c: change In value, d: duration
jQuery.easing["jswing"] = jQuery.easing["swing"];

jQuery.extend(jQuery.easing, {
  def: "easeOutQuad",
  swing: function(x, t, b, c, d) {
    //alert(jQuery.easing.default);
    return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
  },
  easeInQuad: function(x, t, b, c, d) {
    return c * (t /= d) * t + b;
  },
  easeOutQuad: function(x, t, b, c, d) {
    return -c * (t /= d) * (t - 2) + b;
  },
  easeInOutQuad: function(x, t, b, c, d) {
    if ((t /= d / 2) < 1) return (c / 2) * t * t + b;
    return (-c / 2) * (--t * (t - 2) - 1) + b;
  },
  easeInCubic: function(x, t, b, c, d) {
    return c * (t /= d) * t * t + b;
  },
  easeOutCubic: function(x, t, b, c, d) {
    return c * ((t = t / d - 1) * t * t + 1) + b;
  },
  easeInOutCubic: function(x, t, b, c, d) {
    if ((t /= d / 2) < 1) return (c / 2) * t * t * t + b;
    return (c / 2) * ((t -= 2) * t * t + 2) + b;
  },
  easeInQuart: function(x, t, b, c, d) {
    return c * (t /= d) * t * t * t + b;
  },
  easeOutQuart: function(x, t, b, c, d) {
    return -c * ((t = t / d - 1) * t * t * t - 1) + b;
  },
  easeInOutQuart: function(x, t, b, c, d) {
    if ((t /= d / 2) < 1) return (c / 2) * t * t * t * t + b;
    return (-c / 2) * ((t -= 2) * t * t * t - 2) + b;
  },
  easeInQuint: function(x, t, b, c, d) {
    return c * (t /= d) * t * t * t * t + b;
  },
  easeOutQuint: function(x, t, b, c, d) {
    return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
  },
  easeInOutQuint: function(x, t, b, c, d) {
    if ((t /= d / 2) < 1) return (c / 2) * t * t * t * t * t + b;
    return (c / 2) * ((t -= 2) * t * t * t * t + 2) + b;
  },
  easeInSine: function(x, t, b, c, d) {
    return -c * Math.cos((t / d) * (Math.PI / 2)) + c + b;
  },
  easeOutSine: function(x, t, b, c, d) {
    return c * Math.sin((t / d) * (Math.PI / 2)) + b;
  },
  easeInOutSine: function(x, t, b, c, d) {
    return (-c / 2) * (Math.cos((Math.PI * t) / d) - 1) + b;
  },
  easeInExpo: function(x, t, b, c, d) {
    return t == 0 ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
  },
  easeOutExpo: function(x, t, b, c, d) {
    return t == d ? b + c : c * (-Math.pow(2, (-10 * t) / d) + 1) + b;
  },
  easeInOutExpo: function(x, t, b, c, d) {
    if (t == 0) return b;
    if (t == d) return b + c;
    if ((t /= d / 2) < 1) return (c / 2) * Math.pow(2, 10 * (t - 1)) + b;
    return (c / 2) * (-Math.pow(2, -10 * --t) + 2) + b;
  },
  easeInCirc: function(x, t, b, c, d) {
    return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
  },
  easeOutCirc: function(x, t, b, c, d) {
    return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
  },
  easeInOutCirc: function(x, t, b, c, d) {
    if ((t /= d / 2) < 1) return (-c / 2) * (Math.sqrt(1 - t * t) - 1) + b;
    return (c / 2) * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
  },
  easeInElastic: function(x, t, b, c, d) {
    var s = 1.70158;
    var p = 0;
    var a = c;
    if (t == 0) return b;
    if ((t /= d) == 1) return b + c;
    if (!p) p = d * 0.3;
    if (a < Math.abs(c)) {
      a = c;
      var s = p / 4;
    } else var s = (p / (2 * Math.PI)) * Math.asin(c / a);
    return (
      -(
        a *
        Math.pow(2, 10 * (t -= 1)) *
        Math.sin(((t * d - s) * (2 * Math.PI)) / p)
      ) + b
    );
  },
  easeOutElastic: function(x, t, b, c, d) {
    var s = 1.70158;
    var p = 0;
    var a = c;
    if (t == 0) return b;
    if ((t /= d) == 1) return b + c;
    if (!p) p = d * 0.3;
    if (a < Math.abs(c)) {
      a = c;
      var s = p / 4;
    } else var s = (p / (2 * Math.PI)) * Math.asin(c / a);
    return (
      a * Math.pow(2, -10 * t) * Math.sin(((t * d - s) * (2 * Math.PI)) / p) +
      c +
      b
    );
  },
  easeInOutElastic: function(x, t, b, c, d) {
    var s = 1.70158;
    var p = 0;
    var a = c;
    if (t == 0) return b;
    if ((t /= d / 2) == 2) return b + c;
    if (!p) p = d * (0.3 * 1.5);
    if (a < Math.abs(c)) {
      a = c;
      var s = p / 4;
    } else var s = (p / (2 * Math.PI)) * Math.asin(c / a);
    if (t < 1)
      return (
        -0.5 *
          (a *
            Math.pow(2, 10 * (t -= 1)) *
            Math.sin(((t * d - s) * (2 * Math.PI)) / p)) +
        b
      );
    return (
      a *
        Math.pow(2, -10 * (t -= 1)) *
        Math.sin(((t * d - s) * (2 * Math.PI)) / p) *
        0.5 +
      c +
      b
    );
  },
  easeInBack: function(x, t, b, c, d, s) {
    if (s == undefined) s = 1.70158;
    return c * (t /= d) * t * ((s + 1) * t - s) + b;
  },
  easeOutBack: function(x, t, b, c, d, s) {
    if (s == undefined) s = 1.70158;
    return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
  },
  easeInOutBack: function(x, t, b, c, d, s) {
    if (s == undefined) s = 1.70158;
    if ((t /= d / 2) < 1)
      return (c / 2) * (t * t * (((s *= 1.525) + 1) * t - s)) + b;
    return (c / 2) * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + b;
  },
  easeInBounce: function(x, t, b, c, d) {
    return c - jQuery.easing.easeOutBounce(x, d - t, 0, c, d) + b;
  },
  easeOutBounce: function(x, t, b, c, d) {
    if ((t /= d) < 1 / 2.75) {
      return c * (7.5625 * t * t) + b;
    } else if (t < 2 / 2.75) {
      return c * (7.5625 * (t -= 1.5 / 2.75) * t + 0.75) + b;
    } else if (t < 2.5 / 2.75) {
      return c * (7.5625 * (t -= 2.25 / 2.75) * t + 0.9375) + b;
    } else {
      return c * (7.5625 * (t -= 2.625 / 2.75) * t + 0.984375) + b;
    }
  },
  easeInOutBounce: function(x, t, b, c, d) {
    if (t < d / 2)
      return jQuery.easing.easeInBounce(x, t * 2, 0, c, d) * 0.5 + b;
    return (
      jQuery.easing.easeOutBounce(x, t * 2 - d, 0, c, d) * 0.5 + c * 0.5 + b
    );
  }
});

/*
 *
 * TERMS OF USE - EASING EQUATIONS
 *
 * Open source under the BSD License.
 *
 * Copyright © 2001 Robert Penner
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list of
 * conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright notice, this list
 * of conditions and the following disclaimer in the documentation and/or other materials
 * provided with the distribution.
 *
 * Neither the name of the author nor the names of contributors may be used to endorse
 * or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 *  COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 *  GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
 * AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 *  NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
/***
 * BxSlider v4.2.3 - Fully loaded, responsive content slider
 * http://bxslider.com
 *
 * Copyright 2014, Steven Wanderski - http://stevenwanderski.com - http://bxcreative.com
 * Written while drinking Belgian ales and listening to jazz
 *
 * Released under the MIT license - http://opensource.org/licenses/MIT
 ***/
!(function(e) {
  var t = {
    mode: "horizontal",
    slideSelector: "",
    infiniteLoop: !0,
    hideControlOnEnd: !1,
    speed: 500,
    easing: null,
    slideMargin: 0,
    startSlide: 0,
    randomStart: !1,
    captions: !1,
    ticker: !1,
    tickerHover: !1,
    adaptiveHeight: !1,
    adaptiveHeightSpeed: 500,
    video: !1,
    useCSS: !0,
    preloadImages: "visible",
    responsive: !0,
    slideZIndex: 50,
    wrapperClass: "bx-wrapper",
    touchEnabled: !0,
    swipeThreshold: 50,
    oneToOneTouch: !0,
    preventDefaultSwipeX: !0,
    preventDefaultSwipeY: !1,
    keyboardEnabled: !1,
    pager: !0,
    pagerType: "full",
    pagerShortSeparator: " / ",
    pagerSelector: null,
    buildPager: null,
    pagerCustom: null,
    controls: !0,
    nextText: "Next",
    prevText: "Prev",
    nextSelector: null,
    prevSelector: null,
    autoControls: !1,
    startText: "Start",
    stopText: "Stop",
    autoControlsCombine: !1,
    autoControlsSelector: null,
    auto: !1,
    pause: 4e3,
    autoStart: !0,
    autoDirection: "next",
    autoHover: !1,
    autoDelay: 0,
    autoSlideForOnePage: !1,
    minSlides: 1,
    maxSlides: 1,
    moveSlides: 0,
    slideWidth: 0,
    onSliderLoad: function() {
      return !0;
    },
    onSlideBefore: function() {
      return !0;
    },
    onSlideAfter: function() {
      return !0;
    },
    onSlideNext: function() {
      return !0;
    },
    onSlidePrev: function() {
      return !0;
    },
    onSliderResize: function() {
      return !0;
    }
  };
  e.fn.bxSlider = function(n) {
    if (0 === this.length) return this;
    if (this.length > 1)
      return (
        this.each(function() {
          e(this).bxSlider(n);
        }),
        this
      );
    var s = {},
      o = this,
      r = e(window).width(),
      a = e(window).height(),
      l = function() {
        (s.settings = e.extend({}, t, n)),
          (s.settings.slideWidth = parseInt(s.settings.slideWidth)),
          (s.children = o.children(s.settings.slideSelector)),
          s.children.length < s.settings.minSlides &&
            (s.settings.minSlides = s.children.length),
          s.children.length < s.settings.maxSlides &&
            (s.settings.maxSlides = s.children.length),
          s.settings.randomStart &&
            (s.settings.startSlide = Math.floor(
              Math.random() * s.children.length
            )),
          (s.active = { index: s.settings.startSlide }),
          (s.carousel =
            s.settings.minSlides > 1 || s.settings.maxSlides > 1 ? !0 : !1),
          s.carousel && (s.settings.preloadImages = "all"),
          (s.minThreshold =
            s.settings.minSlides * s.settings.slideWidth +
            (s.settings.minSlides - 1) * s.settings.slideMargin),
          (s.maxThreshold =
            s.settings.maxSlides * s.settings.slideWidth +
            (s.settings.maxSlides - 1) * s.settings.slideMargin),
          (s.working = !1),
          (s.controls = {}),
          (s.interval = null),
          (s.animProp = "vertical" === s.settings.mode ? "top" : "left"),
          (s.usingCSS =
            s.settings.useCSS &&
            "fade" !== s.settings.mode &&
            (function() {
              var e = document.createElement("div"),
                t = [
                  "WebkitPerspective",
                  "MozPerspective",
                  "OPerspective",
                  "msPerspective"
                ];
              for (var i in t)
                if (void 0 !== e.style[t[i]])
                  return (
                    (s.cssPrefix = t[i]
                      .replace("Perspective", "")
                      .toLowerCase()),
                    (s.animProp = "-" + s.cssPrefix + "-transform"),
                    !0
                  );
              return !1;
            })()),
          "vertical" === s.settings.mode &&
            (s.settings.maxSlides = s.settings.minSlides),
          o.data("origStyle", o.attr("style")),
          o.children(s.settings.slideSelector).each(function() {
            e(this).data("origStyle", e(this).attr("style"));
          }),
          d();
      },
      d = function() {
        o.wrap(
          '<div class="' +
            s.settings.wrapperClass +
            '"><div class="bx-viewport"></div></div>'
        ),
          (s.viewport = o.parent()),
          (s.loader = e('<div class="bx-loading" />')),
          s.viewport.prepend(s.loader),
          o.css({
            width:
              "horizontal" === s.settings.mode
                ? 1e3 * s.children.length + 215 + "%"
                : "auto",
            position: "absolute"
          }),
          s.usingCSS && s.settings.easing
            ? o.css(
                "-" + s.cssPrefix + "-transition-timing-function",
                s.settings.easing
              )
            : s.settings.easing || (s.settings.easing = "swing");
        v();
        s.viewport.css({
          width: "100%",
          overflow: "hidden",
          position: "relative"
        }),
          s.viewport.parent().css({ maxWidth: u() }),
          s.settings.pager ||
            s.settings.controls ||
            s.viewport.parent().css({ margin: "0 auto 0px" }),
          s.children.css({
            float: "horizontal" === s.settings.mode ? "left" : "none",
            listStyle: "none",
            position: "relative"
          }),
          s.children.css("width", h()),
          "horizontal" === s.settings.mode &&
            s.settings.slideMargin > 0 &&
            s.children.css("marginRight", s.settings.slideMargin),
          "vertical" === s.settings.mode &&
            s.settings.slideMargin > 0 &&
            s.children.css("marginBottom", s.settings.slideMargin),
          "fade" === s.settings.mode &&
            (s.children.css({
              position: "absolute",
              zIndex: 0,
              display: "none"
            }),
            s.children
              .eq(s.settings.startSlide)
              .css({ zIndex: s.settings.slideZIndex, display: "block" })),
          (s.controls.el = e('<div class="bx-controls" />')),
          s.settings.captions && P(),
          (s.active.last = s.settings.startSlide === f() - 1),
          s.settings.video && o.fitVids();
        var t = s.children.eq(s.settings.startSlide);
        ("all" === s.settings.preloadImages || s.settings.ticker) &&
          (t = s.children),
          s.settings.ticker
            ? (s.settings.pager = !1)
            : (s.settings.controls && C(),
              s.settings.auto && s.settings.autoControls && T(),
              s.settings.pager && w(),
              (s.settings.controls ||
                s.settings.autoControls ||
                s.settings.pager) &&
                s.viewport.after(s.controls.el)),
          c(t, g);
      },
      c = function(t, i) {
        var n = t.find('img:not([src=""]), iframe').length;
        if (0 === n) return void i();
        var s = 0;
        t.find('img:not([src=""]), iframe').each(function() {
          e(this)
            .one("load error", function() {
              ++s === n && i();
            })
            .each(function() {
              this.complete && e(this).load();
            });
        });
      },
      g = function() {
        if (
          s.settings.infiniteLoop &&
          "fade" !== s.settings.mode &&
          !s.settings.ticker
        ) {
          var t =
              "vertical" === s.settings.mode
                ? s.settings.minSlides
                : s.settings.maxSlides,
            i = s.children
              .slice(0, t)
              .clone(!0)
              .addClass("bx-clone"),
            n = s.children
              .slice(-t)
              .clone(!0)
              .addClass("bx-clone");
          o.append(i).prepend(n);
        }
        s.loader.remove(),
          m(),
          "vertical" === s.settings.mode && (s.settings.adaptiveHeight = !0),
          s.viewport.height(p()),
          o.redrawSlider(),
          s.settings.onSliderLoad(s, s.active.index),
          (s.initialized = !0),
          s.settings.responsive && e(window).bind("resize", Z),
          s.settings.auto &&
            s.settings.autoStart &&
            (f() > 1 || s.settings.autoSlideForOnePage) &&
            A(),
          s.settings.ticker && H(),
          s.settings.pager && I(s.settings.startSlide),
          s.settings.controls && W(),
          s.settings.touchEnabled && !s.settings.ticker && O(),
          s.settings.keyboardEnabled &&
            !s.settings.ticker &&
            e(document).keydown(N);
      },
      p = function() {
        var t = 0,
          n = e();
        if ("vertical" === s.settings.mode || s.settings.adaptiveHeight)
          if (s.carousel) {
            var o =
              1 === s.settings.moveSlides
                ? s.active.index
                : s.active.index * x();
            for (
              n = s.children.eq(o), i = 1;
              i <= s.settings.maxSlides - 1;
              i++
            )
              n = n.add(
                o + i >= s.children.length
                  ? s.children.eq(i - 1)
                  : s.children.eq(o + i)
              );
          } else n = s.children.eq(s.active.index);
        else n = s.children;
        return (
          "vertical" === s.settings.mode
            ? (n.each(function() {
                t += e(this).outerHeight();
              }),
              s.settings.slideMargin > 0 &&
                (t += s.settings.slideMargin * (s.settings.minSlides - 1)))
            : (t = Math.max.apply(
                Math,
                n
                  .map(function() {
                    return e(this).outerHeight(!1);
                  })
                  .get()
              )),
          "border-box" === s.viewport.css("box-sizing")
            ? (t +=
                parseFloat(s.viewport.css("padding-top")) +
                parseFloat(s.viewport.css("padding-bottom")) +
                parseFloat(s.viewport.css("border-top-width")) +
                parseFloat(s.viewport.css("border-bottom-width")))
            : "padding-box" === s.viewport.css("box-sizing") &&
              (t +=
                parseFloat(s.viewport.css("padding-top")) +
                parseFloat(s.viewport.css("padding-bottom"))),
          t
        );
      },
      u = function() {
        var e = "100%";
        return (
          s.settings.slideWidth > 0 &&
            (e =
              "horizontal" === s.settings.mode
                ? s.settings.maxSlides * s.settings.slideWidth +
                  (s.settings.maxSlides - 1) * s.settings.slideMargin
                : s.settings.slideWidth),
          e
        );
      },
      h = function() {
        var e = s.settings.slideWidth,
          t = s.viewport.width();
        return (
          0 === s.settings.slideWidth ||
          (s.settings.slideWidth > t && !s.carousel) ||
          "vertical" === s.settings.mode
            ? (e = t)
            : s.settings.maxSlides > 1 &&
              "horizontal" === s.settings.mode &&
              (t > s.maxThreshold ||
                (t < s.minThreshold &&
                  (e =
                    (t - s.settings.slideMargin * (s.settings.minSlides - 1)) /
                    s.settings.minSlides))),
          e
        );
      },
      v = function() {
        var e = 1;
        if ("horizontal" === s.settings.mode && s.settings.slideWidth > 0)
          if (s.viewport.width() < s.minThreshold) e = s.settings.minSlides;
          else if (s.viewport.width() > s.maxThreshold)
            e = s.settings.maxSlides;
          else {
            var t = s.children.first().width() + s.settings.slideMargin;
            e = Math.floor((s.viewport.width() + s.settings.slideMargin) / t);
          }
        else "vertical" === s.settings.mode && (e = s.settings.minSlides);
        return e;
      },
      f = function() {
        var e = 0;
        if (s.settings.moveSlides > 0)
          if (s.settings.infiniteLoop) e = Math.ceil(s.children.length / x());
          else
            for (var t = 0, i = 0; t < s.children.length; )
              ++e,
                (t = i + v()),
                (i +=
                  s.settings.moveSlides <= v() ? s.settings.moveSlides : v());
        else e = Math.ceil(s.children.length / v());
        return e;
      },
      x = function() {
        return s.settings.moveSlides > 0 && s.settings.moveSlides <= v()
          ? s.settings.moveSlides
          : v();
      },
      m = function() {
        var e;
        if (
          s.children.length > s.settings.maxSlides &&
          s.active.last &&
          !s.settings.infiniteLoop
        ) {
          if ("horizontal" === s.settings.mode) {
            var t = s.children.last();
            (e = t.position()),
              S(-(e.left - (s.viewport.width() - t.outerWidth())), "reset", 0);
          } else if ("vertical" === s.settings.mode) {
            var i = s.children.length - s.settings.minSlides;
            (e = s.children.eq(i).position()), S(-e.top, "reset", 0);
          }
        } else
          (e = s.children.eq(s.active.index * x()).position()),
            s.active.index === f() - 1 && (s.active.last = !0),
            void 0 !== e &&
              ("horizontal" === s.settings.mode
                ? S(-e.left, "reset", 0)
                : "vertical" === s.settings.mode && S(-e.top, "reset", 0));
      },
      S = function(e, t, i, n) {
        if (s.usingCSS) {
          var r =
            "vertical" === s.settings.mode
              ? "translate3d(0, " + e + "px, 0)"
              : "translate3d(" + e + "px, 0, 0)";
          o.css("-" + s.cssPrefix + "-transition-duration", i / 1e3 + "s"),
            "slide" === t
              ? setTimeout(function() {
                  o.css(s.animProp, r),
                    0 === e
                      ? q()
                      : o.bind(
                          "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",
                          function() {
                            o.unbind(
                              "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"
                            ),
                              q();
                          }
                        );
                }, 0)
              : "reset" === t
              ? o.css(s.animProp, r)
              : "ticker" === t &&
                (o.css(
                  "-" + s.cssPrefix + "-transition-timing-function",
                  "linear"
                ),
                o.css(s.animProp, r),
                o.bind(
                  "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd",
                  function() {
                    o.unbind(
                      "transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd"
                    ),
                      S(n.resetValue, "reset", 0),
                      L();
                  }
                ));
        } else {
          var a = {};
          (a[s.animProp] = e),
            "slide" === t
              ? o.animate(a, i, s.settings.easing, function() {
                  q();
                })
              : "reset" === t
              ? o.css(s.animProp, e)
              : "ticker" === t &&
                o.animate(a, speed, "linear", function() {
                  S(n.resetValue, "reset", 0), L();
                });
        }
      },
      b = function() {
        for (var t = "", i = f(), n = 0; i > n; n++) {
          var o = "";
          (s.settings.buildPager && e.isFunction(s.settings.buildPager)) ||
          s.settings.pagerCustom
            ? ((o = s.settings.buildPager(n)),
              s.pagerEl.addClass("bx-custom-pager"))
            : ((o = n + 1), s.pagerEl.addClass("bx-default-pager")),
            (t +=
              '<div class="bx-pager-item"><a href="" data-slide-index="' +
              n +
              '" class="bx-pager-link">' +
              o +
              "</a></div>");
        }
        s.pagerEl.html(t);
      },
      w = function() {
        s.settings.pagerCustom
          ? (s.pagerEl = e(s.settings.pagerCustom))
          : ((s.pagerEl = e('<div class="bx-pager" />')),
            s.settings.pagerSelector
              ? e(s.settings.pagerSelector).html(s.pagerEl)
              : s.controls.el.addClass("bx-has-pager").append(s.pagerEl),
            b()),
          s.pagerEl.on("click touchend", "a", z);
      },
      C = function() {
        (s.controls.next = e(
          '<a class="bx-next" href="">' + s.settings.nextText + "</a>"
        )),
          (s.controls.prev = e(
            '<a class="bx-prev" href="">' + s.settings.prevText + "</a>"
          )),
          s.controls.next.bind("click touchend", E),
          s.controls.prev.bind("click touchend", y),
          s.settings.nextSelector &&
            e(s.settings.nextSelector).append(s.controls.next),
          s.settings.prevSelector &&
            e(s.settings.prevSelector).append(s.controls.prev),
          s.settings.nextSelector ||
            s.settings.prevSelector ||
            ((s.controls.directionEl = e(
              '<div class="bx-controls-direction" />'
            )),
            s.controls.directionEl
              .append(s.controls.prev)
              .append(s.controls.next),
            s.controls.el
              .addClass("bx-has-controls-direction")
              .append(s.controls.directionEl));
      },
      T = function() {
        (s.controls.start = e(
          '<div class="bx-controls-auto-item"><a class="bx-start" href="">' +
            s.settings.startText +
            "</a></div>"
        )),
          (s.controls.stop = e(
            '<div class="bx-controls-auto-item"><a class="bx-stop" href="">' +
              s.settings.stopText +
              "</a></div>"
          )),
          (s.controls.autoEl = e('<div class="bx-controls-auto" />')),
          s.controls.autoEl.on("click", ".bx-start", k),
          s.controls.autoEl.on("click", ".bx-stop", M),
          s.settings.autoControlsCombine
            ? s.controls.autoEl.append(s.controls.start)
            : s.controls.autoEl
                .append(s.controls.start)
                .append(s.controls.stop),
          s.settings.autoControlsSelector
            ? e(s.settings.autoControlsSelector).html(s.controls.autoEl)
            : s.controls.el
                .addClass("bx-has-controls-auto")
                .append(s.controls.autoEl),
          D(s.settings.autoStart ? "stop" : "start");
      },
      P = function() {
        s.children.each(function() {
          var t = e(this)
            .find("img:first")
            .attr("title");
          void 0 !== t &&
            ("" + t).length &&
            e(this).append(
              '<div class="bx-caption"><span>' + t + "</span></div>"
            );
        });
      },
      E = function(e) {
        e.preventDefault(),
          s.controls.el.hasClass("disabled") ||
            (s.settings.auto && o.stopAuto(), o.goToNextSlide());
      },
      y = function(e) {
        e.preventDefault(),
          s.controls.el.hasClass("disabled") ||
            (s.settings.auto && o.stopAuto(), o.goToPrevSlide());
      },
      k = function(e) {
        o.startAuto(), e.preventDefault();
      },
      M = function(e) {
        o.stopAuto(), e.preventDefault();
      },
      z = function(t) {
        if ((t.preventDefault(), !s.controls.el.hasClass("disabled"))) {
          s.settings.auto && o.stopAuto();
          var i = e(t.currentTarget);
          if (void 0 !== i.attr("data-slide-index")) {
            var n = parseInt(i.attr("data-slide-index"));
            n !== s.active.index && o.goToSlide(n);
          }
        }
      },
      I = function(t) {
        var i = s.children.length;
        return "short" === s.settings.pagerType
          ? (s.settings.maxSlides > 1 &&
              (i = Math.ceil(s.children.length / s.settings.maxSlides)),
            void s.pagerEl.html(t + 1 + s.settings.pagerShortSeparator + i))
          : (s.pagerEl.find("a").removeClass("active"),
            void s.pagerEl.each(function(i, n) {
              e(n)
                .find("a")
                .eq(t)
                .addClass("active");
            }));
      },
      q = function() {
        if (s.settings.infiniteLoop) {
          var e = "";
          0 === s.active.index
            ? (e = s.children.eq(0).position())
            : s.active.index === f() - 1 && s.carousel
            ? (e = s.children.eq((f() - 1) * x()).position())
            : s.active.index === s.children.length - 1 &&
              (e = s.children.eq(s.children.length - 1).position()),
            e &&
              ("horizontal" === s.settings.mode
                ? S(-e.left, "reset", 0)
                : "vertical" === s.settings.mode && S(-e.top, "reset", 0));
        }
        (s.working = !1),
          s.settings.onSlideAfter(
            s.children.eq(s.active.index),
            s.oldIndex,
            s.active.index
          );
      },
      D = function(e) {
        s.settings.autoControlsCombine
          ? s.controls.autoEl.html(s.controls[e])
          : (s.controls.autoEl.find("a").removeClass("active"),
            s.controls.autoEl.find("a:not(.bx-" + e + ")").addClass("active"));
      },
      W = function() {
        1 === f()
          ? (s.controls.prev.addClass("disabled"),
            s.controls.next.addClass("disabled"))
          : !s.settings.infiniteLoop &&
            s.settings.hideControlOnEnd &&
            (0 === s.active.index
              ? (s.controls.prev.addClass("disabled"),
                s.controls.next.removeClass("disabled"))
              : s.active.index === f() - 1
              ? (s.controls.next.addClass("disabled"),
                s.controls.prev.removeClass("disabled"))
              : (s.controls.prev.removeClass("disabled"),
                s.controls.next.removeClass("disabled")));
      },
      A = function() {
        if (s.settings.autoDelay > 0) {
          setTimeout(o.startAuto, s.settings.autoDelay);
        } else
          o.startAuto(),
            e(window)
              .focus(function() {
                o.startAuto();
              })
              .blur(function() {
                o.stopAuto();
              });
        s.settings.autoHover &&
          o.hover(
            function() {
              s.interval && (o.stopAuto(!0), (s.autoPaused = !0));
            },
            function() {
              s.autoPaused && (o.startAuto(!0), (s.autoPaused = null));
            }
          );
      },
      H = function() {
        var t = 0;
        if ("next" === s.settings.autoDirection)
          o.append(s.children.clone().addClass("bx-clone"));
        else {
          o.prepend(s.children.clone().addClass("bx-clone"));
          var i = s.children.first().position();
          t = "horizontal" === s.settings.mode ? -i.left : -i.top;
        }
        if (
          (S(t, "reset", 0),
          (s.settings.pager = !1),
          (s.settings.controls = !1),
          (s.settings.autoControls = !1),
          s.settings.tickerHover)
        )
          if (s.usingCSS) {
            var n,
              r = "horizontal" == s.settings.mode ? 4 : 5;
            s.viewport.hover(
              function() {
                var e = o.css("-" + s.cssPrefix + "-transform");
                (n = parseFloat(e.split(",")[r])), S(n, "reset", 0);
              },
              function() {
                var t = 0;
                s.children.each(function() {
                  t +=
                    "horizontal" == s.settings.mode
                      ? e(this).outerWidth(!0)
                      : e(this).outerHeight(!0);
                });
                var i = s.settings.speed / t,
                  o = ("horizontal" == s.settings.mode ? "left" : "top",
                  i * (t - Math.abs(parseInt(n))));
                L(o);
              }
            );
          } else
            s.viewport.hover(
              function() {
                o.stop();
              },
              function() {
                var t = 0;
                s.children.each(function() {
                  t +=
                    "horizontal" == s.settings.mode
                      ? e(this).outerWidth(!0)
                      : e(this).outerHeight(!0);
                });
                var i = s.settings.speed / t,
                  n = "horizontal" == s.settings.mode ? "left" : "top",
                  r = i * (t - Math.abs(parseInt(o.css(n))));
                L(r);
              }
            );
        L();
      },
      L = function(e) {
        speed = e ? e : s.settings.speed;
        var t = { left: 0, top: 0 },
          i = { left: 0, top: 0 };
        "next" === s.settings.autoDirection
          ? (t = o
              .find(".bx-clone")
              .first()
              .position())
          : (i = s.children.first().position());
        var n = "horizontal" === s.settings.mode ? -t.left : -t.top,
          r = "horizontal" === s.settings.mode ? -i.left : -i.top,
          a = { resetValue: r };
        S(n, "ticker", speed, a);
      },
      F = function(t) {
        var i = e(window),
          n = { top: i.scrollTop(), left: i.scrollLeft() };
        (n.right = n.left + i.width()), (n.bottom = n.top + i.height());
        var s = t.offset();
        return (
          (s.right = s.left + t.outerWidth()),
          (s.bottom = s.top + t.outerHeight()),
          !(
            n.right < s.left ||
            n.left > s.right ||
            n.bottom < s.top ||
            n.top > s.bottom
          )
        );
      },
      N = function(e) {
        var t = document.activeElement.tagName.toLowerCase(),
          i = "input|textarea",
          n = new RegExp(t, ["i"]),
          s = n.exec(i);
        if (null == s && F(o)) {
          if (39 == e.keyCode) return E(e), !1;
          if (37 == e.keyCode) return y(e), !1;
        }
      },
      O = function() {
        (s.touch = { start: { x: 0, y: 0 }, end: { x: 0, y: 0 } }),
          s.viewport.bind("touchstart MSPointerDown pointerdown", X),
          s.viewport.on("click", ".bxslider a", function(e) {
            s.viewport.hasClass("click-disabled") &&
              (e.preventDefault(), s.viewport.removeClass("click-disabled"));
          });
      },
      // X = function(e) {
      //   if ((s.controls.el.addClass("disabled"), s.working))
      //     e.preventDefault(), s.controls.el.removeClass("disabled");
      //   else {
      //     s.touch.originalPos = o.position();
      //     var t = e.originalEvent,
      //       i = "undefined" != typeof t.changedTouches ? t.changedTouches : [t];
      //     (s.touch.start.x = i[0].pageX),
      //       (s.touch.start.y = i[0].pageY),
      //       s.viewport.get(0).setPointerCapture &&
      //         ((s.pointerId = t.pointerId),
      //         s.viewport.get(0).setPointerCapture(s.pointerId)),
      //       s.viewport.bind("touchmove MSPointerMove pointermove", R),
      //       s.viewport.bind("touchend MSPointerUp pointerup", V),
      //       s.viewport.bind("MSPointerCancel pointercancel", Y);
      //   }
      // },
      Y = function() {
        S(s.touch.originalPos.left, "reset", 0),
          s.controls.el.removeClass("disabled"),
          s.viewport.unbind("MSPointerCancel pointercancel", Y),
          s.viewport.unbind("touchmove MSPointerMove pointermove", R),
          s.viewport.unbind("touchend MSPointerUp pointerup", V),
          s.viewport.get(0).releasePointerCapture &&
            s.viewport.get(0).releasePointerCapture(s.pointerId);
      },
      R = function(e) {
        var t = e.originalEvent,
          i = "undefined" != typeof t.changedTouches ? t.changedTouches : [t],
          n = Math.abs(i[0].pageX - s.touch.start.x),
          o = Math.abs(i[0].pageY - s.touch.start.y);
        if (
          (3 * n > o && s.settings.preventDefaultSwipeX
            ? e.preventDefault()
            : 3 * o > n &&
              s.settings.preventDefaultSwipeY &&
              e.preventDefault(),
          "fade" !== s.settings.mode && s.settings.oneToOneTouch)
        ) {
          var r = 0,
            a = 0;
          "horizontal" === s.settings.mode
            ? ((a = i[0].pageX - s.touch.start.x),
              (r = s.touch.originalPos.left + a))
            : ((a = i[0].pageY - s.touch.start.y),
              (r = s.touch.originalPos.top + a)),
            S(r, "reset", 0);
        }
      },
      V = function(e) {
        s.viewport.unbind("touchmove MSPointerMove pointermove", R),
          s.controls.el.removeClass("disabled");
        var t = e.originalEvent,
          i = "undefined" != typeof t.changedTouches ? t.changedTouches : [t],
          n = 0,
          r = 0;
        (s.touch.end.x = i[0].pageX),
          (s.touch.end.y = i[0].pageY),
          "fade" === s.settings.mode
            ? ((r = Math.abs(s.touch.start.x - s.touch.end.x)),
              r >= s.settings.swipeThreshold &&
                (s.touch.start.x > s.touch.end.x
                  ? o.goToNextSlide()
                  : o.goToPrevSlide(),
                o.stopAuto()))
            : ("horizontal" === s.settings.mode
                ? ((r = s.touch.end.x - s.touch.start.x),
                  (n = s.touch.originalPos.left))
                : ((r = s.touch.end.y - s.touch.start.y),
                  (n = s.touch.originalPos.top)),
              !s.settings.infiniteLoop &&
              ((0 === s.active.index && r > 0) || (s.active.last && 0 > r))
                ? S(n, "reset", 200)
                : Math.abs(r) >= s.settings.swipeThreshold
                ? (0 > r ? o.goToNextSlide() : o.goToPrevSlide(), o.stopAuto())
                : S(n, "reset", 200)),
          s.viewport.unbind("touchend MSPointerUp pointerup", V),
          s.viewport.get(0).releasePointerCapture &&
            s.viewport.get(0).releasePointerCapture(s.pointerId);
      },
      Z = function() {
        if (s.initialized)
          if (s.working) window.setTimeout(Z, 10);
          else {
            var t = e(window).width(),
              i = e(window).height();
            (r !== t || a !== i) &&
              ((r = t),
              (a = i),
              o.redrawSlider(),
              s.settings.onSliderResize.call(o, s.active.index));
          }
      };
    return (
      (o.goToSlide = function(t, i) {
        if (!s.working && s.active.index !== t) {
          (s.working = !0),
            (s.oldIndex = s.active.index),
            (s.active.index = 0 > t ? f() - 1 : t >= f() ? 0 : t);
          var n = !0;
          if (
            ((n = s.settings.onSlideBefore(
              s.children.eq(s.active.index),
              s.oldIndex,
              s.active.index
            )),
            "undefined" != typeof n && !n)
          )
            return (s.active.index = s.oldIndex), void (s.working = !1);
          if (
            ("next" === i
              ? s.settings.onSlideNext(
                  s.children.eq(s.active.index),
                  s.oldIndex,
                  s.active.index
                ) || (n = !1)
              : "prev" === i &&
                (s.settings.onSlidePrev(
                  s.children.eq(s.active.index),
                  s.oldIndex,
                  s.active.index
                ) ||
                  (n = !1)),
            "undefined" != typeof n && !n)
          )
            return (s.active.index = s.oldIndex), void (s.working = !1);
          if (
            ((s.active.last = s.active.index >= f() - 1),
            (s.settings.pager || s.settings.pagerCustom) && I(s.active.index),
            s.settings.controls && W(),
            "fade" === s.settings.mode)
          )
            s.settings.adaptiveHeight &&
              s.viewport.height() !== p() &&
              s.viewport.animate(
                { height: p() },
                s.settings.adaptiveHeightSpeed
              ),
              s.children
                .filter(":visible")
                .fadeOut(s.settings.speed)
                .css({ zIndex: 0 }),
              s.children
                .eq(s.active.index)
                .css("zIndex", s.settings.slideZIndex + 1)
                .fadeIn(s.settings.speed, function() {
                  e(this).css("zIndex", s.settings.slideZIndex), q();
                });
          else {
            s.settings.adaptiveHeight &&
              s.viewport.height() !== p() &&
              s.viewport.animate(
                { height: p() },
                s.settings.adaptiveHeightSpeed
              );
            var r = 0,
              a = { left: 0, top: 0 },
              l = null;
            if (!s.settings.infiniteLoop && s.carousel && s.active.last)
              if ("horizontal" === s.settings.mode)
                (l = s.children.eq(s.children.length - 1)),
                  (a = l.position()),
                  (r = s.viewport.width() - l.outerWidth());
              else {
                var d = s.children.length - s.settings.minSlides;
                a = s.children.eq(d).position();
              }
            else if (s.carousel && s.active.last && "prev" === i) {
              var c =
                1 === s.settings.moveSlides
                  ? s.settings.maxSlides - x()
                  : (f() - 1) * x() -
                    (s.children.length - s.settings.maxSlides);
              (l = o.children(".bx-clone").eq(c)), (a = l.position());
            } else if ("next" === i && 0 === s.active.index)
              (a = o
                .find("> .bx-clone")
                .eq(s.settings.maxSlides)
                .position()),
                (s.active.last = !1);
            else if (t >= 0) {
              var g = t * x();
              a = s.children.eq(g).position();
            }
            if ("undefined" != typeof a) {
              var u = "horizontal" === s.settings.mode ? -(a.left - r) : -a.top;
              S(u, "slide", s.settings.speed);
            }
          }
        }
      }),
      (o.goToNextSlide = function() {
        if (s.settings.infiniteLoop || !s.active.last) {
          var e = parseInt(s.active.index) + 1;
          o.goToSlide(e, "next");
        }
      }),
      (o.goToPrevSlide = function() {
        if (s.settings.infiniteLoop || 0 !== s.active.index) {
          var e = parseInt(s.active.index) - 1;
          o.goToSlide(e, "prev");
        }
      }),
      (o.startAuto = function(e) {
        s.interval ||
          ((s.interval = setInterval(function() {
            "next" === s.settings.autoDirection
              ? o.goToNextSlide()
              : o.goToPrevSlide();
          }, s.settings.pause)),
          s.settings.autoControls && e !== !0 && D("stop"));
      }),
      (o.stopAuto = function(e) {
        s.interval &&
          (clearInterval(s.interval),
          (s.interval = null),
          s.settings.autoControls && e !== !0 && D("start"));
      }),
      (o.getCurrentSlide = function() {
        return s.active.index;
      }),
      (o.getCurrentSlideElement = function() {
        return s.children.eq(s.active.index);
      }),
      (o.getSlideCount = function() {
        return s.children.length;
      }),
      (o.isWorking = function() {
        return s.working;
      }),
      (o.redrawSlider = function() {
        s.children.add(o.find(".bx-clone")).outerWidth(h()),
          s.viewport.css("height", p()),
          s.settings.ticker || m(),
          s.active.last && (s.active.index = f() - 1),
          s.active.index >= f() && (s.active.last = !0),
          s.settings.pager &&
            !s.settings.pagerCustom &&
            (b(), I(s.active.index));
      }),
      (o.destroySlider = function() {
        s.initialized &&
          ((s.initialized = !1),
          e(".bx-clone", this).remove(),
          s.children.each(function() {
            void 0 !== e(this).data("origStyle")
              ? e(this).attr("style", e(this).data("origStyle"))
              : e(this).removeAttr("style");
          }),
          void 0 !== e(this).data("origStyle")
            ? this.attr("style", e(this).data("origStyle"))
            : e(this).removeAttr("style"),
          e(this)
            .unwrap()
            .unwrap(),
          s.controls.el && s.controls.el.remove(),
          s.controls.next && s.controls.next.remove(),
          s.controls.prev && s.controls.prev.remove(),
          s.pagerEl &&
            s.settings.controls &&
            !s.settings.pagerCustom &&
            s.pagerEl.remove(),
          e(".bx-caption", this).remove(),
          s.controls.autoEl && s.controls.autoEl.remove(),
          clearInterval(s.interval),
          s.settings.responsive && e(window).unbind("resize", Z),
          s.settings.keyboardEnabled && e(document).unbind("keydown", N));
      }),
      (o.reloadSlider = function(e) {
        void 0 !== e && (n = e), o.destroySlider(), l();
      }),
      l(),
      this
    );
  };
})(jQuery);

/*                    _ _        ____ _               _
   _ __ ___   ___  __| (_) __ _ / ___| |__   ___  ___| | __
  | '_ ` _ \ / _ \/ _` | |/ _` | |   | '_ \ / _ \/ __| |/ /
  | | | | | |  __/ (_| | | (_| | |___| | | |  __/ (__|   <
  |_| |_| |_|\___|\__,_|_|\__,_|\____|_| |_|\___|\___|_|\_\

  http://github.com/sparkbox/mediaCheck

  Version: 0.4.5, 14-07-2014
  Author: Rob Tarr (http://twitter.com/robtarr)
*/
(function() {
  window.mediaCheck = function(options) {
    var breakpoints,
      convertEmToPx,
      createListener,
      getPXValue,
      hasMatchMedia,
      i,
      mmListener,
      mq,
      mqChange;
    mq = void 0;
    mqChange = void 0;
    createListener = void 0;
    convertEmToPx = void 0;
    getPXValue = void 0;
    hasMatchMedia =
      window.matchMedia !== undefined && !!window.matchMedia("!").addListener;
    if (hasMatchMedia) {
      mqChange = function(mq, options) {
        if (mq.matches) {
          if (typeof options.entry === "function") {
            options.entry(mq);
          }
        } else {
          if (typeof options.exit === "function") {
            options.exit(mq);
          }
        }
        if (typeof options.both === "function") {
          return options.both(mq);
        }
      };
      createListener = function() {
        mq = window.matchMedia(options.media);
        mq.addListener(function() {
          return mqChange(mq, options);
        });
        window.addEventListener(
          "orientationchange",
          function() {
            mq = window.matchMedia(options.media);
            return mqChange(mq, options);
          },
          false
        );
        return mqChange(mq, options);
      };
      return createListener();
    } else {
      breakpoints = {};
      mqChange = function(mq, options) {
        if (mq.matches) {
          if (
            typeof options.entry === "function" &&
            (breakpoints[options.media] === false ||
              breakpoints[options.media] == null)
          ) {
            options.entry(mq);
          }
        } else {
          if (
            typeof options.exit === "function" &&
            (breakpoints[options.media] === true ||
              breakpoints[options.media] == null)
          ) {
            options.exit(mq);
          }
        }
        if (typeof options.both === "function") {
          options.both(mq);
        }
        return (breakpoints[options.media] = mq.matches);
      };
      convertEmToPx = function(value) {
        var emElement, px;
        emElement = void 0;
        emElement = document.createElement("div");
        emElement.style.width = "1em";
        emElement.style.position = "absolute";
        document.body.appendChild(emElement);
        px = value * emElement.offsetWidth;
        document.body.removeChild(emElement);
        return px;
      };
      getPXValue = function(width, unit) {
        var value;
        value = void 0;
        switch (unit) {
          case "em":
            value = convertEmToPx(width);
            break;
          default:
            value = width;
        }
        return value;
      };
      for (i in options) {
        breakpoints[options.media] = null;
      }
      mmListener = function() {
        var constraint, fakeMatchMedia, parts, value, windowWidth;
        parts = options.media.match(/\((.*)-.*:\s*([\d\.]*)(.*)\)/);
        constraint = parts[1];
        value = getPXValue(parseInt(parts[2], 10), parts[3]);
        fakeMatchMedia = {};
        windowWidth = window.innerWidth || document.documentElement.clientWidth;
        fakeMatchMedia.matches =
          (constraint === "max" && value > windowWidth) ||
          (constraint === "min" && value < windowWidth);
        return mqChange(fakeMatchMedia, options);
      };
      if (window.addEventListener) {
        window.addEventListener("resize", mmListener);
      } else {
        if (window.attachEvent) {
          window.attachEvent("onresize", mmListener);
        }
      }
      return mmListener();
    }
  };
}.call(this));

/*!
 * jQuery Validation Plugin v1.13.0
 *
 * http://jqueryvalidation.org/
 *
 * Copyright (c) 2014 Jörn Zaefferer
 * Released under the MIT license
 */
(function(factory) {
  if (typeof define === "function" && define.amd) {
    define(["jquery"], factory);
  } else {
    factory(jQuery);
  }
})(function($) {
  $.extend($.fn, {
    // http://jqueryvalidation.org/validate/
    validate: function(options) {
      // if nothing is selected, return nothing; can't chain anyway
      if (!this.length) {
        if (options && options.debug && window.console) {
          console.warn("Nothing selected, can't validate, returning nothing.");
        }
        return;
      }

      // check if a validator for this form was already created
      var validator = $.data(this[0], "validator");
      if (validator) {
        return validator;
      }

      // Add novalidate tag if HTML5.
      this.attr("novalidate", "novalidate");

      validator = new $.validator(options, this[0]);
      $.data(this[0], "validator", validator);

      if (validator.settings.onsubmit) {
        this.validateDelegate(":submit", "click", function(event) {
          if (validator.settings.submitHandler) {
            validator.submitButton = event.target;
          }
          // allow suppressing validation by adding a cancel class to the submit button
          if ($(event.target).hasClass("cancel")) {
            validator.cancelSubmit = true;
          }

          // allow suppressing validation by adding the html5 formnovalidate attribute to the submit button
          if ($(event.target).attr("formnovalidate") !== undefined) {
            validator.cancelSubmit = true;
          }
        });

        // validate the form on submit
        this.submit(function(event) {
          if (validator.settings.debug) {
            // prevent form submit to be able to see console output
            event.preventDefault();
          }
          function handle() {
            var hidden;
            if (validator.settings.submitHandler) {
              if (validator.submitButton) {
                // insert a hidden input as a replacement for the missing submit button
                hidden = $("<input type='hidden'/>")
                  .attr("name", validator.submitButton.name)
                  .val($(validator.submitButton).val())
                  .appendTo(validator.currentForm);
              }
              validator.settings.submitHandler.call(
                validator,
                validator.currentForm,
                event
              );
              if (validator.submitButton) {
                // and clean up afterwards; thanks to no-block-scope, hidden can be referenced
                hidden.remove();
              }
              return false;
            }
            return true;
          }

          // prevent submit for invalid forms or custom submit handlers
          if (validator.cancelSubmit) {
            validator.cancelSubmit = false;
            return handle();
          }
          if (validator.form()) {
            if (validator.pendingRequest) {
              validator.formSubmitted = true;
              return false;
            }
            return handle();
          } else {
            validator.focusInvalid();
            return false;
          }
        });
      }

      return validator;
    },
    // http://jqueryvalidation.org/valid/
    valid: function() {
      var valid, validator;

      if ($(this[0]).is("form")) {
        valid = this.validate().form();
      } else {
        valid = true;
        validator = $(this[0].form).validate();
        this.each(function() {
          valid = validator.element(this) && valid;
        });
      }
      return valid;
    },
    // attributes: space separated list of attributes to retrieve and remove
    removeAttrs: function(attributes) {
      var result = {},
        $element = this;
      $.each(attributes.split(/\s/), function(index, value) {
        result[value] = $element.attr(value);
        $element.removeAttr(value);
      });
      return result;
    },
    // http://jqueryvalidation.org/rules/
    rules: function(command, argument) {
      var element = this[0],
        settings,
        staticRules,
        existingRules,
        data,
        param,
        filtered;

      if (command) {
        settings = $.data(element.form, "validator").settings;
        staticRules = settings.rules;
        existingRules = $.validator.staticRules(element);
        switch (command) {
          case "add":
            $.extend(existingRules, $.validator.normalizeRule(argument));
            // remove messages from rules, but allow them to be set separately
            delete existingRules.messages;
            staticRules[element.name] = existingRules;
            if (argument.messages) {
              settings.messages[element.name] = $.extend(
                settings.messages[element.name],
                argument.messages
              );
            }
            break;
          case "remove":
            if (!argument) {
              delete staticRules[element.name];
              return existingRules;
            }
            filtered = {};
            $.each(argument.split(/\s/), function(index, method) {
              filtered[method] = existingRules[method];
              delete existingRules[method];
              if (method === "required") {
                $(element).removeAttr("aria-required");
              }
            });
            return filtered;
        }
      }

      data = $.validator.normalizeRules(
        $.extend(
          {},
          $.validator.classRules(element),
          $.validator.attributeRules(element),
          $.validator.dataRules(element),
          $.validator.staticRules(element)
        ),
        element
      );

      // make sure required is at front
      if (data.required) {
        param = data.required;
        delete data.required;
        data = $.extend({ required: param }, data);
        $(element).attr("aria-required", "true");
      }

      // make sure remote is at back
      if (data.remote) {
        param = data.remote;
        delete data.remote;
        data = $.extend(data, { remote: param });
      }

      return data;
    }
  });

  // Custom selectors
  $.extend($.expr[":"], {
    // http://jqueryvalidation.org/blank-selector/
    blank: function(a) {
      return !$.trim("" + $(a).val());
    },
    // http://jqueryvalidation.org/filled-selector/
    filled: function(a) {
      return !!$.trim("" + $(a).val());
    },
    // http://jqueryvalidation.org/unchecked-selector/
    unchecked: function(a) {
      return !$(a).prop("checked");
    }
  });

  // constructor for validator
  $.validator = function(options, form) {
    this.settings = $.extend(true, {}, $.validator.defaults, options);
    this.currentForm = form;
    this.init();
  };

  // http://jqueryvalidation.org/jQuery.validator.format/
  $.validator.format = function(source, params) {
    if (arguments.length === 1) {
      return function() {
        var args = $.makeArray(arguments);
        args.unshift(source);
        return $.validator.format.apply(this, args);
      };
    }
    if (arguments.length > 2 && params.constructor !== Array) {
      params = $.makeArray(arguments).slice(1);
    }
    if (params.constructor !== Array) {
      params = [params];
    }
    $.each(params, function(i, n) {
      source = source.replace(new RegExp("\\{" + i + "\\}", "g"), function() {
        return n;
      });
    });
    return source;
  };

  $.extend($.validator, {
    defaults: {
      messages: {},
      groups: {},
      rules: {},
      errorClass: "error",
      validClass: "valid",
      errorElement: "label",
      focusInvalid: true,
      errorContainer: $([]),
      errorLabelContainer: $([]),
      onsubmit: true,
      ignore: ":hidden",
      ignoreTitle: false,
      onfocusin: function(element) {
        this.lastActive = element;

        // hide error label and remove error class on focus if enabled
        if (this.settings.focusCleanup && !this.blockFocusCleanup) {
          if (this.settings.unhighlight) {
            this.settings.unhighlight.call(
              this,
              element,
              this.settings.errorClass,
              this.settings.validClass
            );
          }
          this.hideThese(this.errorsFor(element));
        }
      },
      onfocusout: function(element) {
        if (
          !this.checkable(element) &&
          (element.name in this.submitted || !this.optional(element))
        ) {
          this.element(element);
        }
      },
      onkeyup: function(element, event) {
        if (event.which === 9 && this.elementValue(element) === "") {
          return;
        } else if (
          element.name in this.submitted ||
          element === this.lastElement
        ) {
          this.element(element);
        }
      },
      onclick: function(element) {
        // click on selects, radiobuttons and checkboxes
        if (element.name in this.submitted) {
          this.element(element);

          // or option elements, check parent select in that case
        } else if (element.parentNode.name in this.submitted) {
          this.element(element.parentNode);
        }
      },
      highlight: function(element, errorClass, validClass) {
        if (element.type === "radio") {
          this.findByName(element.name)
            .addClass(errorClass)
            .removeClass(validClass);
        } else {
          $(element)
            .addClass(errorClass)
            .removeClass(validClass);
        }
      },
      unhighlight: function(element, errorClass, validClass) {
        if (element.type === "radio") {
          this.findByName(element.name)
            .removeClass(errorClass)
            .addClass(validClass);
        } else {
          $(element)
            .removeClass(errorClass)
            .addClass(validClass);
        }
      }
    },

    // http://jqueryvalidation.org/jQuery.validator.setDefaults/
    setDefaults: function(settings) {
      $.extend($.validator.defaults, settings);
    },

    messages: {
      required: "This field is required.",
      remote: "Please fix this field.",
      email: "Please enter a valid email address.",
      url: "Please enter a valid URL.",
      date: "Please enter a valid date.",
      dateISO: "Please enter a valid date ( ISO ).",
      number: "Please enter a valid number.",
      digits: "Please enter only digits.",
      creditcard: "Please enter a valid credit card number.",
      equalTo: "Please enter the same value again.",
      maxlength: $.validator.format(
        "Please enter no more than {0} characters."
      ),
      minlength: $.validator.format("Please enter at least {0} characters."),
      rangelength: $.validator.format(
        "Please enter a value between {0} and {1} characters long."
      ),
      range: $.validator.format("Please enter a value between {0} and {1}."),
      max: $.validator.format(
        "Please enter a value less than or equal to {0}."
      ),
      min: $.validator.format(
        "Please enter a value greater than or equal to {0}."
      )
    },

    autoCreateRanges: false,

    prototype: {
      init: function() {
        this.labelContainer = $(this.settings.errorLabelContainer);
        this.errorContext =
          (this.labelContainer.length && this.labelContainer) ||
          $(this.currentForm);
        this.containers = $(this.settings.errorContainer).add(
          this.settings.errorLabelContainer
        );
        this.submitted = {};
        this.valueCache = {};
        this.pendingRequest = 0;
        this.pending = {};
        this.invalid = {};
        this.reset();

        var groups = (this.groups = {}),
          rules;
        $.each(this.settings.groups, function(key, value) {
          if (typeof value === "string") {
            value = value.split(/\s/);
          }
          $.each(value, function(index, name) {
            groups[name] = key;
          });
        });
        rules = this.settings.rules;
        $.each(rules, function(key, value) {
          rules[key] = $.validator.normalizeRule(value);
        });

        function delegate(event) {
          var validator = $.data(this[0].form, "validator"),
            eventType = "on" + event.type.replace(/^validate/, ""),
            settings = validator.settings;
          if (settings[eventType] && !this.is(settings.ignore)) {
            settings[eventType].call(validator, this[0], event);
          }
        }
        $(this.currentForm)
          .validateDelegate(
            ":text, [type='password'], [type='file'], select, textarea, " +
              "[type='number'], [type='search'] ,[type='tel'], [type='url'], " +
              "[type='email'], [type='datetime'], [type='date'], [type='month'], " +
              "[type='week'], [type='time'], [type='datetime-local'], " +
              "[type='range'], [type='color'], [type='radio'], [type='checkbox']",
            "focusin focusout keyup",
            delegate
          )
          // Support: Chrome, oldIE
          // "select" is provided as event.target when clicking a option
          .validateDelegate(
            "select, option, [type='radio'], [type='checkbox']",
            "click",
            delegate
          );

        if (this.settings.invalidHandler) {
          $(this.currentForm).bind(
            "invalid-form.validate",
            this.settings.invalidHandler
          );
        }

        // Add aria-required to any Static/Data/Class required fields before first validation
        // Screen readers require this attribute to be present before the initial submission http://www.w3.org/TR/WCAG-TECHS/ARIA2.html
        $(this.currentForm)
          .find("[required], [data-rule-required], .required")
          .attr("aria-required", "true");
      },

      // http://jqueryvalidation.org/Validator.form/
      form: function() {
        this.checkForm();
        $.extend(this.submitted, this.errorMap);
        this.invalid = $.extend({}, this.errorMap);
        if (!this.valid()) {
          $(this.currentForm).triggerHandler("invalid-form", [this]);
        }
        this.showErrors();
        return this.valid();
      },

      checkForm: function() {
        this.prepareForm();
        for (
          var i = 0, elements = (this.currentElements = this.elements());
          elements[i];
          i++
        ) {
          this.check(elements[i]);
        }
        return this.valid();
      },

      // http://jqueryvalidation.org/Validator.element/
      element: function(element) {
        var cleanElement = this.clean(element),
          checkElement = this.validationTargetFor(cleanElement),
          result = true;

        this.lastElement = checkElement;

        if (checkElement === undefined) {
          delete this.invalid[cleanElement.name];
        } else {
          this.prepareElement(checkElement);
          this.currentElements = $(checkElement);

          result = this.check(checkElement) !== false;
          if (result) {
            delete this.invalid[checkElement.name];
          } else {
            this.invalid[checkElement.name] = true;
          }
        }
        // Add aria-invalid status for screen readers
        $(element).attr("aria-invalid", !result);

        if (!this.numberOfInvalids()) {
          // Hide error containers on last error
          this.toHide = this.toHide.add(this.containers);
        }
        this.showErrors();
        return result;
      },

      // http://jqueryvalidation.org/Validator.showErrors/
      showErrors: function(errors) {
        if (errors) {
          // add items to error list and map
          $.extend(this.errorMap, errors);
          this.errorList = [];
          for (var name in errors) {
            this.errorList.push({
              message: errors[name],
              element: this.findByName(name)[0]
            });
          }
          // remove items from success list
          this.successList = $.grep(this.successList, function(element) {
            return !(element.name in errors);
          });
        }
        if (this.settings.showErrors) {
          this.settings.showErrors.call(this, this.errorMap, this.errorList);
        } else {
          this.defaultShowErrors();
        }
      },

      // http://jqueryvalidation.org/Validator.resetForm/
      resetForm: function() {
        if ($.fn.resetForm) {
          $(this.currentForm).resetForm();
        }
        this.submitted = {};
        this.lastElement = null;
        this.prepareForm();
        this.hideErrors();
        this.elements()
          .removeClass(this.settings.errorClass)
          .removeData("previousValue")
          .removeAttr("aria-invalid");
      },

      numberOfInvalids: function() {
        return this.objectLength(this.invalid);
      },

      objectLength: function(obj) {
        /* jshint unused: false */
        var count = 0,
          i;
        for (i in obj) {
          count++;
        }
        return count;
      },

      hideErrors: function() {
        this.hideThese(this.toHide);
      },

      hideThese: function(errors) {
        errors.not(this.containers).text("");
        this.addWrapper(errors).hide();
      },

      valid: function() {
        return this.size() === 0;
      },

      size: function() {
        return this.errorList.length;
      },

      focusInvalid: function() {
        if (this.settings.focusInvalid) {
          try {
            $(
              this.findLastActive() ||
                (this.errorList.length && this.errorList[0].element) ||
                []
            )
              .filter(":visible")
              .focus()
              // manually trigger focusin event; without it, focusin handler isn't called, findLastActive won't have anything to find
              .trigger("focusin");
          } catch (e) {
            // ignore IE throwing errors when focusing hidden elements
          }
        }
      },

      findLastActive: function() {
        var lastActive = this.lastActive;
        return (
          lastActive &&
          $.grep(this.errorList, function(n) {
            return n.element.name === lastActive.name;
          }).length === 1 &&
          lastActive
        );
      },

      elements: function() {
        var validator = this,
          rulesCache = {};

        // select all valid inputs inside the form (no submit or reset buttons)
        return $(this.currentForm)
          .find("input, select, textarea")
          .not(":submit, :reset, :image, [disabled]")
          .not(this.settings.ignore)
          .filter(function() {
            if (!this.name && validator.settings.debug && window.console) {
              console.error("%o has no name assigned", this);
            }

            // select only the first element for each name, and only those with rules specified
            if (
              this.name in rulesCache ||
              !validator.objectLength($(this).rules())
            ) {
              return false;
            }

            rulesCache[this.name] = true;
            return true;
          });
      },

      clean: function(selector) {
        return $(selector)[0];
      },

      errors: function() {
        var errorClass = this.settings.errorClass.split(" ").join(".");
        return $(
          this.settings.errorElement + "." + errorClass,
          this.errorContext
        );
      },

      reset: function() {
        this.successList = [];
        this.errorList = [];
        this.errorMap = {};
        this.toShow = $([]);
        this.toHide = $([]);
        this.currentElements = $([]);
      },

      prepareForm: function() {
        this.reset();
        this.toHide = this.errors().add(this.containers);
      },

      prepareElement: function(element) {
        this.reset();
        this.toHide = this.errorsFor(element);
      },

      elementValue: function(element) {
        var val,
          $element = $(element),
          type = element.type;

        if (type === "radio" || type === "checkbox") {
          return $("input[name='" + element.name + "']:checked").val();
        } else if (
          type === "number" &&
          typeof element.validity !== "undefined"
        ) {
          return element.validity.badInput ? false : $element.val();
        }

        val = $element.val();
        if (typeof val === "string") {
          return val.replace(/\r/g, "");
        }
        return val;
      },

      check: function(element) {
        element = this.validationTargetFor(this.clean(element));

        var rules = $(element).rules(),
          rulesCount = $.map(rules, function(n, i) {
            return i;
          }).length,
          dependencyMismatch = false,
          val = this.elementValue(element),
          result,
          method,
          rule;

        for (method in rules) {
          rule = { method: method, parameters: rules[method] };
          try {
            result = $.validator.methods[method].call(
              this,
              val,
              element,
              rule.parameters
            );

            // if a method indicates that the field is optional and therefore valid,
            // don't mark it as valid when there are no other rules
            if (result === "dependency-mismatch" && rulesCount === 1) {
              dependencyMismatch = true;
              continue;
            }
            dependencyMismatch = false;

            if (result === "pending") {
              this.toHide = this.toHide.not(this.errorsFor(element));
              return;
            }

            if (!result) {
              this.formatAndAdd(element, rule);
              return false;
            }
          } catch (e) {
            if (this.settings.debug && window.console) {
              console.log(
                "Exception occurred when checking element " +
                  element.id +
                  ", check the '" +
                  rule.method +
                  "' method.",
                e
              );
            }
            throw e;
          }
        }
        if (dependencyMismatch) {
          return;
        }
        if (this.objectLength(rules)) {
          this.successList.push(element);
        }
        return true;
      },

      // return the custom message for the given element and validation method
      // specified in the element's HTML5 data attribute
      // return the generic message if present and no method specific message is present
      customDataMessage: function(element, method) {
        return (
          $(element).data(
            "msg" +
              method.charAt(0).toUpperCase() +
              method.substring(1).toLowerCase()
          ) || $(element).data("msg")
        );
      },

      // return the custom message for the given element name and validation method
      customMessage: function(name, method) {
        var m = this.settings.messages[name];
        return m && (m.constructor === String ? m : m[method]);
      },

      // return the first defined argument, allowing empty strings
      findDefined: function() {
        for (var i = 0; i < arguments.length; i++) {
          if (arguments[i] !== undefined) {
            return arguments[i];
          }
        }
        return undefined;
      },

      defaultMessage: function(element, method) {
        return this.findDefined(
          this.customMessage(element.name, method),
          this.customDataMessage(element, method),
          // title is never undefined, so handle empty string as undefined
          (!this.settings.ignoreTitle && element.title) || undefined,
          $.validator.messages[method],
          "<strong>Warning: No message defined for " +
            element.name +
            "</strong>"
        );
      },

      formatAndAdd: function(element, rule) {
        var message = this.defaultMessage(element, rule.method),
          theregex = /\$?\{(\d+)\}/g;
        if (typeof message === "function") {
          message = message.call(this, rule.parameters, element);
        } else if (theregex.test(message)) {
          message = $.validator.format(
            message.replace(theregex, "{$1}"),
            rule.parameters
          );
        }
        this.errorList.push({
          message: message,
          element: element,
          method: rule.method
        });

        this.errorMap[element.name] = message;
        this.submitted[element.name] = message;
      },

      addWrapper: function(toToggle) {
        if (this.settings.wrapper) {
          toToggle = toToggle.add(toToggle.parent(this.settings.wrapper));
        }
        return toToggle;
      },

      defaultShowErrors: function() {
        var i, elements, error;
        for (i = 0; this.errorList[i]; i++) {
          error = this.errorList[i];
          if (this.settings.highlight) {
            this.settings.highlight.call(
              this,
              error.element,
              this.settings.errorClass,
              this.settings.validClass
            );
          }
          this.showLabel(error.element, error.message);
        }
        if (this.errorList.length) {
          this.toShow = this.toShow.add(this.containers);
        }
        if (this.settings.success) {
          for (i = 0; this.successList[i]; i++) {
            this.showLabel(this.successList[i]);
          }
        }
        if (this.settings.unhighlight) {
          for (i = 0, elements = this.validElements(); elements[i]; i++) {
            this.settings.unhighlight.call(
              this,
              elements[i],
              this.settings.errorClass,
              this.settings.validClass
            );
          }
        }
        this.toHide = this.toHide.not(this.toShow);
        this.hideErrors();
        this.addWrapper(this.toShow).show();
      },

      validElements: function() {
        return this.currentElements.not(this.invalidElements());
      },

      invalidElements: function() {
        return $(this.errorList).map(function() {
          return this.element;
        });
      },

      showLabel: function(element, message) {
        var place,
          group,
          errorID,
          error = this.errorsFor(element),
          elementID = this.idOrName(element),
          describedBy = $(element).attr("aria-describedby");
        if (error.length) {
          // refresh error/success class
          error
            .removeClass(this.settings.validClass)
            .addClass(this.settings.errorClass);
          // replace message on existing label
          error.html(message);
        } else {
          // create error element
          error = $("<" + this.settings.errorElement + ">")
            .attr("id", elementID + "-error")
            .addClass(this.settings.errorClass)
            .html(message || "");

          // Maintain reference to the element to be placed into the DOM
          place = error;
          if (this.settings.wrapper) {
            // make sure the element is visible, even in IE
            // actually showing the wrapped element is handled elsewhere
            place = error
              .hide()
              .show()
              .wrap("<" + this.settings.wrapper + "/>")
              .parent();
          }
          if (this.labelContainer.length) {
            this.labelContainer.append(place);
          } else if (this.settings.errorPlacement) {
            this.settings.errorPlacement(place, $(element));
          } else {
            place.insertAfter(element);
          }

          // Link error back to the element
          if (error.is("label")) {
            // If the error is a label, then associate using 'for'
            error.attr("for", elementID);
          } else if (
            error.parents("label[for='" + elementID + "']").length === 0
          ) {
            // If the element is not a child of an associated label, then it's necessary
            // to explicitly apply aria-describedby

            errorID = error.attr("id");
            // Respect existing non-error aria-describedby
            if (!describedBy) {
              describedBy = errorID;
            } else if (!describedBy.match(new RegExp("\b" + errorID + "\b"))) {
              // Add to end of list if not already present
              describedBy += " " + errorID;
            }
            $(element).attr("aria-describedby", describedBy);

            // If this element is grouped, then assign to all elements in the same group
            group = this.groups[element.name];
            if (group) {
              $.each(this.groups, function(name, testgroup) {
                if (testgroup === group) {
                  $("[name='" + name + "']", this.currentForm).attr(
                    "aria-describedby",
                    error.attr("id")
                  );
                }
              });
            }
          }
        }
        if (!message && this.settings.success) {
          error.text("");
          if (typeof this.settings.success === "string") {
            error.addClass(this.settings.success);
          } else {
            this.settings.success(error, element);
          }
        }
        this.toShow = this.toShow.add(error);
      },

      errorsFor: function(element) {
        var name = this.idOrName(element),
          describer = $(element).attr("aria-describedby"),
          selector = "label[for='" + name + "'], label[for='" + name + "'] *";
        // aria-describedby should directly reference the error element
        if (describer) {
          selector = selector + ", #" + describer.replace(/\s+/g, ", #");
        }
        return this.errors().filter(selector);
      },

      idOrName: function(element) {
        return (
          this.groups[element.name] ||
          (this.checkable(element) ? element.name : element.id || element.name)
        );
      },

      validationTargetFor: function(element) {
        // if radio/checkbox, validate first element in group instead
        if (this.checkable(element)) {
          element = this.findByName(element.name).not(this.settings.ignore)[0];
        }
        return element;
      },

      checkable: function(element) {
        return /radio|checkbox/i.test(element.type);
      },

      findByName: function(name) {
        return $(this.currentForm).find("[name='" + name + "']");
      },

      getLength: function(value, element) {
        switch (element.nodeName.toLowerCase()) {
          case "select":
            return $("option:selected", element).length;
          case "input":
            if (this.checkable(element)) {
              return this.findByName(element.name).filter(":checked").length;
            }
        }
        return value.length;
      },

      depend: function(param, element) {
        return this.dependTypes[typeof param]
          ? this.dependTypes[typeof param](param, element)
          : true;
      },

      dependTypes: {
        boolean: function(param) {
          return param;
        },
        string: function(param, element) {
          return !!$(param, element.form).length;
        },
        function: function(param, element) {
          return param(element);
        }
      },

      optional: function(element) {
        var val = this.elementValue(element);
        return (
          !$.validator.methods.required.call(this, val, element) &&
          "dependency-mismatch"
        );
      },

      startRequest: function(element) {
        if (!this.pending[element.name]) {
          this.pendingRequest++;
          this.pending[element.name] = true;
        }
      },

      stopRequest: function(element, valid) {
        this.pendingRequest--;
        // sometimes synchronization fails, make sure pendingRequest is never < 0
        if (this.pendingRequest < 0) {
          this.pendingRequest = 0;
        }
        delete this.pending[element.name];
        if (
          valid &&
          this.pendingRequest === 0 &&
          this.formSubmitted &&
          this.form()
        ) {
          $(this.currentForm).submit();
          this.formSubmitted = false;
        } else if (!valid && this.pendingRequest === 0 && this.formSubmitted) {
          $(this.currentForm).triggerHandler("invalid-form", [this]);
          this.formSubmitted = false;
        }
      },

      previousValue: function(element) {
        return (
          $.data(element, "previousValue") ||
          $.data(element, "previousValue", {
            old: null,
            valid: true,
            message: this.defaultMessage(element, "remote")
          })
        );
      }
    },

    classRuleSettings: {
      required: { required: true },
      email: { email: true },
      url: { url: true },
      date: { date: true },
      dateISO: { dateISO: true },
      number: { number: true },
      digits: { digits: true },
      creditcard: { creditcard: true }
    },

    addClassRules: function(className, rules) {
      if (className.constructor === String) {
        this.classRuleSettings[className] = rules;
      } else {
        $.extend(this.classRuleSettings, className);
      }
    },

    classRules: function(element) {
      var rules = {},
        classes = $(element).attr("class");

      if (classes) {
        $.each(classes.split(" "), function() {
          if (this in $.validator.classRuleSettings) {
            $.extend(rules, $.validator.classRuleSettings[this]);
          }
        });
      }
      return rules;
    },

    attributeRules: function(element) {
      var rules = {},
        $element = $(element),
        type = element.getAttribute("type"),
        method,
        value;

      for (method in $.validator.methods) {
        // support for <input required> in both html5 and older browsers
        if (method === "required") {
          value = element.getAttribute(method);
          // Some browsers return an empty string for the required attribute
          // and non-HTML5 browsers might have required="" markup
          if (value === "") {
            value = true;
          }
          // force non-HTML5 browsers to return bool
          value = !!value;
        } else {
          value = $element.attr(method);
        }

        // convert the value to a number for number inputs, and for text for backwards compability
        // allows type="date" and others to be compared as strings
        if (
          /min|max/.test(method) &&
          (type === null || /number|range|text/.test(type))
        ) {
          value = Number(value);
        }

        if (value || value === 0) {
          rules[method] = value;
        } else if (type === method && type !== "range") {
          // exception: the jquery validate 'range' method
          // does not test for the html5 'range' type
          rules[method] = true;
        }
      }

      // maxlength may be returned as -1, 2147483647 ( IE ) and 524288 ( safari ) for text inputs
      if (rules.maxlength && /-1|2147483647|524288/.test(rules.maxlength)) {
        delete rules.maxlength;
      }

      return rules;
    },

    dataRules: function(element) {
      var method,
        value,
        rules = {},
        $element = $(element);
      for (method in $.validator.methods) {
        value = $element.data(
          "rule" +
            method.charAt(0).toUpperCase() +
            method.substring(1).toLowerCase()
        );
        if (value !== undefined) {
          rules[method] = value;
        }
      }
      return rules;
    },

    staticRules: function(element) {
      var rules = {},
        validator = $.data(element.form, "validator");

      if (validator.settings.rules) {
        rules =
          $.validator.normalizeRule(validator.settings.rules[element.name]) ||
          {};
      }
      return rules;
    },

    normalizeRules: function(rules, element) {
      // handle dependency check
      $.each(rules, function(prop, val) {
        // ignore rule when param is explicitly false, eg. required:false
        if (val === false) {
          delete rules[prop];
          return;
        }
        if (val.param || val.depends) {
          var keepRule = true;
          switch (typeof val.depends) {
            case "string":
              keepRule = !!$(val.depends, element.form).length;
              break;
            case "function":
              keepRule = val.depends.call(element, element);
              break;
          }
          if (keepRule) {
            rules[prop] = val.param !== undefined ? val.param : true;
          } else {
            delete rules[prop];
          }
        }
      });

      // evaluate parameters
      $.each(rules, function(rule, parameter) {
        rules[rule] = $.isFunction(parameter) ? parameter(element) : parameter;
      });

      // clean number parameters
      $.each(["minlength", "maxlength"], function() {
        if (rules[this]) {
          rules[this] = Number(rules[this]);
        }
      });
      $.each(["rangelength", "range"], function() {
        var parts;
        if (rules[this]) {
          if ($.isArray(rules[this])) {
            rules[this] = [Number(rules[this][0]), Number(rules[this][1])];
          } else if (typeof rules[this] === "string") {
            parts = rules[this].replace(/[\[\]]/g, "").split(/[\s,]+/);
            rules[this] = [Number(parts[0]), Number(parts[1])];
          }
        }
      });

      if ($.validator.autoCreateRanges) {
        // auto-create ranges
        if (rules.min && rules.max) {
          rules.range = [rules.min, rules.max];
          delete rules.min;
          delete rules.max;
        }
        if (rules.minlength && rules.maxlength) {
          rules.rangelength = [rules.minlength, rules.maxlength];
          delete rules.minlength;
          delete rules.maxlength;
        }
      }

      return rules;
    },

    // Converts a simple string to a {string: true} rule, e.g., "required" to {required:true}
    normalizeRule: function(data) {
      if (typeof data === "string") {
        var transformed = {};
        $.each(data.split(/\s/), function() {
          transformed[this] = true;
        });
        data = transformed;
      }
      return data;
    },

    // http://jqueryvalidation.org/jQuery.validator.addMethod/
    addMethod: function(name, method, message) {
      $.validator.methods[name] = method;
      $.validator.messages[name] =
        message !== undefined ? message : $.validator.messages[name];
      if (method.length < 3) {
        $.validator.addClassRules(name, $.validator.normalizeRule(name));
      }
    },

    methods: {
      // http://jqueryvalidation.org/required-method/
      required: function(value, element, param) {
        // check if dependency is met
        if (!this.depend(param, element)) {
          return "dependency-mismatch";
        }
        if (element.nodeName.toLowerCase() === "select") {
          // could be an array for select-multiple or a string, both are fine this way
          var val = $(element).val();
          return val && val.length > 0;
        }
        if (this.checkable(element)) {
          return this.getLength(value, element) > 0;
        }
        return $.trim(value).length > 0;
      },

      // http://jqueryvalidation.org/email-method/
      email: function(value, element) {
        // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
        // Retrieved 2014-01-14
        // If you have a problem with this implementation, report a bug against the above spec
        // Or use custom methods to implement your own email validation
        return (
          this.optional(element) ||
          /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(
            value
          )
        );
      },

      // http://jqueryvalidation.org/url-method/
      url: function(value, element) {
        // contributed by Scott Gonzalez: http://projects.scottsplayground.com/iri/
        return (
          this.optional(element) ||
          /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(
            value
          )
        );
      },

      // http://jqueryvalidation.org/date-method/
      date: function(value, element) {
        return (
          this.optional(element) ||
          !/Invalid|NaN/.test(new Date(value).toString())
        );
      },

      // http://jqueryvalidation.org/dateISO-method/
      dateISO: function(value, element) {
        return (
          this.optional(element) ||
          /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/.test(
            value
          )
        );
      },

      // http://jqueryvalidation.org/number-method/
      number: function(value, element) {
        return (
          this.optional(element) ||
          /^-?(?:\d+|\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value)
        );
      },

      // http://jqueryvalidation.org/digits-method/
      digits: function(value, element) {
        return this.optional(element) || /^\d+$/.test(value);
      },

      // http://jqueryvalidation.org/creditcard-method/
      // based on http://en.wikipedia.org/wiki/Luhn/
      creditcard: function(value, element) {
        if (this.optional(element)) {
          return "dependency-mismatch";
        }
        // accept only spaces, digits and dashes
        if (/[^0-9 \-]+/.test(value)) {
          return false;
        }
        var nCheck = 0,
          nDigit = 0,
          bEven = false,
          n,
          cDigit;

        value = value.replace(/\D/g, "");

        // Basing min and max length on
        // http://developer.ean.com/general_info/Valid_Credit_Card_Types
        if (value.length < 13 || value.length > 19) {
          return false;
        }

        for (n = value.length - 1; n >= 0; n--) {
          cDigit = value.charAt(n);
          nDigit = parseInt(cDigit, 10);
          if (bEven) {
            if ((nDigit *= 2) > 9) {
              nDigit -= 9;
            }
          }
          nCheck += nDigit;
          bEven = !bEven;
        }

        return nCheck % 10 === 0;
      },

      // http://jqueryvalidation.org/minlength-method/
      minlength: function(value, element, param) {
        var length = $.isArray(value)
          ? value.length
          : this.getLength($.trim(value), element);
        return this.optional(element) || length >= param;
      },

      // http://jqueryvalidation.org/maxlength-method/
      maxlength: function(value, element, param) {
        var length = $.isArray(value)
          ? value.length
          : this.getLength($.trim(value), element);
        return this.optional(element) || length <= param;
      },

      // http://jqueryvalidation.org/rangelength-method/
      rangelength: function(value, element, param) {
        var length = $.isArray(value)
          ? value.length
          : this.getLength($.trim(value), element);
        return (
          this.optional(element) || (length >= param[0] && length <= param[1])
        );
      },

      // http://jqueryvalidation.org/min-method/
      min: function(value, element, param) {
        return this.optional(element) || value >= param;
      },

      // http://jqueryvalidation.org/max-method/
      max: function(value, element, param) {
        return this.optional(element) || value <= param;
      },

      // http://jqueryvalidation.org/range-method/
      range: function(value, element, param) {
        return (
          this.optional(element) || (value >= param[0] && value <= param[1])
        );
      },

      // http://jqueryvalidation.org/equalTo-method/
      equalTo: function(value, element, param) {
        // bind to the blur event of the target in order to revalidate whenever the target field is updated
        // TODO find a way to bind the event just once, avoiding the unbind-rebind overhead
        var target = $(param);
        if (this.settings.onfocusout) {
          target
            .unbind(".validate-equalTo")
            .bind("blur.validate-equalTo", function() {
              $(element).valid();
            });
        }
        return value === target.val();
      },

      // http://jqueryvalidation.org/remote-method/
      remote: function(value, element, param) {
        if (this.optional(element)) {
          return "dependency-mismatch";
        }

        var previous = this.previousValue(element),
          validator,
          data;

        if (!this.settings.messages[element.name]) {
          this.settings.messages[element.name] = {};
        }
        previous.originalMessage = this.settings.messages[element.name].remote;
        this.settings.messages[element.name].remote = previous.message;

        param = (typeof param === "string" && { url: param }) || param;

        if (previous.old === value) {
          return previous.valid;
        }

        previous.old = value;
        validator = this;
        this.startRequest(element);
        data = {};
        data[element.name] = value;
        $.ajax(
          $.extend(
            true,
            {
              url: param,
              mode: "abort",
              port: "validate" + element.name,
              dataType: "json",
              data: data,
              context: validator.currentForm,
              success: function(response) {
                var valid = response === true || response === "true",
                  errors,
                  message,
                  submitted;

                validator.settings.messages[element.name].remote =
                  previous.originalMessage;
                if (valid) {
                  submitted = validator.formSubmitted;
                  validator.prepareElement(element);
                  validator.formSubmitted = submitted;
                  validator.successList.push(element);
                  delete validator.invalid[element.name];
                  validator.showErrors();
                } else {
                  errors = {};
                  message =
                    response || validator.defaultMessage(element, "remote");
                  errors[element.name] = previous.message = $.isFunction(
                    message
                  )
                    ? message(value)
                    : message;
                  validator.invalid[element.name] = true;
                  validator.showErrors(errors);
                }
                previous.valid = valid;
                validator.stopRequest(element, valid);
              }
            },
            param
          )
        );
        return "pending";
      }
    }
  });

  $.format = function deprecated() {
    throw "$.format has been deprecated. Please use $.validator.format instead.";
  };

  // ajax mode: abort
  // usage: $.ajax({ mode: "abort"[, port: "uniqueport"]});
  // if mode:"abort" is used, the previous request on that port (port can be undefined) is aborted via XMLHttpRequest.abort()

  var pendingRequests = {},
    ajax;
  // Use a prefilter if available (1.5+)
  if ($.ajaxPrefilter) {
    $.ajaxPrefilter(function(settings, _, xhr) {
      var port = settings.port;
      if (settings.mode === "abort") {
        if (pendingRequests[port]) {
          pendingRequests[port].abort();
        }
        pendingRequests[port] = xhr;
      }
    });
  } else {
    // Proxy ajax
    ajax = $.ajax;
    $.ajax = function(settings) {
      var mode = ("mode" in settings ? settings : $.ajaxSettings).mode,
        port = ("port" in settings ? settings : $.ajaxSettings).port;
      if (mode === "abort") {
        if (pendingRequests[port]) {
          pendingRequests[port].abort();
        }
        pendingRequests[port] = ajax.apply(this, arguments);
        return pendingRequests[port];
      }
      return ajax.apply(this, arguments);
    };
  }

  // provides delegate(type: String, delegate: Selector, handler: Callback) plugin for easier event delegation
  // handler is only called when $(event.target).is(delegate), in the scope of the jquery-object for event.target

  $.extend($.fn, {
    validateDelegate: function(delegate, type, handler) {
      return this.bind(type, function(event) {
        var target = $(event.target);
        if (target.is(delegate)) {
          return handler.apply(target, arguments);
        }
      });
    }
  });
});

// Generated by CoffeeScript 1.6.2
/*!
jQuery Waypoints - v2.0.5
Copyright (c) 2011-2014 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/jquery-waypoints/blob/master/licenses.txt
*/

(function() {
  var __indexOf =
      [].indexOf ||
      function(item) {
        for (var i = 0, l = this.length; i < l; i++) {
          if (i in this && this[i] === item) return i;
        }
        return -1;
      },
    __slice = [].slice;

  (function(root, factory) {
    if (typeof define === "function" && define.amd) {
      return define("waypoints", ["jquery"], function($) {
        return factory($, root);
      });
    } else {
      return factory(root.jQuery, root);
    }
  })(window, function($, window) {
    var $w,
      Context,
      Waypoint,
      allWaypoints,
      contextCounter,
      contextKey,
      contexts,
      isTouch,
      jQMethods,
      methods,
      resizeEvent,
      scrollEvent,
      waypointCounter,
      waypointKey,
      wp,
      wps;

    $w = $(window);
    isTouch = __indexOf.call(window, "ontouchstart") >= 0;
    allWaypoints = {
      horizontal: {},
      vertical: {}
    };
    contextCounter = 1;
    contexts = {};
    contextKey = "waypoints-context-id";
    resizeEvent = "resize.waypoints";
    scrollEvent = "scroll.waypoints";
    waypointCounter = 1;
    waypointKey = "waypoints-waypoint-ids";
    wp = "waypoint";
    wps = "waypoints";
    Context = (function() {
      function Context($element) {
        var _this = this;

        this.$element = $element;
        this.element = $element[0];
        this.didResize = false;
        this.didScroll = false;
        this.id = "context" + contextCounter++;
        this.oldScroll = {
          x: $element.scrollLeft(),
          y: $element.scrollTop()
        };
        this.waypoints = {
          horizontal: {},
          vertical: {}
        };
        this.element[contextKey] = this.id;
        contexts[this.id] = this;
        $element.bind(scrollEvent, function() {
          var scrollHandler;

          if (!(_this.didScroll || isTouch)) {
            _this.didScroll = true;
            scrollHandler = function() {
              _this.doScroll();
              return (_this.didScroll = false);
            };
            return window.setTimeout(
              scrollHandler,
              $[wps].settings.scrollThrottle
            );
          }
        });
        $element.bind(resizeEvent, function() {
          var resizeHandler;

          if (!_this.didResize) {
            _this.didResize = true;
            resizeHandler = function() {
              $[wps]("refresh");
              return (_this.didResize = false);
            };
            return window.setTimeout(
              resizeHandler,
              $[wps].settings.resizeThrottle
            );
          }
        });
      }

      Context.prototype.doScroll = function() {
        var axes,
          _this = this;

        axes = {
          horizontal: {
            newScroll: this.$element.scrollLeft(),
            oldScroll: this.oldScroll.x,
            forward: "right",
            backward: "left"
          },
          vertical: {
            newScroll: this.$element.scrollTop(),
            oldScroll: this.oldScroll.y,
            forward: "down",
            backward: "up"
          }
        };
        if (isTouch && (!axes.vertical.oldScroll || !axes.vertical.newScroll)) {
          $[wps]("refresh");
        }
        $.each(axes, function(aKey, axis) {
          var direction, isForward, triggered;

          triggered = [];
          isForward = axis.newScroll > axis.oldScroll;
          direction = isForward ? axis.forward : axis.backward;
          $.each(_this.waypoints[aKey], function(wKey, waypoint) {
            var _ref, _ref1;

            if (
              axis.oldScroll < (_ref = waypoint.offset) &&
              _ref <= axis.newScroll
            ) {
              return triggered.push(waypoint);
            } else if (
              axis.newScroll < (_ref1 = waypoint.offset) &&
              _ref1 <= axis.oldScroll
            ) {
              return triggered.push(waypoint);
            }
          });
          triggered.sort(function(a, b) {
            return a.offset - b.offset;
          });
          if (!isForward) {
            triggered.reverse();
          }
          return $.each(triggered, function(i, waypoint) {
            if (waypoint.options.continuous || i === triggered.length - 1) {
              return waypoint.trigger([direction]);
            }
          });
        });
        return (this.oldScroll = {
          x: axes.horizontal.newScroll,
          y: axes.vertical.newScroll
        });
      };

      Context.prototype.refresh = function() {
        var axes,
          cOffset,
          isWin,
          _this = this;

        isWin = $.isWindow(this.element);
        cOffset = this.$element.offset();
        this.doScroll();
        axes = {
          horizontal: {
            contextOffset: isWin ? 0 : cOffset.left,
            contextScroll: isWin ? 0 : this.oldScroll.x,
            contextDimension: this.$element.width(),
            oldScroll: this.oldScroll.x,
            forward: "right",
            backward: "left",
            offsetProp: "left"
          },
          vertical: {
            contextOffset: isWin ? 0 : cOffset.top,
            contextScroll: isWin ? 0 : this.oldScroll.y,
            contextDimension: isWin
              ? $[wps]("viewportHeight")
              : this.$element.height(),
            oldScroll: this.oldScroll.y,
            forward: "down",
            backward: "up",
            offsetProp: "top"
          }
        };
        return $.each(axes, function(aKey, axis) {
          return $.each(_this.waypoints[aKey], function(i, waypoint) {
            var adjustment, elementOffset, oldOffset, _ref, _ref1;

            adjustment = waypoint.options.offset;
            oldOffset = waypoint.offset;
            elementOffset = $.isWindow(waypoint.element)
              ? 0
              : waypoint.$element.offset()[axis.offsetProp];
            if ($.isFunction(adjustment)) {
              adjustment = adjustment.apply(waypoint.element);
            } else if (typeof adjustment === "string") {
              adjustment = parseFloat(adjustment);
              if (waypoint.options.offset.indexOf("%") > -1) {
                adjustment = Math.ceil(
                  (axis.contextDimension * adjustment) / 100
                );
              }
            }
            waypoint.offset =
              elementOffset -
              axis.contextOffset +
              axis.contextScroll -
              adjustment;
            if (
              (waypoint.options.onlyOnScroll && oldOffset != null) ||
              !waypoint.enabled
            ) {
              return;
            }
            if (
              oldOffset !== null &&
              (oldOffset < (_ref = axis.oldScroll) && _ref <= waypoint.offset)
            ) {
              return waypoint.trigger([axis.backward]);
            } else if (
              oldOffset !== null &&
              (oldOffset > (_ref1 = axis.oldScroll) && _ref1 >= waypoint.offset)
            ) {
              return waypoint.trigger([axis.forward]);
            } else if (
              oldOffset === null &&
              axis.oldScroll >= waypoint.offset
            ) {
              return waypoint.trigger([axis.forward]);
            }
          });
        });
      };

      Context.prototype.checkEmpty = function() {
        if (
          $.isEmptyObject(this.waypoints.horizontal) &&
          $.isEmptyObject(this.waypoints.vertical)
        ) {
          this.$element.unbind([resizeEvent, scrollEvent].join(" "));
          return delete contexts[this.id];
        }
      };

      return Context;
    })();
    Waypoint = (function() {
      function Waypoint($element, context, options) {
        var idList, _ref;

        if (options.offset === "bottom-in-view") {
          options.offset = function() {
            var contextHeight;

            contextHeight = $[wps]("viewportHeight");
            if (!$.isWindow(context.element)) {
              contextHeight = context.$element.height();
            }
            return contextHeight - $(this).outerHeight();
          };
        }
        this.$element = $element;
        this.element = $element[0];
        this.axis = options.horizontal ? "horizontal" : "vertical";
        this.callback = options.handler;
        this.context = context;
        this.enabled = options.enabled;
        this.id = "waypoints" + waypointCounter++;
        this.offset = null;
        this.options = options;
        context.waypoints[this.axis][this.id] = this;
        allWaypoints[this.axis][this.id] = this;
        idList = (_ref = this.element[waypointKey]) != null ? _ref : [];
        idList.push(this.id);
        this.element[waypointKey] = idList;
      }

      Waypoint.prototype.trigger = function(args) {
        if (!this.enabled) {
          return;
        }
        if (this.callback != null) {
          this.callback.apply(this.element, args);
        }
        if (this.options.triggerOnce) {
          return this.destroy();
        }
      };

      Waypoint.prototype.disable = function() {
        return (this.enabled = false);
      };

      Waypoint.prototype.enable = function() {
        this.context.refresh();
        return (this.enabled = true);
      };

      Waypoint.prototype.destroy = function() {
        delete allWaypoints[this.axis][this.id];
        delete this.context.waypoints[this.axis][this.id];
        return this.context.checkEmpty();
      };

      Waypoint.getWaypointsByElement = function(element) {
        var all, ids;

        ids = element[waypointKey];
        if (!ids) {
          return [];
        }
        all = $.extend({}, allWaypoints.horizontal, allWaypoints.vertical);
        return $.map(ids, function(id) {
          return all[id];
        });
      };

      return Waypoint;
    })();
    methods = {
      init: function(f, options) {
        var _ref;

        options = $.extend({}, $.fn[wp].defaults, options);
        if ((_ref = options.handler) == null) {
          options.handler = f;
        }
        this.each(function() {
          var $this, context, contextElement, _ref1;

          $this = $(this);
          contextElement =
            (_ref1 = options.context) != null
              ? _ref1
              : $.fn[wp].defaults.context;
          if (!$.isWindow(contextElement)) {
            contextElement = $this.closest(contextElement);
          }
          contextElement = $(contextElement);
          context = contexts[contextElement[0][contextKey]];
          if (!context) {
            context = new Context(contextElement);
          }
          return new Waypoint($this, context, options);
        });
        $[wps]("refresh");
        return this;
      },
      disable: function() {
        return methods._invoke.call(this, "disable");
      },
      enable: function() {
        return methods._invoke.call(this, "enable");
      },
      destroy: function() {
        return methods._invoke.call(this, "destroy");
      },
      prev: function(axis, selector) {
        return methods._traverse.call(this, axis, selector, function(
          stack,
          index,
          waypoints
        ) {
          if (index > 0) {
            return stack.push(waypoints[index - 1]);
          }
        });
      },
      next: function(axis, selector) {
        return methods._traverse.call(this, axis, selector, function(
          stack,
          index,
          waypoints
        ) {
          if (index < waypoints.length - 1) {
            return stack.push(waypoints[index + 1]);
          }
        });
      },
      _traverse: function(axis, selector, push) {
        var stack, waypoints;

        if (axis == null) {
          axis = "vertical";
        }
        if (selector == null) {
          selector = window;
        }
        waypoints = jQMethods.aggregate(selector);
        stack = [];
        this.each(function() {
          var index;

          index = $.inArray(this, waypoints[axis]);
          return push(stack, index, waypoints[axis]);
        });
        return this.pushStack(stack);
      },
      _invoke: function(method) {
        this.each(function() {
          var waypoints;

          waypoints = Waypoint.getWaypointsByElement(this);
          return $.each(waypoints, function(i, waypoint) {
            waypoint[method]();
            return true;
          });
        });
        return this;
      }
    };
    $.fn[wp] = function() {
      var args, method;

      (method = arguments[0]),
        (args = 2 <= arguments.length ? __slice.call(arguments, 1) : []);
      if (methods[method]) {
        return methods[method].apply(this, args);
      } else if ($.isFunction(method)) {
        return methods.init.apply(this, arguments);
      } else if ($.isPlainObject(method)) {
        return methods.init.apply(this, [null, method]);
      } else if (!method) {
        return $.error(
          "jQuery Waypoints needs a callback function or handler option."
        );
      } else {
        return $.error(
          "The " + method + " method does not exist in jQuery Waypoints."
        );
      }
    };
    $.fn[wp].defaults = {
      context: window,
      continuous: true,
      enabled: true,
      horizontal: false,
      offset: 0,
      triggerOnce: false
    };
    jQMethods = {
      refresh: function() {
        return $.each(contexts, function(i, context) {
          return context.refresh();
        });
      },
      viewportHeight: function() {
        var _ref;

        return (_ref = window.innerHeight) != null ? _ref : $w.height();
      },
      aggregate: function(contextSelector) {
        var collection, waypoints, _ref;

        collection = allWaypoints;
        if (contextSelector) {
          collection =
            (_ref = contexts[$(contextSelector)[0][contextKey]]) != null
              ? _ref.waypoints
              : void 0;
        }
        if (!collection) {
          return [];
        }
        waypoints = {
          horizontal: [],
          vertical: []
        };
        $.each(waypoints, function(axis, arr) {
          $.each(collection[axis], function(key, waypoint) {
            return arr.push(waypoint);
          });
          arr.sort(function(a, b) {
            return a.offset - b.offset;
          });
          waypoints[axis] = $.map(arr, function(waypoint) {
            return waypoint.element;
          });
          return (waypoints[axis] = $.unique(waypoints[axis]));
        });
        return waypoints;
      },
      above: function(contextSelector) {
        if (contextSelector == null) {
          contextSelector = window;
        }
        return jQMethods._filter(contextSelector, "vertical", function(
          context,
          waypoint
        ) {
          return waypoint.offset <= context.oldScroll.y;
        });
      },
      below: function(contextSelector) {
        if (contextSelector == null) {
          contextSelector = window;
        }
        return jQMethods._filter(contextSelector, "vertical", function(
          context,
          waypoint
        ) {
          return waypoint.offset > context.oldScroll.y;
        });
      },
      left: function(contextSelector) {
        if (contextSelector == null) {
          contextSelector = window;
        }
        return jQMethods._filter(contextSelector, "horizontal", function(
          context,
          waypoint
        ) {
          return waypoint.offset <= context.oldScroll.x;
        });
      },
      right: function(contextSelector) {
        if (contextSelector == null) {
          contextSelector = window;
        }
        return jQMethods._filter(contextSelector, "horizontal", function(
          context,
          waypoint
        ) {
          return waypoint.offset > context.oldScroll.x;
        });
      },
      enable: function() {
        return jQMethods._invoke("enable");
      },
      disable: function() {
        return jQMethods._invoke("disable");
      },
      destroy: function() {
        return jQMethods._invoke("destroy");
      },
      extendFn: function(methodName, f) {
        return (methods[methodName] = f);
      },
      _invoke: function(method) {
        var waypoints;

        waypoints = $.extend(
          {},
          allWaypoints.vertical,
          allWaypoints.horizontal
        );
        return $.each(waypoints, function(key, waypoint) {
          waypoint[method]();
          return true;
        });
      },
      _filter: function(selector, axis, test) {
        var context, waypoints;

        context = contexts[$(selector)[0][contextKey]];
        if (!context) {
          return [];
        }
        waypoints = [];
        $.each(context.waypoints[axis], function(i, waypoint) {
          if (test(context, waypoint)) {
            return waypoints.push(waypoint);
          }
        });
        waypoints.sort(function(a, b) {
          return a.offset - b.offset;
        });
        return $.map(waypoints, function(waypoint) {
          return waypoint.element;
        });
      }
    };
    $[wps] = function() {
      var args, method;

      (method = arguments[0]),
        (args = 2 <= arguments.length ? __slice.call(arguments, 1) : []);
      if (jQMethods[method]) {
        return jQMethods[method].apply(null, args);
      } else {
        return jQMethods.aggregate.call(null, method);
      }
    };
    $[wps].settings = {
      resizeThrottle: 100,
      scrollThrottle: 30
    };
    return $w.on("load.waypoints", function() {
      return $[wps]("refresh");
    });
  });
}.call(this));
