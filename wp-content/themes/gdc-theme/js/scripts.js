var s,
NAV = {
  settings: {
      bodyEl: jQuery('body'),
      openbtn: jQuery('.menu-btn'),
      mainMenu: jQuery( "nav.main-nav" ),
      utilityMenu: jQuery('nav.secondary-nav ul'),
      phone: jQuery('.contact-info .number'),
      appt: jQuery('.contact-info .btn'),
      subMenuToggle: jQuery('.sub-btn')
  },
  init: function() {

    s = this.settings;

    this.bindUIActions();
    
    mediaCheck({
      media: '(min-width: 1100px)',
      entry: function() {
        NAV.desktopInit();
      },
      exit: function() {
        NAV.mobileInit();
      }
    });
  },
  bindUIActions: function() {
    s.openbtn.on("click", function() {
      if(jQuery(this).hasClass('open')) {
        NAV.closeMenu();
      }
      else {
        NAV.displayMenu();
      }
    });
    s.subMenuToggle.on('click',function(){
      NAV.toggleSubMenu(this);
    });
  },
  displayMenu: function (e){
    s.bodyEl.addClass('menu-displayed');
    s.openbtn.addClass('open');
  },
  closeMenu: function(e){
    s.bodyEl.removeClass('menu-displayed');
    s.openbtn.removeClass('open');
  },
  toggleSubMenu: function(el){
    jQuery(el).siblings('.sub-menu').slideToggle();
    jQuery(el).toggleClass('active-sub-btn');
  },
  mobileInit: function (){
    if(!$('.utility-duplicate').length){
      s.bodyEl.addClass('mobile-menu').removeClass('desktop-menu');
      
      //  DUPLICATE UTILITY NAV and SOCIAL ICONS AT BOTTOM OF SLIDE OUT MENU
      s.utilityMenu.clone().addClass('utility-duplicate').appendTo(s.mainMenu);
      s.appt.clone().addClass('appt-duplicate').appendTo(s.mainMenu);
      s.phone.clone().addClass('mobile-number').appendTo('.secondary-nav-container');
      s.mainMenu.addClass('mobile-nav');
      s.openbtn.show();
    };
  },
  mobileDestroy: function (){
    s.bodyEl.removeClass('menu-displayed mobile-menu').addClass('desktop-menu');
    s.openbtn.hide();
    jQuery('.utility-duplicate,.submenu-toggle,.appt-duplicate,.mobile-number').remove();
    s.mainMenu.removeClass('mobile-nav');
  },
  desktopInit: function (){
    NAV.mobileDestroy();
  }
}; //END NAV


jQuery(document).ready(function() {

  jQuery("#contact").validate();

  NAV.init();


  //back-to-top button
  var body = jQuery('body');
  var waypointTrigger = jQuery('.content-wrapper');
  var bttBtn = jQuery('a.btt-btn');
  
  waypointTrigger.waypoint(function(direction) {
    body.toggleClass('displaybtt');
  });
  
  bttBtn.on('click',function(event){
    jQuery('body,html').animate({scrollTop:0},800);
    return false;
  });


  jQuery(window).bind("load", function() {

    jQuery(function() {
      jQuery('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') === this.pathname.replace(/^\//,'') && location.hostname === this.hostname) {
          var target = jQuery(this.hash);
          target = target.length ? target : jQuery('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            jQuery('html,body').animate({
              scrollTop: target.offset().top
            }, 1000);
            return false;
          }
        }
      });
    });


  });

  jQuery(".acc-content").hide();
  jQuery(".acc-btn").click(function() {
    jQuery(this).show().toggleClass("acc-active").next().slideToggle('normal');

  });
  jQuery(".sidebar span.sub-btn").next().hide();
  jQuery(".sidebar span.sub-btn").on('click', function() {
    jQuery(this).next().slideToggle('normal');
    jQuery(this).toggleClass("active-sub-btn");
  });

  jQuery(".sidebar faq-btn").next().hide();
  jQuery(".sidebar faq-btn").on('click', function() {
    jQuery(this).next().slideToggle('normal');
    jQuery(this).toggleClass("active-faq-btn");
  });

  jQuery('.home-slider.activate-slider').bxSlider({
    auto:true,
    adaptiveHeight: true,
    speed: 1000,
    pause: 7000,
    onSliderLoad: function() {
      jQuery('.home-slider.activate-slider').parent().parent().parent().addClass('loaded');
    }
  });
  
  jQuery('.secondary-slider').bxSlider({
    auto:true,
    speed: 1000,
    pause: 7000
  });
  
});