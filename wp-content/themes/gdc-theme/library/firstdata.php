<?php

class FirstData
{
    /**
     * Configuration
     */
    private $sandbox = FALSE;

    /**
     * Transaction type refers to the codes on this page.  Yours will probably also be 00.
     * Ref:  
     * @var string
     */
    private $transaction_type = "00";

    private $password = array();
    private $exactId = array();
    private $gateway_address = array();

    /**
     * Tweak all of these variables according to your needs -- you could even
     * create another object (e.g. FirstData\Transaction) if you wanted to separate everything
     * into a separate class, though that may not be necessary for a project of this simplicity. 
     */
    private $cc_number;
    private $cc_expiry;
    private $cc_cvv;
    private $cardholder_name; 
    private $patient_name;
    private $payment_amount;

    public function __construct()
    {
        $this->exactId['sandbox'] = "AJ1433-01";
        $this->exactId['production'] = "D18278-01";

        $this->password['sandbox'] = "9w3ren95k4xp827wkqx252j68y46t526";
        $this->password['production'] = "lgxmncmqg1bdl98e00ih4679u12e6z26";

        $this->gateway_address['sandbox'] = 'https://api.demo.globalgatewaye4.firstdata.com/transaction/v11';
        $this->gateway_address['production'] = 'https://api.globalgatewaye4.firstdata.com/transaction/v11';
    }

    /**
     * Sets reservation variables that will ultimately sent to FirstData
     * @param  string $cc_number            CC Number
     * @param  string $cc_expiry            4 digit expiry e.g. 0414
     * @param  string $cc_cvv               CC CVV 
     * @param  string $cardholder_name      Cardholder's Name
     * @param  string $patient_name         Patients's Name
     * @param  string $payment_amount       Amount being paid
     * @return array                        Information passed from send information regarding transaction
     */
    public function makePayment($cc_number, $cc_expiry, $cc_cvv, $cardholder_name, $patient_name, $payment_amount)
    {
        $this->cc_number = $cc_number;
        $this->cc_expiry = $cc_expiry;
        $this->cc_cvv = $cc_cvv;
        $this->cardholder_name = $cardholder_name;
        $this->customer_ref = $patient_name;
        $this->payment_amount = $payment_amount;

        return $this->sendPayment();
    }

    /**
     * Send Payment
     * @return array Data pertaining to transaction submission.
     */
    public function sendPayment()
    {
        if ($this->sandbox) {
            $gateway_address = $this->gateway_address['sandbox'];
            $exactId = $this->exactId['sandbox'];
            $password = $this->password['sandbox'];
        } else {
            $gateway_address = $this->gateway_address['production'];
            $exactId = $this->exactId['production'];
            $password = $this->password['production'];
        }
        $json_array = array(
                'gateway_id' => $exactId,
                'password' => $password,
                'transaction_type' => $this->transaction_type,
                'amount' => $this->payment_amount,
                'cardholder_name' => $this->cardholder_name,
                'cc_number' => $this->cc_number,
                'cc_expiry' => $this->cc_expiry,
                'cc_cvv' => $this->cc_cvv,
                'customer_ref' => $this->customer_ref);

        //var_dump($json_array);
        $transaction_string = json_encode($json_array);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $gateway_address);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $transaction_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json;charset=UTF-8',
        'Content-Length: ' . strlen($transaction_string),
        'Accept: application/json'));
        $result = curl_exec($ch);
        $error  = curl_error($ch);
        //var_dump($result);
        
        if ($error || ! $result) {
            return array("success"=>false, "error"=>"Could not contact Payment Gateway.  Please try again later");
        } else {
            //var_dump(json_decode($result));
            //Cheap JSON Check
            if (substr($result, 0, 1) == "{" || substr($result, 0, 1) == '[') {
                $result_obj = json_decode($result);
            } else {
                $result_obj->exact_message = $result;
            }
            if (isset($result_obj->transaction_approved) && $result_obj->transaction_approved == 1) {
                return array("success"=>true, "authorization"=>$result_obj->authorization_num);
            } else {
                if (isset($result_obj->bank_message) && strlen($result_obj->bank_message)) {
                    return array("success"=>false, "error"=>$result_obj->bank_message);
                } else {
                    return array("success"=>false, "error"=>$result_obj->exact_message);
                }
            }
        }
    }
}

/* End of file firstdata.php */
