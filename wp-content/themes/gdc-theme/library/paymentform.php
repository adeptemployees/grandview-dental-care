<?php 
	require_once("firstdata.php");
	$firstdata = new FirstData();
	
	if($_POST['cardholder_firstname'] !== "" && 
		$_POST['cardholder_lastname'] !== "" && 
		$_POST['patient_firstname'] !== "" && 
		$_POST['patient_lastname'] !== "" && 
		$_POST['card_type'] !== "" && 
		$_POST['card_number'] !== "" && 
		$_POST['card_exp'] !== "" &&
		$_POST['card_cvv'] !== "" && 
		$_POST['payment_amount'] !== "" && 
		$_POST['conf_email'] !== ""
	){	
		$cardholder_name = $_POST['cardholder_firstname']." ".$_POST['cardholder_lastname'];
		$patient_name = $_POST['patient_firstname']." ".$_POST['patient_lastname'];
		$cc_expiry = str_replace("/", "", $_POST['card_exp']);
		$cc_number = str_replace(array(' ', '-'), array('', ''), $_POST['card_number']);
		$cc_cvv	= $_POST['card_cvv'];
		$payment_amount = str_replace("$", "", $_POST['payment_amount']);
		
		$result = $firstdata->makePayment($cc_number, $cc_expiry, $cc_cvv, $cardholder_name, $patient_name, $payment_amount);
		//var_dump($result);
		
		
		if($result['success']) {

			//set up email addresses
			$admin_email = "beth@grandviewdentalcare.com";
			$user_email = $_POST['conf_email'];

			//email subjects
			$admin_subject = 'Payment processed for '.$patient_name;
			$user_subject = 'Thank you for your payment to Grandview Dental Care';

			//email message
			$admin_message = '<html><body>';
			$admin_message .= '<p>A payment has been submitted by <strong>'.$cardholder_name.'</strong> for <strong>'.$patient_name.'</strong> in the amount of <strong>$'.$payment_amount.'</strong>.</p>' . "\r\n\r\n";
			$admin_message .= '<p><strong>Additional Data:</strong></p>' . "\r\n";
			$admin_message .= '<p><strong>Auth Code: </strong>'.$result['authorization'] . "\r\n";
			$admin_message .= '<br><strong>Card Type: </strong>'.$_POST['card_type'] . "\r\n";
			$admin_message .= '<br><strong>Card Number (last 4): </strong>'. substr($cc_number, -4) . "\r\n";
			$admin_message .= '<br><strong>Exp Date: </strong>'. $_POST['card_exp'] . "\r\n";
			$admin_message .= '<br><strong>User Email: </strong> '.$user_email . '</p>' . "\r\n";
			$admin_message .= '</body></html>';


			$user_message = '<html><body>';
			$user_message .= '<p>Thank you for submitting a payment.  We will process the payment and apply it to the account in the next 1-3 business days.</p>' . "\r\n\r\n";
			$user_message .= '<p>Below is the data that was submitted to Grandview Dental:</p>' . "\r\n";
			$user_message .= '<p><strong>Patient Name: </strong>'.$patient_name . "\r\n";
			$user_message .= '<br><strong>Cardholder Name: </strong>'.$cardholder_name . "\r\n";
			$user_message .= '<br><strong>Card Type: </strong>'.$_POST['card_type'] . "\r\n";
			$user_message .= '<br><strong>Card Number (last 4): </strong>'. substr($cc_number, -4) . "\r\n";
			$user_message .= '<br><strong>Exp Date: </strong>'.$_POST['card_exp'] . "\r\n";
			$user_message .= '<br><strong>Payment Amount: </strong>$'.$payment_amount . '</p>' . "\r\n\r\n";
			$user_message .= '<p>If you have any questions or concerns, please <a href="https://www.grandviewdentalcare.com/contact-us/">contact us</a>.</p>' . "\r\n\r\n";
			$user_message .= '<p>Thanks,' . "\r\n";
			$user_message .= '<br>The Grandview Dental Team' . "\r\n";
			$user_message .= '<br><a href="https://www.grandviewdentalcare.com">Grandview Dental Care</a>' . "\r\n";
			$user_message .= '</body></html>';


			//email headers
			$admin_headers = 'MIME-Version: 1.0' . "\r\n";
			$admin_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$admin_headers .= 'From: info@grandviewdentalcare.com' . "\r\n";
			$admin_headers .= 'Reply-To: ' . $user_email . "\r\n";
			$admin_headers .= 'X-Mailer: PHP/' . phpversion();

			$user_headers = 'MIME-Version: 1.0' . "\r\n";
			$user_headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			$user_headers .= 'From: info@grandviewdentalcare.com' . "\r\n";
			$user_headers .= 'Reply-To: info@grandviewdentalcare.com' . "\r\n";
			$user_headers .= 'X-Mailer: PHP/' . phpversion();
			


			mail($admin_email, $admin_subject, $admin_message, $admin_headers);
			mail("forms@marketingadept.com", $admin_subject, $admin_message, $admin_headers);
			mail($user_email, $user_subject, $user_message, $user_headers);

			$host  = $_SERVER['HTTP_HOST'];
			header("Location: http://$host/make-a-payment/?success=yes");
		}
		else {
			$host  = $_SERVER['HTTP_HOST'];
			header("Location: http://$host/make-a-payment/?success=no");	
		}
	}
	else {
		$host  = $_SERVER['HTTP_HOST'];
		header("Location: http://$host/make-a-payment/?success=no");
	}
	
	
	
	
	
	
	
?>