<?php
/*
Template Name: Full Width Banded Page
*/
?>



<?php get_header();?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <div class="page-title services-tech-intro<?php echo (is_page('new-patient-special') || is_page('refer') || is_page('dental-seminar') ? ' refer-page' : ''); ?>"><!--start page title-->

    <div class="container"><!--start container-->
        <div class="intro">
            <?php if(!is_page('new-patient-special') && !is_page('refer')): ?>
            <?php
                if(get_field('alternate_headline')) {
                    echo '<h1>'.get_field('alternate_headline').'</h1>';
                } else {
                    echo '<h1>'.get_the_title().'</h1>';
                }
            ?>
            <?php echo the_field('intro_content'); ?>
            <?php else: ?>
                <div class="referer-intro">
                    <?php echo the_field('intro_content'); ?>
                </div>
            <?php endif; ?>
        </div>
        
        <?php if(has_post_thumbnail()) { 
            $img = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
            $img_med = aq_resize($img, 600);
            if($img_med) {
                echo '<div class="feature-image"><img src="'.$img_med.'" width="600"></div>';
            } else {
                echo '<div class="feature-image">'.get_the_post_thumbnail($page->ID, 'medium').'</div>'; 
            }
        } ?>

    </div><!--end container-->

</div><!--end page title-->

    <div class="page"><!--start page-->

        <div class="main-content-wrap"><!--start main content wrap-->

            <?php if( have_rows('layout') ):?>

                <?php while ( have_rows('layout') ) : the_row();?>

                    <?php if( get_row_layout() == 'general_content' ):?>

                        <?php if( get_sub_field('content') ): ?>
                            <div class="full-wrap"><!--start full-wrap-->
                            <div class="container"><!--start container-->
                            <div class="wrap"><!--start wrap-->
                            <div class="main-content-full"><!--start main content-->
                            <div class="content row"><!--start content-->
    
                                <?php the_sub_field('content');?>
    
                            </div><!--end content-->
                            </div><!--end main content-->
                            </div><!--end wrap-->
                            </div><!--end container-->
                            </div>
                        <?php endif; ?>

                    <?php elseif( get_row_layout() == 'call_to_action' ):?> 
                        <div class="full-wrap"><!--start full-wrap-->
                        <div class="container"><!--start container-->
                        <div class="wrap"><!--start wrap-->
                        <div class="main-content-full"><!--start main content-->
                        <div class="content row"><!--start row-->
        
                            <div class="cta"><!--start cta-->
                                <div class="bg-image" style="background-image:url('<?php echo the_sub_field('background_image');?>');"></div>
                                <?php if(get_sub_field('background_image')) { echo '<div class="overlay"></div>'; } ?>
                                <div class="content">
                                    <?php the_sub_field('content');?>
            
                                    <?php if( get_sub_field('button_text') ): ?>
                                        <a href="<?php the_sub_field('button_link');?>" class="btn green-btn xl-btn"><?php the_sub_field('button_text');?></a>
                                    <?php endif; ?>
                                </div>
                            </div><!--end cta-->
        
                        </div><!--end row-->
                        </div><!--end main content-->
                        </div><!--end wrap-->
                        </div><!--end container-->
                        </div><!--end full-wrap-->
                    <?php elseif( get_row_layout() == 'page_button' ):?> 
                        <div class="full-wrap"><!--start full-wrap-->
                        <div class="container"><!--start container-->
                        <div class="wrap"><!--start wrap-->
                        <div class="main-content-full"><!--start main content-->
                        <div class="content row"><!--start row-->
                
                            <?php if( have_rows('button') ):?>
                
                                <!--loop through the rows of data-->
                                <?php while ( have_rows('button') ) : the_row();?>
                
                                    <?php if( get_sub_field('button_text') ): ?>
                                        <a href=" <?php if( get_sub_field('button_link') ): ?><?php the_sub_field('button_link');?><?php endif; ?> <?php if( get_sub_field('button_file') ): ?><?php the_sub_field('button_file');?><?php endif; ?>" class="btn <?php the_sub_field('button_color');?> <?php the_sub_field('button_size');?>"><?php the_sub_field('button_text');?></a>
                                    <?php endif; ?>

                                <?php endwhile;?>
                    
                            <?php endif; ?>

                        </div><!--end row-->
                        </div><!--end main content-->
                        </div><!--end wrap-->
                        </div><!--end container-->
                        </div><!--end full-wrap-->
                    <?php elseif( get_row_layout() == 'video' ):?>
                        <div class="full-wrap"><!--start full-wrap-->
                        <div class="container"><!--start container-->
                        <div class="wrap"><!--start wrap-->
                        <div class="main-content-full"><!--start main content-->
                        <div class="content row"><!--start content-->
                    
                            <?php if( get_sub_field('video_embed') ): ?>
                    
                                <div class="video-wrapper"><!--start video wrapper-->
                        
                                    <?php the_sub_field('video_embed');?>
                        
                                </div><!--end video wrapper-->

                            <?php endif; ?>

                        </div><!--end content-->
                        </div><!--end main content-->
                        </div><!--end wrap-->
                        </div><!--end container-->
                        </div><!--end full-wrap-->
                    <?php elseif( get_row_layout() == 'testimonials' ):?>
                        <div class="full-wrap"><!--start full-wrap-->
                        <div class="container"><!--start container-->
                        <div class="wrap"><!--start wrap-->
                        <div class="main-content-full"><!--start main content-->     
                        <div class="row testimonials"><!--start content-->
                    
                            <?php if( have_rows('testimonial') ):?>
                    
                                <!--loop through the rows of data-->
                                <?php while ( have_rows('testimonial') ) : the_row();?>
                        
                                    <div class="testimonial content row"><!--start single call out-->
                            
                                    <?php if( get_sub_field('content') ): ?>
                        
                                        <?php the_sub_field('content');?>
                        
                                    <?php endif; ?>
                    
                                    <?php if( get_sub_field('cite') ): ?>
                    
                                        <cite>- <?php the_sub_field('cite');?></cite>
                    
                                    <?php endif; ?>
                    
                                    </div><!--end testimonial-->
                    
                                <?php endwhile;?>

                            <?php endif;?>

                        </div><!--end testimonials-->
                        </div><!--end main content-->
                        </div><!--end wrap-->
                        </div><!--end container-->
                        </div><!--end full-wrap-->
                    <?php elseif( get_row_layout() == 'faqs' ):?> 
                        <div class="full-wrap"><!--start full-wrap-->
                        <div class="container"><!--start container-->
                        <div class="wrap"><!--start wrap-->
                        <div class="main-content-full"><!--start main content-->       
                        <div class="faqs content row"><!--start content-->
                    
                            <?php if( have_rows('faq_group') ):?>
                    
                                <?php while ( have_rows('faq_group') ) : the_row();?>
                    
                                    <h2 class="faq-title"><?php the_sub_field('faq_group_title');?></h2>
                            
                                    <div class="single-faq"><!--single faq-->
                            
                                        <?php if( have_rows('single_faq') ):?>
                        
                                            <?php while ( have_rows('single_faq') ) : the_row();?>
                        
                                                <span class="m-btn acc-btn"><?php the_sub_field('faq_title');?> <i class="fa fa-caret-down"></i></span>
                        
                                                <div class="acc-content">
                        
                                                    <?php the_sub_field('faq_content');?>
                        
                                                </div><!--end acc content-->
                        
                                            <?php endwhile;?>
                        
                                        <?php endif; ?>
                    
                                    </div><!--end single faq-->

                                <?php endwhile;?>

                            <?php endif; ?>

                        </div><!--end faqs-->
                        </div><!--end main content-->
                        </div><!--end wrap-->
                        </div><!--end container-->
                        </div><!--end full-wrap-->
                    <?php elseif( get_row_layout() == 'page_slider' ):?>
                        <div class="full-wrap"><!--start full-wrap-->
                        <div class="container"><!--start container-->
                        <div class="wrap"><!--start wrap-->
                        <div class="main-content-full"><!--start main content-->
                        <div class="content row page-slider"><!--start content-->
                    
                            <div class="slide-wrap"><!--start slide wrap-->
                    
                                <ul class="bxslider secondary-slider">
                    
                                    <?php if( have_rows('slide') ):?>
                    
                                        <!--loop through the rows of data-->
                                        <?php while ( have_rows('slide') ) : the_row();?>
                    
                                            <li>
                    
                                                <?php if( get_sub_field('slide_image') ): ?>
                    
                                                    <img src="<?php the_sub_field('slide_image');?>">
                    
                                                <?php endif; ?>
                    
                                            </li>
                    
                                        <?php endwhile;?>
                    
                                    <?php endif;?>
                    
                                </ul>
                    
                            </div><!--end slide wrap-->

                        </div><!--end content-->
                        </div><!--end main content-->
                        </div><!--end wrap-->
                        </div><!--end container-->
                        </div><!--end full-wrap-->
                    <?php elseif( get_row_layout() == 'full_columns' ):?> 
                        <?php
                            if( get_sub_field('background_style') && get_sub_field('background_style') !== 'none' ) {
                                $bg_style = ' ' . get_sub_field('background_style') . '-bg';
                            } else {
                                $bg_style = '';
                            }
                        ?>
                        <div class="full-wrap<?php print $bg_style; ?>"><!--start full-wrap-->
                        <div class="container"><!--start container-->
                        <div class="wrap"><!--start wrap-->
                        <div class="main-content-full"><!--start main content-->
                        <div class="content row"><!--start content-->

                            <?php if( have_rows('column') ):?>
                    
                                <!--check if the repeater field has rows of data-->
                                <?php while ( have_rows('column') ) : the_row();?>
                        
                                    <div class="col-12"><!--start col 12-->
                            
                                        <!--display a sub field value-->
                                        <?php if( get_sub_field('content') ): ?>
                                            <?php the_sub_field('content');?>
                        
                                        <?php endif; ?>                                
                        
                                    </div><!--end col 12-->
                    
                                <?php endwhile;?>

                            <?php endif;?>

                        </div><!--end content-->
                        </div><!--end main content-->
                        </div><!--end wrap-->
                        </div><!--end container-->
                        </div><!--end full-wrap-->
                    <?php elseif( get_row_layout() == 'half_columns' ):?> 
                        
                        <div class="container"><!--start container-->
                        <div class="wrap"><!--start wrap-->
                        <div class="main-content-full"><!--start main content-->        
                        <div class="content row"><!--start content-->
                    
                            <?php if( have_rows('column') ):?>
                    
                                <!--check if the repeater field has rows of data-->
                                <?php while ( have_rows('column') ) : the_row();?>
                        
                                    <div class="col-6"><!--start col 6-->
                            
                                        <!--display a sub field value-->
                                        <?php if( get_sub_field('content') ): ?>
                                            <?php the_sub_field('content');?>
                        
                                        <?php endif; ?>                                
                        
                                    </div><!--end col 6-->
                    
                                <?php endwhile;?>

                            <?php endif;?>

                        </div><!--end content-->
                        </div><!--end main content-->
                        </div><!--end wrap-->
                        </div><!--end container-->
                        </div><!--end full-wrap-->
                    <?php elseif( get_row_layout() == 'third_columns' ):?> 
                        <div class="full-wrap"><!--start full-wrap-->
                        <div class="container"><!--start container-->
                        <div class="wrap"><!--start wrap-->
                        <div class="main-content-full"><!--start main content-->
                        <div class="content row"><!--start content-->
                    
                            <?php if( have_rows('column') ):?>
                    
                                <!--check if the repeater field has rows of data-->
                                <?php while ( have_rows('column') ) : the_row();?>
                        
                                    <div class="col-4"><!--start col 3-->
                            
                                        <!--display a sub field value-->
                                        <?php if( get_sub_field('content') ): ?>
                                            <?php the_sub_field('content');?>
                            
                                        <?php endif; ?>                                
                            
                                    </div><!--end col 3-->

                                <?php endwhile;?>

                            <?php endif;?>

                        </div><!--end content-->
                        </div><!--end main content-->
                        </div><!--end wrap-->
                        </div><!--end container-->
                        </div><!--end full-wrap-->
                    <?php else: ?>

                    <?php endif; ?>

                <?php endwhile; ?>

            <?php else :?>

                <!--other content goes here-->

            <?php endif;?><!--end flexible content-->

            <?php if(is_page('current-patients')): ?>
                <div class="container"><!--start container-->
                <div class="wrap"><!--start wrap-->
                <div class="main-content-full"><!--start main content-->
                <?php
                //page is current-patients - display latest blog post excerpt
                $args = array('numberposts' => 1, 'post_type' => 'post', 'post_status' => 'publish');
                $recent_post = get_posts($args);
                foreach ($recent_post as $post) {
                    setup_postdata($post);
                    echo '<p><strong><a href="'.get_permalink().'">'.get_the_title().'</a></strong><br>'.get_the_excerpt().'... <strong><a href="'.get_permalink().'">Continue Reading &raquo;</a></strong></p>';
                }?>
                </div><!--end main content-->
                </div><!--end wrap-->
                </div><!--end container-->
            <?php endif; ?>

        </div><!--end main content wrap-->

    </div> <!-- page -->

<?php endwhile; endif; ?><!--end loop-->

<?php get_footer();?>