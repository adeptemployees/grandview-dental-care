
		<?php get_header();?>

			<div class="slider"><!--start slider-->

				<ul class="bxslider"><!--start bx slider-->

						<!-- check if the repeater field has rows of data-->
						<?php if( have_rows('home_page_slide') ): ?>

						 	<!--loop through the rows of data-->
						   <?php while ( have_rows('home_page_slide') ) : the_row(); ?>


					<li>
						<div class="slide" style="background: #000 url('<?php the_sub_field('slide_background');?>') center center no-repeat; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php the_sub_field('slide_bg');?>', sizingMethod='scale');-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php the_sub_field('slide_bg');?>', sizingMethod='scale')";"><!--start slide bg-->

						        <!--display a sub field value-->

						       <!--check if the flexible content field has rows of data-->
								<?php if( have_rows('slide') ):?>

								     <!--loop through the rows of data-->
								    <?php while ( have_rows('slide') ) : the_row();?>

								    	<div class="slide-content"><!--start slide content-->

									        <?php if( get_row_layout() == 'general_slide' ):?>

									        <?php if( get_sub_field('slide_title') ): ?>
												<h1><?php the_sub_field('slide_title');?></h1>
											<?php endif; ?>

											<?php if( get_sub_field('slide_description') ): ?>
												<p><?php the_sub_field('slide_description');?></p>
											<?php endif; ?>

											<?php if( get_sub_field('slide_button_text') ): ?>
											<a href="<?php the_sub_field('slide_button_link');?>" class="btn orange-btn"><?php the_sub_field('slide_button_text');?></a>
											<?php endif; ?>

									        <?php elseif( get_row_layout() == 'testimonial_slide' ):?> 

									        	<?php if( get_sub_field('testimonial') ): ?>
												<span class="slide-quote"><?php the_sub_field('testimonial');?></span>
												<?php endif; ?>

												<?php if( get_sub_field('slide_button_text') ): ?>
												<a href="<?php the_sub_field('slide_button_link');?>" class="btn orange-btn"><?php the_sub_field('slide_button_text');?></a>
												<?php endif; ?>


									        <?php endif;?>

									    <?php endwhile;?>


										<?php else :?>

							    <!--no layouts found-->

										</div><!--end slide content-->

								<?php endif;?>

							
						</div><!--end slide bg-->

					</li>

					<?php endwhile; ?>

						<?php else : ?>

					    <!--no slides found-->

					<?php endif; ?>

					<!--end slide-->
					

				</ul><!-- end bx slider-->
				
			</div><!--end slider-->

			<div class="home-call-outs"><!--start home call outs-->

				<!--check if the repeater field has rows of data-->
					<?php if( have_rows('home_call_out') ):?>

					<!--loop through the rows of data-->
					    <?php while ( have_rows('home_call_out') ) : the_row();?>


				<div class="home-call-out" style="background: #000 url('<?php the_sub_field('call_out_bg');?>') center center no-repeat; -webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover; background-size: cover; filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php the_sub_field('call_out_bg');?>', sizingMethod='scale');-ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src='<?php the_sub_field('call_out_bg');?>', sizingMethod='scale')";"><!--start home call out-->

					<div class="content"><!--start content-->

						<?php if( get_sub_field('title') ): ?>
						<h2><?php the_sub_field('title');?></h2>
						<?php endif; ?>

						<?php if( get_sub_field('description') ): ?>
						<p><?php the_sub_field('description');?></p>
						<?php endif; ?>

						<?php if( get_sub_field('button_text') ): ?>
						<a href="<?php the_sub_field('button_link');?>" class="btn ghost-btn"><?php the_sub_field('button_text');?></a>
						<?php endif; ?>

					</div><!--end content-->

				</div><!--end home call out-->

				 <?php endwhile;?>

				<?php else :?>

				    <!--no rows found-->

				<?php endif;?>
				
			</div><!--end home call outs-->

			<div class="orgs"><!--start organizations-->

				<?php if( get_field('organizations_title') ): ?>
				<h3><?php the_field('organizations_title');?></h3>
				<?php endif; ?>

				<?php if( get_field('organizations_description') ): ?>
				<p><?php the_field('organizations_description');?></p>
				<?php endif; ?>

				<ul class="org-list">

						<!--check if the repeater field has rows of data-->
						<?php if( have_rows('organization') ):?>

						 	<!--loop through the rows of data-->
						   <?php  while ( have_rows('organization') ) : the_row();?>

						        <!--display a sub field value-->
								<?php if( get_sub_field('logo') ): ?>
						        <li><a href="<?php the_sub_field('logo_link');?>"><img src="<?php the_sub_field('logo');?>" alt=""></a></li>
								<?php endif; ?>

						    <?php endwhile;?>

						<?php else :?>

						    <!--no rows found-->

						<?php endif;?>

			
				</ul>


				<?php if( get_field('organizations_button_text') ): ?>
					<a href="<?php the_field('organizations_button_link');?>" class="btn ghost-btn"><?php the_field('organizations_button_text');?></a>
					<?php endif; ?>
				
			</div><!--end organizations-->

<?php get_footer();?>