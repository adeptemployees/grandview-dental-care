<?php
/*
Template Name: Payment Page
*/
?>



<?php get_header();?>

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

	<div class="page-title"><!--start page title-->

		<div class="container"><!--start container-->

			<h1><?php the_title();?></h1>

		</div><!--end container-->

	</div><!--end page title-->

	<div class="page"><!--start page-->

		<div class="main-content-wrap"><!--start main content wrap-->

			<div class="container"><!--start container-->

				<div class="wrap"><!--start wrap-->

					<div class="main-content-full"><!--start main content-->
						<div class="content row">
							<?php if(isset($_GET['success']) && $_GET['success'] == "yes") : ?>
								<p><strong>Your payment has been received and noted on your account.</strong></p>
							<?php elseif(isset($_GET['success']) && $_GET['success'] == "no") : ?>
								<p><strong>There was an error processing your request. Please go back and try again.</strong>  </p>
							<?php else : ?>
	
								<?php the_content(); ?>
								
								<div class="gform_wrapper">
									<form action="<?php echo bloginfo('template_url'); ?>/library/paymentform.php" method="post" id="payment-form">
										<div class="input-group">
											<label for="patient_firstname">Patient First Name <span class="gfield_required">*</span></label>
											<input type="text" id="patient_firstname" name="patient_firstname" required="required">
										</div>
										<div class="input-group">
											<label for="patient_lastname">Patient Last Name <span class="gfield_required">*</span></label>
											<input type="text" id="patient_lastname" name="patient_lastname" required="required">
										</div>
										<div class="input-group">
											<label for="cardholder_firstname">Cardholder First Name <span class="gfield_required">*</span></label>
											<input type="text" id="cardholder_firstname" name="cardholder_firstname" required="required">
										</div>
										<div class="input-group">
											<label for="cardholder_lastname">Cardholder Last Name <span class="gfield_required">*</span></label>
											<input type="text" id="cardholder_lastname" name="cardholder_lastname" required="required">
										</div>
										<div class="input-group large">
											<label for="card_type">Payment Type <span class="gfield_required">*</span> <span class="card-types"><i class="fa fa-cc-visa"></i> <i class="fa fa-cc-mastercard"></i> <i class="fa fa-cc-discover"></i> <i class="fa fa-cc-amex"></i></span></label>
											<select type="text" id="card_type" name="card_type" required="required">
												<option value="">-- Select One --</option>
												<option value="Visa">Visa</option>
												<option value="Mastercard">Mastercard</option>
												<option value="Discover">Discover</option>
												<option value="Amex">American Express</option>
											</select>
										</div>
										<div class="input-group large">
											<label for="card_number">Credit Card Number <span class="gfield_required">*</span></label>
											<input type="text" id="card_number" name="card_number" required="required">
										</div>
										<div class="input-group small">
											<label for="card_exp">Expiration Date <span class="gfield_required">*</span></label>
											<input type="text" id="card_exp" name="card_exp" required="required">
											<small>Format: MM/YY</small>
										</div>
										<div class="input-group small">
											<label for="card_cvv">CVV <span class="gfield_required">*</span></label>
											<input type="text" id="card_cvv" name="card_cvv" required="required">
										</div>
										<div class="input-group large">
											<label for="payment_amount">Payment Amount <span class="gfield_required">*</span></label>
											<input type="text" id="payment_amount" name="payment_amount" required="required" placeholder="$">
										</div>

										<div class="input-group large">
											<label for="conf_email">Email Address <span class="gfield_required">*</span></label>
											<input type="email" id="conf_email" name="conf_email" required="required">
											<small>Email address where we should send a confirmation</small>
										</div>
										

										<input type="submit" value="Submit Payment">
									</form>
								</div>
								
							<?php endif; ?>

						</div> <!-- content -->

					</div><!--end main content-->

				</div><!--end wrap-->

			</div><!--end container-->

		</div><!--end main content wrap-->

	</div> <!-- page -->

<?php endwhile; endif; ?><!--end loop-->

<?php get_footer();?>


