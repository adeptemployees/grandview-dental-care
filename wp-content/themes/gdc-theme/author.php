<?php get_header();?>

<div class="page-title"><!--start page title-->

	<div class="container"><!--start container-->

		<h1>Author: <em><?php the_author(); ?></em></h1>
		
	</div><!--end container-->

</div><!--end page title-->

<div class="page"><!--start page-->

	<div class="main-content-wrap"><!--start main content wrap-->

		<div class="container"><!--start container-->

			<div class="wrap"><!--start wrap-->

				<div class="main-content"><!--start main content-->

					<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

					<div class="entry"><!--start entry-->

						<?php if ( has_post_thumbnail($post->ID) ) { ?>

							<div class="entry-featured"><!--start featured-->
						
								<a href="<?php echo the_permalink(); ?>"><?php the_post_thumbnail('entry');?></a>
						
							</div><!--end featured-->

						<?php } if(blog_first_image()) { 
									//$img_src = blog_first_image();
									//$img_resize = aq_resize($img_src, 300);
									//var_dump($img_resize);
						?>

							<div class="entry-featured">
								<a href="<?php echo the_permalink(); ?>"><img src="<?php echo blog_first_image(); ?>" alt=""></a>
							</div>
						<?php } ?>
						
						<div class="entry-excerpt"><!--start entry except-->

							<a href="<?php the_permalink(); ?>"><h3><?php the_title();?></h3></a>
						
							<em><?php the_author_posts_link(); ?> | <?php echo get_the_date();?></em>
						
							<?php the_excerpt();?> <a class="btn green-btn m-btn" href="<?php the_permalink(); ?>">Read More &raquo;</a>

						</div><!--end entry excerpt-->
						
					</div><!--end entry-->

				<?php endwhile; else: ?>
				<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
			<?php endif; ?>

			<div class="pagination"><!--start pagination-->
				<?php
				global $wp_query;
				
				if ( $wp_query->max_num_pages > 1 ) {
					$big = 99999999;
					echo paginate_links(array(
						'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
						'format' => '/page/%#%',
						'total' => $wp_query->max_num_pages,
						'current' => max(1, get_query_var('paged')),
						'show_all' => false,
						'end_size' => 2,
						'mid_size' => 3,
						'prev_next' => true,
						'prev_text' => 'Prev',
						'next_text' => 'Next',
						'type' => 'list'
						));
					} ?>
				</div> <!-- pagination -->

			</div><!--end main content-->

			<div class="sidebar"><!--start sidebar-->
				<div class="widget">
					<?php dynamic_sidebar('sidebar_widget');?>
				</div> <!-- widget -->
			</div> <!-- sidebar -->

		</div><!--end wrap-->

	</div><!--end container-->

</div><!--end main content wrap-->

<?php get_footer();?>