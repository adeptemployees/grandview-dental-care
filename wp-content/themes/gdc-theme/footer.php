</div> <!-- content-wrapper -->
			<div class="footer-wrap wrap"><!--start footer wrap-->

				<footer class="main-footer"><!--start footer-->

					<div class="wrap">

						<?php

						$children_1 = wp_list_pages("title_li=&child_of=167&echo=0");?>

						<ul>
							<?php $parent_title_1 = get_the_title(167);?>
							<?php $parent_link = get_permalink(167);?>
							<li><a href="<?php echo $parent_link; ?>"><?php echo $parent_title_1;?></a></li>
							<?php echo $children_1; ?>
						</ul>

						<?php

						$children_2 = wp_list_pages("title_li=&child_of=179&echo=0");?>

						<ul>
							<?php $parent_title_2 = get_the_title(179);?>
							<?php $parent_link_2 = get_permalink(179);?>
							<li><a href="<?php echo $parent_link_2;?>"><?php echo $parent_title_2;?></a></li>
							<?php echo $children_2; ?>
						</ul>

						<?php $children_3 = wp_list_pages("title_li=&child_of=189&echo=0");?>

						<ul>
							<?php $parent_title_3 = get_the_title(189);?>
							<?php $parent_link_3 = get_permalink(189);?>
							<li><a href="<?php echo $parent_link_3;?>"><?php echo $parent_title_3;?></a></li>
							<?php echo $children_3; ?>
						</ul>

						<?php $children_4 = wp_list_pages("title_li=&child_of=201&echo=0&exclude=7203");?>

						<ul>
							<?php $parent_title_4 = get_the_title(201);?>
							<?php $parent_link_4 = get_permalink(201);?>
							<li><a href="<?php echo $parent_link_4;?>"><?php echo $parent_title_4;?></a></li>
							<?php echo $children_4; ?>
						</ul>

						<?php $children_5 = wp_list_pages("title_li=&child_of=209&echo=0");?>

						<ul>
							<?php $parent_title_5 = get_the_title(209);?>
							<?php $parent_link_5 = get_permalink(209);?>
							<li><a href="<?php echo $parent_link_5;?>"><?php echo $parent_title_5;?></a></li>
							<?php echo $children_5; ?>
						</ul>

						<?php $children_6 = wp_list_pages("title_li=&child_of=217&echo=0&exclude=223");?>

						<ul>
							<?php $parent_title_6 = get_the_title(217);?>
							<?php $parent_link_6 = get_permalink(217);?>
							<li><a href="<?php echo $parent_link_6;?>"><?php echo $parent_title_6;?></a></li>
							<?php echo $children_6; ?>
						</ul>

						<?php $children_7 = wp_list_pages("title_li=&child_of=225&echo=0");?>

						<ul>
							<?php $parent_title_7 = get_the_title(225);?>
							<?php $parent_link_7 = get_permalink(225);?>
							<li><a href="<?php echo $parent_link_7;?>"><?php echo $parent_title_7;?></a></li>
							<?php echo $children_7; ?>
						</ul>
						
						<?php $children_11 = wp_list_pages("title_li=&child_of=7538&echo=0");?>

						<ul>
							<?php $parent_title_11 = get_the_title(7538);?>
							<?php $parent_link_11 = get_permalink(7538);?>
							<li><a href="<?php echo $parent_link_11;?>"><?php echo $parent_title_11;?></a></li>
							<?php echo $children_11; ?>
						</ul>
						
						<?php $children_10 = wp_list_pages("title_li=&child_of=231&echo=0");?>

						<ul>
							<?php $parent_title_10 = get_the_title(231);?>
							<?php $parent_link_10 = get_permalink(231);?>
							<li><a href="<?php echo $parent_link_10;?>"><?php echo $parent_title_10;?></a></li>
							<?php echo $children_10; ?>
						</ul>

						<?php $children_11 = wp_list_pages("title_li=&child_of=235&echo=0");?>

						<ul>
							<?php $parent_title_11 = get_the_title(235);?>
							<?php $parent_link_11 = get_permalink(235);?>
							<li><a href="<?php echo $parent_link_11;?>"><?php echo $parent_title_11;?></a></li>
							<?php echo $children_11; ?>
						</ul>

						<?php $children_12 = wp_list_pages("title_li=&child_of=237&echo=0");?>

						<ul>
							<?php $parent_title_12 = get_the_title(237);?>
							<?php $parent_link_12 = get_permalink(237);?>
							<li><a href="<?php echo $parent_link_12;?>"><?php echo $parent_title_12;?></a></li>
							<?php echo $children_12; ?>
						</ul>

						<?php //$children_13 = wp_list_pages("title_li=&child_of=25&echo=0");?>

						<ul>
							<?php $parent_title_13 = get_the_title(25);?>
							<?php $parent_link_13 = get_permalink(25);?>
							<li><a href="<?php echo $parent_link_13;?>">Grandview Location</a></li>
						</ul>

					</div>

					<div class="footer-content">
						<p>Grandview Dental Care is a full-service dental office serving patients in Grandview, Upper Arlington, Short North, Hilliard and the surrounding Columbus areas.</p>
						<img src="<?php echo get_template_directory_uri(); ?>/images/hipaa-seal-of-compliance.png" width="450" height="240" alt="HIPAA Seal of Compliance - HIPAA Verified"> 
					</div>
					
				</footer><!--end footer-->

				<div class="footer-info"><!--start footer info-->
					<small>&copy;<?php echo date('Y'); ?> Grandview Dental Care | <?php echo get_field('street_address', 'options'); ?>, <?php echo get_field('city', 'options'); ?>, <?php echo get_field('state', 'options'); ?> <?php echo get_field('zip_code', 'options'); ?></small>

				</div><!--end footer info-->

			</div><!--end footer wrap-->
			
		</div>
		
		<a href="#top" class="btt-btn"><i class="fa fa-caret-up"></i></a>


		<?php 
			wp_enqueue_script("plugins", get_template_directory_uri()."/js/min/plugins.js", array("jquery"), false, true );
			wp_enqueue_script("scripts", get_template_directory_uri()."/js/min/scripts.js", array("plugins"), false, true );
		?>
		
		<?php wp_footer(); ?>

		<script>
		$(document).ready(function() {
			if (!Modernizr.svg) {
				$("img").each(function() {
					$(this).attr("src", $(this).data("fallback"));
				});
			}
		});
		</script>
		
<?php if(is_user_logged_in()) : ?>
		<!-- BUGHERD -->
		<script type='text/javascript'>
		(function (d, t) {
			var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
			bh.type = 'text/javascript';
			bh.src = '//www.bugherd.com/sidebarv2.js?apikey=lc96wfgylpe6bvff6tznlg';
			s.parentNode.insertBefore(bh, s);
		})(document, 'script');
		</script>		
<?php endif; ?>

		<script type="text/javascript">
			setTimeout(function(){var a=document.createElement("script");
			var b=document.getElementsByTagName("script")[0];
			a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0023/2874.js?"+Math.floor(new Date().getTime()/3600000);
			a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
		</script>

		<!-- <script defer charset="utf-8" type="text/javascript">var switchTo5x=true;</script>
		<script defer charset="utf-8" type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
		<script defer charset="utf-8" type="text/javascript">stLight.options({"publisher":"1e9bacdd-43c8-440f-bd37-55833d702fe0","publisherGA":"UA-4008027-1", "doNotCopy":true,"hashAddressBar":false,"doNotHash":true});var st_type="wordpress4.2.2";</script> -->

	</body>
</html>